###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[========================================================================[.rst:
DetectorMacros
==============

Useful macros for the configuration of the Detector or downwnstream projects.

add_detector_plugin
-------------------

::

 add_detector_plugin(name sources...)

Create a plugin library for Detector/DD4hep with the given name from the
listed sources.

#]========================================================================]

function(add_detector_plugin name)
    add_library(${name} MODULE ${ARGN})
    target_link_libraries(${name} Detector::DetectorLib)
    add_custom_command(TARGET ${name} POST_BUILD
        COMMAND env LD_LIBRARY_PATH=.:$<TARGET_FILE_DIR:DD4hep::DD4hepGaudiPluginMgr>:$ENV{LD_LIBRARY_PATH}
            $<TARGET_FILE:DD4hep::listcomponents>
                -o ${CMAKE_CURRENT_BINARY_DIR}/${name}.components
                $<TARGET_FILE_NAME:${name}>
        # make sure we have a "copy" of the components file alongside the module library
        # (the "|| true" is to cover the case the module is created in ${CMAKE_CURRENT_BINARY_DIR})
        COMMAND ln -sf ${CMAKE_CURRENT_BINARY_DIR}/${name}.components . 2>/dev/null || true
        WORKING_DIRECTORY $<TARGET_FILE_DIR:${name}>
        BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/${name}.components)
    # components file destination should match module destination
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${name}.components DESTINATION lib)
    install(TARGETS ${name} DESTINATION lib)
endfunction()
