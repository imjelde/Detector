/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/Handle.h"
#include "DD4hep/detail/ConditionsInterna.h"

#include <optional>

namespace LHCb::Detector {

  namespace detail {
    struct DeIOVObject;
    inline static constexpr double GaudiCentimeter    = 10.0;
    inline static constexpr double DD4hepToLHCbCm     = GaudiCentimeter / dd4hep::centimeter;
    inline static constexpr double LHCbToDD4hepCm     = dd4hep::centimeter / GaudiCentimeter;
    inline static constexpr double GaudiTeV           = 1.0;
    inline static constexpr double DD4hepToLHCbEnergy = dd4hep::TeV / GaudiTeV;
    //
    inline auto toLHCbLengthUnits( const double length ) { return DD4hepToLHCbCm * length; }
    inline auto toLHCbAreaUnits( const double area ) { return DD4hepToLHCbCm * DD4hepToLHCbCm * area; }
    inline auto toLHCbEnergyUnits( const double energy ) { return DD4hepToLHCbEnergy * energy; }
    inline ROOT::Math::Transform3D toLHCbLengthUnits( const ROOT::Math::Transform3D& t ) {
      double xx{}, xy{}, xz{}, dx{}, yx{}, yy{}, yz{}, dy{}, zx{}, zy{}, zz{}, dz{};
      t.GetComponents( xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz );
      return {xx, xy, xz, toLHCbLengthUnits( dx ), //
              yx, yy, yz, toLHCbLengthUnits( dy ), //
              zx, zy, zz, toLHCbLengthUnits( dz )};
    }
    inline ROOT::Math::Translation3D toLHCbLengthUnits( const ROOT::Math::Translation3D& t ) {
      double dx{}, dy{}, dz{};
      t.GetComponents( dx, dy, dz );
      return {toLHCbLengthUnits( dx ), toLHCbLengthUnits( dy ), toLHCbLengthUnits( dz )};
    }
    inline auto toLHCbLengthUnits( const ROOT::Math::XYZVector& v ) { return DD4hepToLHCbCm * v; }
    inline auto toDD4hepUnits( const ROOT::Math::XYZVector& v ) { return LHCbToDD4hepCm * v; }
  } // namespace detail

  template <typename ObjectType>
  struct DeIOVElement : dd4hep::Handle<ObjectType> {
    using dd4hep::Handle<ObjectType>::Handle;

    /// Print the detector element's information to stdout
    void print( int indent, int flg ) const { this->access()->print( indent, flg ); }
    /// Compute key value for caching
    static auto key( const std::string& value ) { return dd4hep::ConditionKey::itemCode( value ); }
    /// Compute key value for caching
    static auto key( const char* value ) { return dd4hep::ConditionKey::itemCode( value ); }

    /// Detector element Class ID, as given by DetDesc, for backward compatibility
    auto clsID() const { return this->access()->clsID; }
    /// Accessor to detector structure
    auto detector() const { return this->access()->detector; }
    /// Accessor to the geometry structure of this detector element
    auto geometry() const { return this->access()->geometry; }
    /// Accessor to the geometry structure of this detector element
    auto lVolumeName() const { return geometry().volume().name(); }
    /// Access to the alignmant object to transformideal coordinates
    auto detectorAlignment() const { return this->access()->detectorAlignment; }

    /// Checker whether global point is inside this detectos element.
    bool isInside( const ROOT::Math::XYZPoint& globalPoint ) const;

    /** Access to more sophisticated geometry information                      */
    /// Check if the geometry is connected to a logical volume
    bool hasLVolume() const { return geometry().volume().isValid(); }
    /// Check if the geometry is connected to a supporting parent detector element
    bool hasSupport() const { return detector().parent().isValid(); }

    /// Access to transformation matrices
    auto toLocalMatrix() const { return detail::toLHCbLengthUnits( this->access()->toLocalMatrix ); }
    auto toGlobalMatrix() const { return detail::toLHCbLengthUnits( this->access()->toGlobalMatrix ); }
    auto toLocalMatrixNominal() const { return detail::toLHCbLengthUnits( this->access()->toLocalMatrixNominal ); }
    auto toGlobalMatrixNominal() const { return detail::toLHCbLengthUnits( this->access()->toGlobalMatrixNominal ); }

    /// Local -> Global and Global -> Local transformations
    auto toLocal( const ROOT::Math::XYZPoint& global ) const {
      return ROOT::Math::XYZPoint( toLocal( ROOT::Math::XYZVector( global ) ) );
    }
    auto toGlobal( const ROOT::Math::XYZPoint& local ) const {
      return ROOT::Math::XYZPoint( toGlobal( ROOT::Math::XYZVector( local ) ) );
    }
    auto toLocal( const ROOT::Math::XYZVector& globalDirection ) const {
      return this->access()->toLocal( globalDirection );
    }
    auto toGlobal( const ROOT::Math::XYZVector& localDirection ) const {
      return this->access()->toGlobal( localDirection );
    }
    auto toLocal( const ROOT::Math::Transform3D& transform ) const { return toLocalMatrix() * transform; }
    auto toGlobal( const ROOT::Math::Transform3D& transform ) const { return toGlobalMatrix() * transform; }
    auto ownToOffNominalMatrix() const { return detail::toLHCbLengthUnits( this->access()->delta ); }
    auto elemDeltaTranslations() const {
      std::vector<double> res{0, 0, 0};
      ownToOffNominalMatrix().Translation().GetComponents( res[0], res[1], res[2] );
      return res;
    }
    auto elemDeltaRotations() const {
      std::vector<double> res{0, 0, 0, 0, 0, 0, 0, 0, 0};
      ownToOffNominalMatrix().Rotation().GetComponents( res[0], res[1], res[2], res[3], res[4], res[5], res[6], res[7],
                                                        res[8] );
      return res;
    }

    auto delta() const {
      const ROOT::Math::Transform3D delta    = detail::toLHCbLengthUnits( ownToOffNominalMatrix() );
      const auto                    rot      = delta.Rotation<ROOT::Math::RotationZYX>();
      const auto                    pivot    = detail::toLHCbLengthUnits( detectorAlignment().delta().pivot );
      const auto                    pivotRot = pivot * rot * pivot.Inverse();
      const auto                    trans    = ( delta * pivotRot.Inverse() ).Translation();
      return dd4hep::Delta( pivot.Vect(), trans, rot );
    }

    /// returns motionSystemTransform if we are dealing with the Velo
    auto motionSystemTransform() const { return this->access()->motionSystemTransform(); }

    /// Obtain the 3D transformation that applies misalignment from nominal
    /// position in the frame of the detector element's parent.
    auto localDeltaMatrix( const ROOT::Math::Transform3D& globalDelta ) const {
      const auto d_0 = ownToOffNominalMatrix();
      const auto T   = toGlobalMatrix();
      return d_0 * T.Inverse() * globalDelta * T;
    }

    void applyToAllChildren( const std::function<void( DeIOVElement<detail::DeIOVObject> )>& func ) const {
      return this->access()->applyToAllChildren( func );
    }

    auto size() const { return this->access()->size(); }
  };

  namespace detail {

    struct DeIOVObject;

    /**
     *  Base class for interval of validity dependent data
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    struct DeIOVObject : dd4hep::detail::ConditionObject {
      DeIOVObject( dd4hep::DetElement de, dd4hep::cond::ConditionUpdateContext& ctxt, int classID = 6,
                   bool hasAlignment = true );
      /// Detector element Class ID, as given by DetDesc, for backward compatibility
      int clsID{6}; // matches DetDesc CLID_AlignmentCondition
      /// The dd4hep detector element reference of this gaudi detector element
      dd4hep::DetElement detector;
      /// The alignment object
      dd4hep::Alignment detectorAlignment;
      /// We cache a reference to the geometry information of this gaudi detector element
      dd4hep::PlacedVolume geometry;
      /// We want to cache here the matrix from local to world as original one is a TGeoHMatrix in ROOT system of units
      /// and we want a Transform3D with LHCb system of units
      ROOT::Math::Transform3D toGlobalMatrix;
      /// We want to cache here the nominal matrix from local to world as original one is a TGeoHMatrix in ROOT
      /// system of units and we want a Transform3D with LHCb system of units
      ROOT::Math::Transform3D toGlobalMatrixNominal;
      /// We want to cache here the matrix from world to local (i.e. inverse toGlobalMatrix)
      ROOT::Math::Transform3D toLocalMatrix;
      /// We want to cache here the nominal matrix from world to local (i.e. inverse toGlobalMatrixNominal)
      ROOT::Math::Transform3D toLocalMatrixNominal;
      /// We want to cache here the delta matrix, that is the alignment
      ROOT::Math::Transform3D delta;

      /// returns motionSystemTransform if we are dealing with the Velo, false otherwise
      std::optional<ROOT::Math::Transform3D> motionSystemTransform() const { return {}; }

      /// virtual method to generically apply a function to all children
      virtual void        applyToAllChildren( const std::function<void( DeIOVElement<DeIOVObject> )>& ) const {};
      virtual std::size_t size() const { return 0; }

      auto toLocal( const ROOT::Math::XYZVector& globalDirection ) const {
        return detail::toLHCbLengthUnits( detectorAlignment.worldToLocal( detail::toDD4hepUnits( globalDirection ) ) );
      }
      auto toGlobal( const ROOT::Math::XYZVector& localDirection ) const {
        return detail::toLHCbLengthUnits( detectorAlignment.localToWorld( detail::toDD4hepUnits( localDirection ) ) );
      }

      /// Printout method to stdout
      virtual void print( int indent, int flags ) const;
    };
  } // End namespace detail

  using DeIOV = DeIOVElement<detail::DeIOVObject>;

} // End namespace LHCb::Detector

/// helper member using IGeometryInfo::isInside
template <typename ObjectType>
bool LHCb::Detector::DeIOVElement<ObjectType>::isInside( const ROOT::Math::XYZPoint& globalPoint ) const {
  // Go through all nodes to check each of them
  const auto& alignment  = detectorAlignment();
  auto        localPoint = toLocal( globalPoint );
  for ( const auto& node : alignment.nodes() ) {
    Double_t p[3]{localPoint.x(), localPoint.y(), localPoint.z()};
    if ( node.volume()->Contains( p ) ) return true;
  }
  return false;
}
