/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Core/MagneticFieldGrid.h"
#include "Math/Vector3D.h"
#include <string>
#include <vector>

struct GridQuadrant;

namespace LHCb::Magnet {

  // Conversion factors needed to load the MagneticFieldGridData
  static constexpr double cm    = 10;
  static constexpr double gauss = 1e-7;

  class MagneticFieldGridReader {
  public:
    MagneticFieldGridReader( std::string field_map_path );

    bool readFiles( const std::vector<std::string>& filenames, MagneticFieldGrid& grid ) const;

    bool readDC06File( const std::string& filename, MagneticFieldGrid& grid ) const;

    void fillConstantField( const ROOT::Math::XYZVector& field, MagneticFieldGrid& grid ) const;

  private:
    void        fillGridFromQuadrants( GridQuadrant* quadrants, MagneticFieldGrid& grid ) const;
    bool        readQuadrant( const std::string& filename, GridQuadrant& quad ) const;
    std::string m_mapFilePath = "";
  };

} // namespace LHCb::Magnet