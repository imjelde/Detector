//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================
#pragma once

// Framework include files
#include "Core/ConditionConverter.h"
#include "Core/ConditionsOverlay.h"
#include "Core/ConditionsReader.h"
#include "DD4hep/ConditionsListener.h"
#include "DD4hep/Printout.h"
#include "DDCond/ConditionsDataLoader.h"
#include "XML/XML.h"

#include <map>
#include <memory>
#include <string>

/// Namespace for the Gaudi framework
namespace LHCb::Detector {

  ///
  /**
   *  \author   M.Frank
   *  \version  1.0
   *  \ingroup  DD4HEP_GAUDI
   */
  class ConditionsLoader : public dd4hep::cond::ConditionsDataLoader {
    using Key    = std::pair<std::string, std::string>;
    using KeyMap = std::map<dd4hep::Condition::key_type, Key>;
    std::unique_ptr<ConditionsReader>  m_reader;
    std::string                        m_readerType, m_directory, m_match, m_dbtag;
    std::map<int, ConditionConverter*> m_converters;
    std::string                        m_iovTypeName;
    const dd4hep::IOVType*             m_iovType = 0;
    // Optional conditions overlay
    //
    // If the overlay is enabled, we keep a copy of the data last read from the database,
    // that can then be updated to override values of conditions. When a condition is overriden
    // (dirty flag set to true) future reads from the disk will get the data from the overlay
    // ignoring the disk content.
    bool              m_useOverlay = false;
    ConditionsOverlay m_overlay;

  public:
    /// Default constructor
    ConditionsLoader( dd4hep::Detector& description, dd4hep::cond::ConditionsManager mgr, const std::string& nam );
    /// Access a conditions loader for a particular class ID
    ConditionConverter& conditionConverter( int class_id );
    /// Initialize loader according to user information
    void initialize() override;
    /// Optimized update using conditions slice data
    size_t load_many( const dd4hep::IOV& req_validity, RequiredItems& work, LoadedItems& loaded,
                      dd4hep::IOV& conditions_validity ) override;

    /// Accessor for the ConditionsOverlay internal instance.
    ConditionsOverlay& overlay() { return m_overlay; }
  };
} // namespace LHCb::Detector
