//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================
#pragma once

// Framework include files
#include "Core/ConditionIdentifier.h"
#include "Core/UpgradeTags.h"
#include "DD4hep/Alignments.h"
#include "DD4hep/ConditionsData.h"
#include "DD4hep/Objects.h"
#include "DD4hep/OpaqueDataBinder.h"
#include "DD4hep/Printout.h"
#include "XML/Conversions.h"
#include "XML/XML.h"
#include "XML/XMLTags.h"

/// Namespace for the Gaudi framework
namespace LHCb::Detector {

  ///
  /**
   *  \author   M.Frank
   *  \version  1.0
   *  \ingroup  DD4HEP_GAUDI
   */
  class ConditionConverter {
  public:
    virtual ~ConditionConverter()                               = default;
    virtual dd4hep::Condition operator()( dd4hep::Detector& description, ConditionIdentifier* identifier,
                                          xml_h element ) const = 0;
  };

  ///
  /**
   *  \author   M.Frank
   *  \version  1.0
   *  \ingroup  DD4HEP_GAUDI
   */
  template <int i>
  class ConcreteConditionConverter : public ConditionConverter {
  public:
    ConcreteConditionConverter()          = default;
    virtual ~ConcreteConditionConverter() = default;
    virtual dd4hep::Condition operator()( dd4hep::Detector& description, ConditionIdentifier* identifier,
                                          xml_h element ) const override;
  };
} /* End namespace LHCb::Detector                       */
