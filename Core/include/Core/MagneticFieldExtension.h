/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/MagneticFieldGrid.h"
#include "DD4hep/DetElement.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Detector.h"
#include "DDCond/ConditionsSlice.h"

#include <map>
#include <memory>
#include <string>

namespace LHCb::Magnet {

  /**
   * @brief Create a DetElement extension that can load the field map files from disk
   *
   * @param description The dd4hep::Detector instance for the geometry description
   * @param field_map_path The directory containing the field map files
   */
  void setup_magnetic_field_extension( dd4hep::Detector& description, std::string field_map_path,
                                       double nominal_current = 5850 );

  /**
   * @brief Return the list of conditions the MagneticFieldDepends on
   *
   * @return std::vector<std::string>
   */
  std::vector<std::string> dependency_list();

  class MagneticFieldExtension {

  public:
    MagneticFieldExtension( dd4hep::Detector& description, std::string magFieldDir, double nominal_current )
        : m_description{description}, m_magneticFieldFilesLocation{magFieldDir}, m_nominalCurrent{nominal_current} {}

    /* Load the magnetic field map from a specific condition */
    std::shared_ptr<LHCb::Magnet::MagneticFieldGrid>
    load_magnetic_field_grid( dd4hep::cond::ConditionUpdateContext& context, bool& useRealMap, bool& isDown,
                              double& signedRelativeCurrent );

    // Extension state
    dd4hep::Detector& m_description;
    std::string       m_magneticFieldFilesLocation; // Loaded from the conditions
    double            m_nominalCurrent;             // Nominal current in amps
  };

} // End namespace LHCb::Magnet
