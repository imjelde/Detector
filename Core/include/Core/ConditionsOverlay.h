/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <filesystem>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <string_view>
#include <yaml-cpp/yaml.h>

namespace LHCb::Detector {
  /// Writable in-memory no-IOV YAML-based conditions database
  ///
  /// The main purpose is to optionally override entries from the actual database, but can be used
  /// for testing.
  ///
  /// Until there's any write (via ConditionsOverlay::update) the overlay is transparent and calls
  /// to ConditionsOverlay::get will be just passthrough (keeping a copy of the docs that are processed).
  ///
  /// When part of a document is updated the call to ConditionsOverlay::get will return the copy in the
  /// overlay ignoring the content retrived from the database, while still being transparent for all
  /// other documents.
  ///
  /// \warning { ConditionsOverlay is only meant to work with YAML documents. }
  class ConditionsOverlay {

    struct OverlayEntry {
      YAML::Node data;
      bool       dirty = false;
    };
    std::map<std::string, OverlayEntry> m_store;

  public:
    /// Add (or overwrite) a document to the overlay and return a copy of the added data.
    YAML::Node add( const std::string& path, YAML::Node doc = {} ) {
      if ( path.empty() ) throw std::domain_error( "invalid path" ); // FIXME use boost::contract
      return m_store.insert_or_assign( path, OverlayEntry{std::move( doc )} ).first->second.data;
    }
    /// Get the YAML::Node from the overlay if present and have been written to, otherwise use (parse)
    /// the content of the raw_data
    YAML::Node get( const std::string& path, const std::string& raw_data ) {
      if ( auto item = m_store.find( path ); item != m_store.end() && item->second.dirty ) {
        // we have and entry that has been touched once, so we give it back
        return item->second.data;
      }
      // otherwise we parse and give back the result (keeping a copy for later)
      return add( path, YAML::Load( raw_data ) );
    }
    /// Update a condition value in the overlay.
    /// If the containing document is not present, an empty one is used as starting point
    void update( const std::string& path, const std::string& name, YAML::Node value ) {
      if ( path.empty() ) throw std::domain_error( "invalid path" ); // FIXME use boost::contract
      auto& [node, dirty] = m_store[path];
      dirty               = true;
      node[name]          = std::move( value );
    }
    /// Write the content of the overlay to disk, in the given directory
    void dump( std::filesystem::path root_dir ) const {
      namespace fs = std::filesystem;
      std::for_each( m_store.begin(), m_store.end(), [&root_dir]( const auto& element ) {
        std::string_view path{element.first};
        if ( path[0] == '/' ) path.remove_prefix( 1 );
        auto target = root_dir / path;
        fs::create_directories( target.parent_path() );
        std::ofstream( target ) << element.second.data << '\n';
      } );
    }
    /// Get the number of documents in the overlay
    auto size() const { return m_store.size(); }
    /// Empty the overlay
    void clear() { m_store.clear(); }
  };
} // namespace LHCb::Detector
