/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/ConditionsRepository.h"
#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsSlice.h"

#include <filesystem>
#include <map>
#include <memory>
#include <vector>
#include <yaml-cpp/yaml.h>

namespace LHCb::Detector {
  class ConditionsOverlay;

  class DetectorDataService {

  public:
    DetectorDataService( dd4hep::Detector& description, std::vector<std::string> detectorNames )
        : m_description( description )
        , m_manager( description, "DD4hep_ConditionsManager_Type1" )
        , m_detectorNames( detectorNames ) {}

    void initialize( std::string repositoryUrl, std::string conditionsDBtag, bool withOverlay = false );
    void finalize();

    std::shared_ptr<dd4hep::cond::ConditionsSlice> get_slice( size_t iov );
    std::shared_ptr<dd4hep::cond::ConditionsSlice> load_slice( size_t iov );
    void                                           drop_slice( size_t iov );

    /// Update a condition value in the conditions overlay.
    /// The DetectorDataService must be initialized with the overlay enabled when invokind this function,
    /// otherwise an exception is thrown.
    void update_condition( const std::string& path, const std::string& name, YAML::Node data );
    /// Dump the conditions in the overlay to a directory on disk.
    /// The DetectorDataService must be initialized with the overlay enabled when invokind this function,
    /// otherwise an exception is thrown.
    void dump_conditions( std::filesystem::path root_dir );
    /// Helper to update in the conditions overlay a condition bound to a DetElement.
    void update_condition( const dd4hep::DetElement& de, const std::string& cond, YAML::Node data );
    /// Helper to update in the conditions overlay the alignment condition of a given DetElement.
    void update_alignment( const dd4hep::DetElement& de, const dd4hep::Delta& delta );

  protected:
    dd4hep::Detector&                     m_description;
    dd4hep::cond::ConditionsManager       m_manager;
    std::vector<std::string>              m_detectorNames;
    const dd4hep::IOVType*                m_iov_typ{nullptr};
    std::shared_ptr<ConditionsRepository> m_all_conditions = std::make_shared<ConditionsRepository>();
    // key is iov's lower bound, value is tuple (upper bound, slice)
    std::map<size_t, std::tuple<size_t, std::shared_ptr<dd4hep::cond::ConditionsSlice>>> m_cache;
    std::mutex                                                                           m_cache_mutex;
    LHCb::Detector::ConditionsOverlay*                                                   m_condOverlay{nullptr};
  };

  inline const std::type_info& get_condition_type( const dd4hep::Condition& condition ) {
    if ( condition.is_bound() )
      return condition.typeInfo();
    else {
      // assign the result of operator* to avoid clang's -Wpotentially-evaluated-expression
      auto& held_object = *condition;
      return typeid( held_object );
    }
  }

} // End namespace LHCb::Detector
