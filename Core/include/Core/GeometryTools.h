/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <functional>
#include <map>
#include <sstream>
#include <string>

#include "TGeoNode.h"

namespace lhcb::geometrytools {

  /**
   * struct to compare TGeoNode * by node name
   */
  struct TGeoNodeCmp {
    bool operator()( const TGeoNode* a, const TGeoNode* b ) const { return strcmp( a->GetName(), b->GetName() ) < 0; }
  };

  /**
   *  The template for our sorted node map
   */
  template <typename T>
  using nodeMap = std::map<TGeoNode*, T, TGeoNodeCmp>;

  /**
   *  Utility to derive the return type of the callable
   */
  template <typename Lambda>
  struct function_traits : function_traits<decltype( &Lambda::operator() )> {};
  template <typename R, typename C, typename... Args>
  struct function_traits<R ( C::* )( Args... ) const> {
    using result = R;
  };
  template <typename F>
  using return_type = typename function_traits<F>::result;

  /**
   * Traverse the ROOT node tree and apply the callback to each of them
   * For each node, the callback has access to:
   *  - the node pointer
   *  - the path
   *  - the level
   *  - a map with all daughters sorted by node name, with the result of the callback on each of them
   */
  template <typename C>
  return_type<C> traverse( TGeoNode* node, C& callback, std::string path = "/world", int level = 0 ) {

    nodeMap<return_type<C>> daughters;
    for ( int i = 0; i < node->GetNdaughters(); i++ ) {
      TGeoNode* n = node->GetDaughter( i );
      daughters.emplace( n, traverse( n, callback, path + +"/" + n->GetName(), level + 1 ) );
    }

    return callback( node, daughters, path, level );
  }

  /**
   * Return a string representing specific a node
   */
  std::string toString( TGeoNode* node );

  /**
   * Checksum a geometry, stating by its top node.
   * Returns a map <path, checksum> for all nodes with level <= maxlevel
   */
  std::map<std::string, std::string> checksum( TGeoNode* node, int maxlevel = 10 );

} // namespace lhcb::geometrytools
