/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <boost/program_options.hpp>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>

#include <yaml-cpp/yaml.h>

#include "DD4hep/Detector.h"
#include "DD4hep/Printout.h"

#include "Core/GeometryTools.h"

#include "TGeoManager.h"
#include "TGeoNode.h"

namespace po = boost::program_options;

/*
 * Simple tool to checksum a geometry file
 */

int main( int argc, char* argv[] ) {

  try {
    po::options_description visible{"visible"};
    visible.add_options()( "help,h", "Help" )( "maxlevel", po::value<int>()->default_value( 5 ),
                                               "Max depth for checksums for checksum printout" )(
        "outfile,o", po::value<std::string>()->default_value( "-" ), "Filename for the output" );

    po::options_description hidden{"hidden"};
    hidden.add_options()( "input,i", po::value<std::string>(), "input file" );

    po::options_description opts{"options"};
    opts.add( visible ).add( hidden );

    po::positional_options_description pos_opts;
    pos_opts.add( "input", -1 );

    po::variables_map       vm;
    po::command_line_parser parser{argc, argv};
    parser.options( opts ).positional( pos_opts );
    po::parsed_options parsed = parser.run();
    po::store( parsed, vm );
    notify( vm );

    if ( vm.count( "help" ) ) {
      std::cout << "Usage: " << argv[0] << " [options] <compact file>" << std::endl;
      std::cout << visible << std::endl;
      return 0;
    }

    if ( !vm.count( "input" ) ) {
      std::cerr << "Please specify the compact file to load" << std::endl;
      return 1;
    }

    std::string input_file  = vm["input"].as<std::string>();
    std::string output_file = vm["outfile"].as<std::string>();

    dd4hep::Detector& desc = dd4hep::Detector::getInstance();
    desc.fromXML( input_file );
    std::map<std::string, std::string> checksums =
        lhcb::geometrytools::checksum( gGeoManager->GetTopNode(), vm["maxlevel"].as<int>() );

    if ( output_file == "-" ) {
      for ( auto& e : checksums ) { std::cout << e.first << ":" << e.second << std::endl; }
    } else {

      YAML::Emitter yaml;
      yaml << checksums;

      std::ofstream outfile( output_file, std::ofstream::trunc );
      outfile << yaml.c_str();
      outfile.close();
    }
  } catch ( const po::error& ex ) {
    std::cerr << ex.what() << '\n';
    return 1;
  }

  return 0;
}
