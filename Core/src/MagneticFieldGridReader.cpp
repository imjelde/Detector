/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/MagneticFieldGridReader.h"
#include "Core/MagneticFieldGrid.h"
#include "DD4hep/DD4hepUnits.h"
#include "DD4hep/Printout.h"
#include "Math/Vector3D.h"

#include <cstring>
#include <fstream>
#include <string>

//=============================================================================
// Read the field map files and scale by scaleFactor
//=============================================================================

////////////////////////////////////////////////////////////////////////////////////////
// structure that holds the grid parameters for one quadrant (a la DC06)
////////////////////////////////////////////////////////////////////////////////////////
struct GridQuadrant {
  std::vector<ROOT::Math::XYZVector> Q;
  double                             zOffset{0};
  double                             Dxyz[3]{{}};
  size_t                             Nxyz[3]{{}};
};

LHCb::Magnet::MagneticFieldGridReader::MagneticFieldGridReader( std::string field_map_path ) {
  m_mapFilePath = field_map_path;
}

bool LHCb::Magnet::MagneticFieldGridReader::readFiles( const std::vector<std::string>&  filenames,
                                                       LHCb::Magnet::MagneticFieldGrid& grid ) const {
  assert( filenames.size() == 4 );

  GridQuadrant quadrants[4];
  bool         sc = true;
  for ( int iquad = 0; iquad < 4 && sc; ++iquad ) { sc = readQuadrant( filenames[iquad], quadrants[iquad] ); }

  // check that the quadrants are consistent
  if ( sc ) {
    for ( int iquad = 1; iquad < 4; ++iquad ) {
      assert( quadrants[0].zOffset == quadrants[iquad].zOffset );
      for ( size_t icoord = 0; icoord < 3; ++icoord ) {
        assert( quadrants[0].Dxyz[icoord] == quadrants[iquad].Dxyz[icoord] );
        assert( quadrants[0].Nxyz[icoord] == quadrants[iquad].Nxyz[icoord] );
      }
    }

    // now fill the grid
    fillGridFromQuadrants( quadrants, grid );
  }

  return sc;
}

bool LHCb::Magnet::MagneticFieldGridReader::readDC06File( const std::string&               filename,
                                                          LHCb::Magnet::MagneticFieldGrid& grid ) const {
  GridQuadrant quadrants[4];
  // read the first quadrant
  bool sc = readQuadrant( filename, quadrants[0] );

  if ( sc ) {
    // multiply by 4
    for ( size_t iquad = 1; iquad < 4; ++iquad ) quadrants[iquad] = quadrants[0];

    // now fill the grid
    fillGridFromQuadrants( quadrants, grid );
  }

  return sc;
}

void LHCb::Magnet::MagneticFieldGridReader::fillConstantField( const ROOT::Math::XYZVector& /* field */,
                                                               LHCb::Magnet::MagneticFieldGrid& grid ) const {
  // make a grid that spans the entire world
  auto Dxyz      = std::array<float, 3>{2 * dd4hep::km, 2 * dd4hep::km, 2 * dd4hep::km};
  grid.m_invDxyz = {1.0f / Dxyz[0], 1.0f / Dxyz[1], 1.0f / Dxyz[2], 0.0f};
  grid.m_min     = {-dd4hep::km, -dd4hep::km, -dd4hep::km, 0.0f};
  grid.m_Nxyz    = {2, 2, 2};
  grid.m_B.clear();
  grid.m_B.resize( grid.m_Nxyz[0] * grid.m_Nxyz[1] * grid.m_Nxyz[2], {0, 0, 0, 0} );
}

////////////////////////////////////////////////////////////////////////////////////////
// routine to fill the grid from the 4 quadrants
////////////////////////////////////////////////////////////////////////////////////////

void LHCb::Magnet::MagneticFieldGridReader::fillGridFromQuadrants( GridQuadrant*                    quadrants,
                                                                   LHCb::Magnet::MagneticFieldGrid& grid ) const {
  // flip the signs of the field such that they are all in the same frame
  for ( size_t iquad = 1; iquad < 4; ++iquad ) {
    int signx( 1 ), signz( 1 );
    switch ( iquad ) {
    case 1:
      signx = -1;
      break;
    case 2:
      signx = -1;
      signz = -1;
      break;
    case 3:
      signz = -1;
    }
    for ( auto& Q : quadrants[iquad].Q ) Q = {signx * Q.x(), Q.y(), signz * Q.z()};
  }

  // now we remap: put the 4 quadrants in a single grid
  const size_t Nxquad = quadrants[0].Nxyz[0];
  const size_t Nyquad = quadrants[0].Nxyz[1];
  const size_t Nzquad = quadrants[0].Nxyz[2];

  // new number of bins. take into account that they overlap at z axis
  auto Dxyz      = std::array{(float)quadrants[0].Dxyz[0], (float)quadrants[0].Dxyz[1], (float)quadrants[0].Dxyz[2]};
  using N        = LHCb::Magnet::MagneticFieldGrid::IndexType;
  grid.m_invDxyz = {1.0f / Dxyz[0], 1.0f / Dxyz[1], 1.0f / Dxyz[2], 0.0f};
  grid.m_Nxyz    = {N( 2 * Nxquad - 1 ), N( 2 * Nyquad - 1 ), N( Nzquad ), 0};
  grid.m_B.resize( ( std::size_t )( grid.m_Nxyz[0] * grid.m_Nxyz[1] * grid.m_Nxyz[2] ), {0, 0, 0, 0} );
  for ( size_t iz = 0; iz < Nzquad; ++iz )
    for ( size_t iy = 0; iy < Nyquad; ++iy )
      for ( size_t ix = 0; ix < Nxquad; ++ix ) {
        // 4th quadrant (negative x, negative y)
        const auto& Q4 = quadrants[3].Q[Nxquad * ( Nyquad * iz + iy ) + ix];
        grid.m_B[grid.m_Nxyz[0] * ( grid.m_Nxyz[1] * iz + ( Nyquad - iy - 1 ) ) + ( Nxquad - ix - 1 )] = {
            (float)Q4.x(), (float)Q4.y(), (float)Q4.z(), 0.0f};
        // 2nd quadrant (negative x, positive y)
        const auto& Q2 = quadrants[1].Q[Nxquad * ( Nyquad * iz + iy ) + ix];
        grid.m_B[grid.m_Nxyz[0] * ( grid.m_Nxyz[1] * iz + ( Nyquad + iy - 1 ) ) + ( Nxquad - ix - 1 )] = {
            (float)Q2.x(), (float)Q2.y(), (float)Q2.z(), 0.0f};
        // 3rd quadrant (postive x, negative y)
        const auto& Q3 = quadrants[2].Q[Nxquad * ( Nyquad * iz + iy ) + ix];
        grid.m_B[grid.m_Nxyz[0] * ( grid.m_Nxyz[1] * iz + ( Nyquad - iy - 1 ) ) + ( Nxquad + ix - 1 )] = {
            (float)Q3.x(), (float)Q3.y(), (float)Q3.z(), 0.0f};
        // 1st quadrant (positive x, positive y)
        const auto& Q1 = quadrants[0].Q[Nxquad * ( Nyquad * iz + iy ) + ix];
        grid.m_B[grid.m_Nxyz[0] * ( grid.m_Nxyz[1] * iz + ( Nyquad + iy - 1 ) ) + ( Nxquad + ix - 1 )] = {
            (float)Q1.x(), (float)Q1.y(), (float)Q1.z(), 0.0f};
      }
  grid.m_min = {-( ( Nxquad - 1 ) * Dxyz[0] ), -( ( Nyquad - 1 ) * Dxyz[1] ), (float)quadrants[0].zOffset};

  dd4hep::printout( dd4hep::INFO, "MagneticFieldGridReader", "Field grid , nbins x,y,z  : (%d, %d, %d)", grid.m_Nxyz[0],
                    grid.m_Nxyz[1], grid.m_Nxyz[2] );
  dd4hep::printout( dd4hep::INFO, "MagneticFieldGridReader", "dx, xmin, xmax: (%f, %f, %f)", Dxyz[0], grid.m_min[0],
                    grid.m_min[0] + ( grid.m_Nxyz[0] - 1 ) * Dxyz[0] );
  dd4hep::printout( dd4hep::INFO, "MagneticFieldGridReader", "dy, ymin, ymax: (%f, %f, %f)", Dxyz[0], grid.m_min[1],
                    grid.m_min[1] + ( grid.m_Nxyz[1] - 1 ) * Dxyz[1] );
  dd4hep::printout( dd4hep::INFO, "MagneticFieldGridReader", "dz, zmin, zmax: (%f, %f, %f)", Dxyz[0], grid.m_min[2],
                    grid.m_min[2] + ( grid.m_Nxyz[2] - 1 ) * Dxyz[2] );
}

////////////////////////////////////////////////////////////////////////////////////////
// read the data for a single quadrant from a file
////////////////////////////////////////////////////////////////////////////////////////

bool LHCb::Magnet::MagneticFieldGridReader::readQuadrant( const std::string& filename, GridQuadrant& quad ) const {

  // Fix this !
  std::string full_filename = m_mapFilePath + "/" + filename;
  dd4hep::printout( dd4hep::WARNING, "MagneticFieldGridReader", "FieldMapFile:  %s", full_filename.c_str() );
  // open the file
  std::ifstream infile( full_filename.c_str() );

  if ( infile ) {
    dd4hep::printout( dd4hep::INFO, "MagneticFieldGridReader", "Opened magnetic field file:  %s",
                      full_filename.c_str() );

    // Skip the header till PARAMETER
    char line[255];
    do { infile.getline( line, 255 ); } while ( line[0] != 'P' );

    // Get the PARAMETER
    std::string sPar[2];
    char*       token = strtok( line, " " );
    int         ip    = 0;
    do {
      if ( token ) {
        sPar[ip] = token;
        token    = strtok( nullptr, " " );
      } else
        continue;
      ip++;
    } while ( token != nullptr );
    long int npar = atoi( sPar[1].c_str() );

    // Skip the header till GEOMETRY
    do { infile.getline( line, 255 ); } while ( line[0] != 'G' );

    // Skip any comment before GEOMETRY
    do { infile.getline( line, 255 ); } while ( line[0] != '#' );

    // Get the GEOMETRY
    infile.getline( line, 255 );
    std::string sGeom[7];
    token  = strtok( line, " " );
    int ig = 0;
    do {
      if ( token ) {
        sGeom[ig] = token;
        token     = strtok( nullptr, " " );
      } else
        continue;
      ig++;
    } while ( token != nullptr );

    // Grid dimensions are given in cm in CDF file. Convert to CLHEP units

    quad.Dxyz[0] = atof( sGeom[0].c_str() ) * cm;
    quad.Dxyz[1] = atof( sGeom[1].c_str() ) * cm;
    quad.Dxyz[2] = atof( sGeom[2].c_str() ) * cm;
    quad.Nxyz[0] = atoi( sGeom[3].c_str() );
    quad.Nxyz[1] = atoi( sGeom[4].c_str() );
    quad.Nxyz[2] = atoi( sGeom[5].c_str() );
    quad.zOffset = atof( sGeom[6].c_str() ) * cm;

    // Number of lines with data to be read
    long int nlines = ( npar - 7 ) / 3;
    quad.Q.clear();
    quad.Q.reserve( nlines );

    // Skip comments and fill a vector of magnetic components for the
    // x, y and z positions given in GEOMETRY
    while ( infile ) {
      // parse each line of the file,
      // comment lines begin with '#' in the cdf file
      infile.getline( line, 255 );
      if ( line[0] == '#' ) continue;
      std::string sFx, sFy, sFz;
      char*       token = strtok( line, " " );
      if ( token ) {
        sFx   = token;
        token = strtok( nullptr, " " );
      } else
        continue;
      if ( token ) {
        sFy   = token;
        token = strtok( nullptr, " " );
      } else
        continue;
      if ( token ) {
        sFz   = token;
        token = strtok( nullptr, " " );
      } else
        continue;
      if ( token != nullptr ) continue;

      // Field values are given in gauss in CDF file. Convert to CLHEP units
      double fx = std::stod( sFx ) * gauss;
      double fy = std::stod( sFy ) * gauss;
      double fz = std::stod( sFz ) * gauss;

      // Add the magnetic field components of each point
      quad.Q.emplace_back( fx, fy, fz );
    }
    infile.close();
    if ( nlines != int( quad.Q.size() ) ) {
      dd4hep::printout( dd4hep::ERROR, "MagneticFieldGridReader", "Number of points in field map does not match" );
      return false;
    }
  } else {
    dd4hep::printout( dd4hep::ERROR, "MagneticFieldGridReader", "Unable to open magnetic field file: %s",
                      full_filename.c_str() );
    return false;
  }
  return true;
}
