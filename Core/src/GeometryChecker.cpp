/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"

#include "Math/Vector3D.h"
#include "TCanvas.h"
#include "TGeoManager.h"
#include "TGeoNavigator.h"
#include "TGraph2D.h"
#include "TImage.h"

#include <string>

/**
 * Simple plugin that dumps the materials found between two points specified on the command line,
 * e.g. by doing:
 * geoPluginRun -input compact/trunk/LHCb.xml -plugin LHCb_Check_Geometry 0.0 0.0 0.0 1000.0 1000 1000
 *
 */
static long tgeo_find_intersections( dd4hep::Detector& /* description */, int argc, char** argv ) {

  const char* myname = "CheckGeometry";

  dd4hep::printout( dd4hep::INFO, myname, "In Geometry checker" );

  if ( argc < 6 ) {
    dd4hep::printout( dd4hep::ERROR, myname, "Please specify the positions of the start and end points" );
    exit( 1 );
  }

  // Parse the start and end points
  ROOT::Math::XYZPoint p1{std::stod( argv[0] ), std::stod( argv[1] ), std::stod( argv[2] )};
  ROOT::Math::XYZPoint p2{std::stod( argv[3] ), std::stod( argv[4] ), std::stod( argv[5] )};

  // Set the initial status of the navigator
  auto nav = gGeoManager->GetCurrentNavigator();

  nav->SetCurrentPoint( p1.x(), p1.y(), p1.z() );
  auto direction = p2 - p1;
  direction /= direction.R();
  nav->SetCurrentDirection( direction.x(), direction.y(), direction.z() );
  bool nav_finished = false;

  // Set the navigator state, finding the node at the ciurrent position
  nav->FindNode();

  // Iterate until we're close enogh to the end point (1cm)
  while ( !nav_finished ) {

    const double*      ipos = nav->GetCurrentPoint();
    std::ostringstream os;
    os << ipos[0] << ", " << ipos[1] << ", " << ipos[2] << " " << nav->GetCurrentNode()->GetName() << " - "
       << nav->GetCurrentNode()->GetVolume()->GetMaterial()->GetName();

    dd4hep::printout( dd4hep::INFO, myname, "%s", os.str().c_str() );

    ROOT::Math::XYZPoint i{ipos[0], ipos[1], ipos[2]};
    nav->FindNextBoundaryAndStep( sqrt( ( i - p2 ).Mag2() ) );
    const double*        pos = nav->GetCurrentPoint();
    ROOT::Math::XYZPoint p{pos[0], pos[1], pos[2]};
    nav_finished = ( ( p - p2 ).Mag2() < 1.0 );
  }

  return 1;
}
DECLARE_APPLY( find_intersections, tgeo_find_intersections )

static long tgeo_scan( dd4hep::Detector& /* description */, int argc, char** argv ) {

  ROOT::Math::XYZPoint start_point{0, 0, 0}, end_point{0, 0, 0};
  double               nb_points = 10;
  bool                 help      = false;
  bool                 error     = false;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( 0 == ::strncmp( "-help", argv[i], 4 ) ) { help = true; }
  }

  if ( argc == 7 ) {
    start_point = ROOT::Math::XYZPoint( atof( argv[0] ), atof( argv[1] ), atof( argv[2] ) );
    end_point   = ROOT::Math::XYZPoint( atof( argv[3] ), atof( argv[4] ), atof( argv[5] ) );
    nb_points   = atof( argv[6] );
  } else {
    error = true;
  }

  if ( help || error ) {
    /// Help printout describing the basic command line interface
    std::cout << "Usage: -plugin <name>  xstart ystart zstart xend yend zend          \n"
                 "     name:   factory name     find_intersections                    \n"
                 "     -help                    Show this help.                       \n"
                 "\tArguments given: "
              << dd4hep::arguments( argc, argv ) << std::endl;
    ::exit( EINVAL );
  }

  auto                  nav = gGeoManager->GetCurrentNavigator();
  ROOT::Math::XYZVector direction{end_point - start_point};
  direction /= nb_points;
  double x = start_point.x();
  double y = start_point.y();
  double z = start_point.z();
  while ( z <= end_point.z() ) {
    while ( y <= end_point.y() ) {
      while ( x <= end_point.x() ) {
        TGeoNode*          node = nav->FindNode( x, y, z );
        std::ostringstream os;
        os << "( " << x << ", " << y << ", " << z << ") - " << node->GetName() << " - "
           << node->GetVolume()->GetMaterial()->GetName();
        dd4hep::printout( dd4hep::INFO, "scan", "%s", os.str().c_str() );
        x += direction.x();
      }
      x = start_point.x();
      y += direction.y();
    }
    y = start_point.y();
    z += direction.z();
  }

  return 0;
}
DECLARE_APPLY( scan, tgeo_scan )

/**
 * Simple plugin that creates a plot of the density of the materials found in a x/y plane for the given z
 * e.g. by doing:
 * geoPluginRun -input compact/trunk/LHCb.xml -interpreter -plugin get_slice -50 50 -50 50 50 1000
 */
static long tgeo_get_slice( dd4hep::Detector& /* description */, int argc, char** argv ) {

  double xmin{0}, xmax{0}, ymin{0}, ymax{0}, z{0}, x{0}, y{0};
  int    nb_points = 10;
  bool   help      = false;
  bool   error     = false;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( 0 == ::strncmp( "-help", argv[i], 4 ) ) { help = true; }
  }

  if ( argc == 6 ) {
    xmin      = atof( argv[0] );
    xmax      = atof( argv[1] );
    ymin      = atof( argv[2] );
    ymax      = atof( argv[3] );
    z         = atof( argv[4] );
    nb_points = atof( argv[5] );
  } else {
    error = true;
  }

  if ( help || error ) {
    /// Help printout describing the basic command line interface
    std::cout << "Usage: -plugin <name>  xmin xmax ymin ymax z nb_points              \n"
                 "     name:   factory name     get_slice                             \n"
                 "     -help                    Show this help.                       \n"
                 "\tArguments given: "
              << dd4hep::arguments( argc, argv ) << std::endl;
    ::exit( EINVAL );
  }

  TGraph2D* plot = new TGraph2D( nb_points * nb_points );
  plot->SetNpx( nb_points );
  plot->SetNpy( nb_points );
  plot->SetTitle( "" );

  int    p     = 0;
  double stepx = ( xmax - xmin ) / nb_points;
  double stepy = ( ymax - ymin ) / nb_points;

  for ( x = xmin + stepx / 2; x < xmax; x += stepx ) {
    for ( y = ymin + stepy / 2; y < ymax; y += stepy ) {
      auto node = gGeoManager->FindNode( x, y, z );
      if ( node ) {
        double d = node->GetMedium()->GetMaterial()->GetDensity();
        plot->SetPoint( p++, x, y, d );
      }
    }
  }
  plot->Draw( "COLZ" );
  TImage* img = TImage::Create();
  img->FromPad( gPad );
  img->WriteImage( "slice.png" );
  return 0;
}
DECLARE_APPLY( get_slice, tgeo_get_slice )