/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Framework include files
#include "Core/DeIOV.h"
#include "Core/Keys.h"
#include "Core/PrintHelpers.h"

#include "DD4hep/Alignments.h"
#include "DD4hep/AlignmentsPrinter.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsDebug.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/GrammarUnparsed.h"
#include "DD4hep/Printout.h"
#include "DD4hep/detail/AlignmentsInterna.h"

#include <map>
#include <sstream>
#include <vector>

namespace {
  ROOT::Math::Transform3D toTransform3D( const TGeoHMatrix& m ) {
    const auto*               trans = m.GetTranslation();
    ROOT::Math::Translation3D t{trans[0], trans[1], trans[2]};
    const auto*               rot = m.GetRotationMatrix();
    ROOT::Math::Rotation3D    r{rot[0], rot[1], rot[2], rot[3], rot[4], rot[5], rot[6], rot[7], rot[8]};
    return {r, t};
  }
} // namespace

/// Initialization of sub-classes
LHCb::Detector::detail::DeIOVObject::DeIOVObject( dd4hep::DetElement de, dd4hep::cond::ConditionUpdateContext& ctxt,
                                                  int classID, bool hasAlignment )
    :
#if !defined( DD4HEP_MINIMAL_CONDITIONS )
    ConditionObject( Keys::deKeyName )
    ,
#endif
    clsID( classID )
    , detector( de )
    , geometry( de.placement() ) {
  hash = LHCb::Detector::ConditionKey( de, Keys::deKey );
  data.bindExtern( this );

  if ( hasAlignment ) {
    auto kalign       = LHCb::Detector::ConditionKey( detector, Keys::alignmentKey );
    detectorAlignment = ctxt.condition( kalign );
  } else {
    detectorAlignment = de.nominal();
  }

  if ( detectorAlignment.isValid() ) {
    toGlobalMatrix = toTransform3D( detectorAlignment.worldTransformation() );
    toLocalMatrix  = toGlobalMatrix.Inverse();
  }

  toGlobalMatrixNominal = toTransform3D( detector.nominal().worldTransformation() );
  toLocalMatrixNominal  = toGlobalMatrixNominal.Inverse();
  TGeoHMatrix geoDelta{};
  detectorAlignment.delta().computeMatrix( geoDelta );
  delta = toTransform3D( geoDelta );
}

/// Printout method to stdout
void LHCb::Detector::detail::DeIOVObject::print( int indent, int flg ) const {
  std::string prefix = getIndentation( indent );
  dd4hep::printout( dd4hep::INFO, "DeIOV", "%s*========== Detector:%s", prefix.c_str(), detector.path().c_str() );
  dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+ Name:%s Hash:%016lX Type:%s Flags:%08X IOV:%s", prefix.c_str(),
                    dd4hep::cond::cond_name( this ).c_str(), hash, is_bound() ? data.dataType().c_str() : "<UNBOUND>",
                    flags, iov ? iov->str().c_str() : "--" );
  if ( flg & BASICS ) {
    const dd4hep::DetElement::Children& c = detector.children();
    dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+ Detector:%s #Dau:%d", prefix.c_str(), detector.name(),
                      int( c.size() ) );
  }
  if ( flg & DETAIL ) {
    dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+  >> Alignment:%s", prefix.c_str(),
                      dd4hep::yes_no( detectorAlignment.isValid() ) );
    if ( detectorAlignment.isValid() ) {
      char                         txt1[64], txt2[64], txt3[64];
      std::stringstream            str;
      dd4hep::Alignment::Object*   ptr            = detectorAlignment.ptr();
      const dd4hep::AlignmentData& alignment_data = detectorAlignment.data();
      const dd4hep::Delta&         D              = alignment_data.delta;

      if ( D.hasTranslation() )
        ::snprintf( txt1, sizeof( txt1 ), "Tr: x:%g y:%g z:%g ", D.translation.x(), D.translation.Y(),
                    D.translation.Z() );
      else
        ::snprintf( txt1, sizeof( txt1 ), "Tr:    ------- " );
      if ( D.hasRotation() )
        ::snprintf( txt2, sizeof( txt2 ), "Rot: phi:%g psi:%g theta:%g ", D.rotation.Phi(), D.rotation.Psi(),
                    D.rotation.Theta() );
      else
        ::snprintf( txt2, sizeof( txt2 ), "Rot:   ------- " );
      if ( D.hasPivot() )
        ::snprintf( txt3, sizeof( txt3 ), "Rot: x:%g y:%g z:%g ", D.pivot.Vect().X(), D.pivot.Vect().Y(),
                    D.pivot.Vect().Z() );
      else
        ::snprintf( txt3, sizeof( txt3 ), "Pivot: ------- " );

      dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+  >> Aligment [%p] Typ:%s \tData:(%11s-%8s-%5s)", prefix.c_str(),
                        detectorAlignment.ptr(), dd4hep::typeName( typeid( *ptr ) ).c_str(),
                        D.hasTranslation() ? "Translation" : "", D.hasRotation() ? "Rotation" : "",
                        D.hasPivot() ? "Pivot" : "" );
      if ( D.hasTranslation() || D.hasRotation() || D.hasPivot() ) {
        dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+  >> Aligment-Delta %s %s %s", prefix.c_str(), txt1, txt2, txt3 );
      }
    }
  }
}
