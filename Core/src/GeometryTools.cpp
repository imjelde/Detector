/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/GeometryTools.h"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include "TClass.h"
#include "TGeoBBox.h"
#include "TGeoManager.h"
#include "TGeoMatrix.h"
#include "TGeoNode.h"

#include "DD4hep/Shapes.h"

#include <openssl/sha.h>

/**
 *  Utilities to serialize the various ROOT geometry classes
 */
namespace {

  void serialize( TGeoMatrix& m, std::stringstream& stream ) {

    stream << ":mx:" << std::setprecision( 3 );
    const Double_t* rot = m.GetRotationMatrix();
    const Double_t* tr  = m.GetTranslation();

    for ( int i = 0; i < 9; i++ ) { stream << rot[i]; }
    for ( int i = 0; i < 3; i++ ) { stream << tr[i]; }

    if ( m.IsScale() ) {
      const Double_t* scl = m.GetScale();
      for ( int i = 0; i < 3; i++ ) { stream << scl[i]; }
    }
  }

  void serialize( TGeoMaterial& m, std::stringstream& stream ) { stream << ":mat:" << m.GetName(); }

  void serialize( TGeoMedium& m, std::stringstream& stream ) {
    stream << ":med:" << m.GetName();
    serialize( *( m.GetMaterial() ), stream );
  }

  void serialize( TGeoShape& n, std::stringstream& stream ) {
    stream << ":shape:" << n.IsA()->GetName() << std::setprecision( 3 );
    std::vector<double> dimensions = dd4hep::get_shape_dimensions( &n );
    for ( auto& d : dimensions ) { stream << d; }
  }

  void serialize( TGeoNode& n, std::stringstream& stream ) {
    stream << n.GetName();
    serialize( *( n.GetMatrix() ), stream );
    serialize( *( n.GetMedium() ), stream );
    serialize( *( n.GetVolume()->GetShape() ), stream );
  }

} // namespace

std::string lhcb::geometrytools::toString( TGeoNode* node ) {
  std::stringstream nodestr;
  serialize( *node, nodestr );
  return nodestr.str();
}

std::map<std::string, std::string> lhcb::geometrytools::checksum( TGeoNode* node, int maxlevel ) {

  std::map<std::string, std::string> checksums;

  // callback to compute the checkum of a node and its children checksums
  auto compute_checksum = [&checksums, maxlevel]( TGeoNode* n, const nodeMap<std::string>& daughters, std::string path,
                                                  int level ) -> std::string {
    // Preparing the context for the SHA algorithm
    unsigned char md[SHA256_DIGEST_LENGTH]; // 32 bytes
    SHA256_CTX    context;
    if ( !SHA256_Init( &context ) ) { throw new std::exception(); }

    // Checkum of the node
    std::string chk = lhcb::geometrytools::toString( n );
    SHA256_Update( &context, (unsigned char*)chk.c_str(), chk.size() );

    for ( auto d : daughters ) {
      std::string checksum = d.second;
      SHA256_Update( &context, (unsigned char*)checksum.c_str(), checksum.size() );
    }

    if ( !SHA256_Final( md, &context ) ) { throw new std::exception(); }
    std::stringstream shastr;
    shastr << std::hex << std::setfill( '0' );
    for ( const auto& byte : md ) { shastr << std::setw( 2 ) << (int)byte; }

    if ( level < maxlevel ) { checksums.emplace( path, shastr.str() ); }

    return shastr.str();
  };

  traverse( node, compute_checksum );
  return checksums;
}
