/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/MagneticFieldExtension.h"
#include "Core/MagneticFieldGrid.h"
#include "Core/MagneticFieldGridReader.h"

#include "Detector/Magnet/DeMagnet.h"

#include "Core/Keys.h"
#include "DD4hep/DetElement.h"
#include "DD4hep/Printout.h"

#include <yaml-cpp/yaml.h>

void LHCb::Magnet::setup_magnetic_field_extension( dd4hep::Detector& description, std::string field_map_path,
                                                   double nominal_current ) {

  dd4hep::DetElement magnetdet = description.detector( "Magnet" );
  using Ext_t                  = LHCb::Magnet::MagneticFieldExtension;

  auto mfhelper = new LHCb::Magnet::MagneticFieldExtension( description, field_map_path, nominal_current );
  magnetdet.addExtension( new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>( mfhelper ) );
}

std::vector<std::string> LHCb::Magnet::dependency_list() {
  return {"Measured", "FieldMapFilesUp", "FieldMapFilesDown", "ScaleDown", "ScaleUp"};
}

/*
 * Method to load the magnetic field file based on the conditions
 */
std::shared_ptr<LHCb::Magnet::MagneticFieldGrid> LHCb::Magnet::MagneticFieldExtension::load_magnetic_field_grid(
    dd4hep::cond::ConditionUpdateContext& context, bool& useRealMap, bool& isDown, double& signedRelativeCurrent ) {

  LHCb::Magnet::MagneticFieldGridReader reader{m_magneticFieldFilesLocation};
  auto                                  grid = std::make_shared<LHCb::Magnet::MagneticFieldGrid>();

  // Getting the Magnet DetectorElement
  dd4hep::DetElement magnetdet = m_description.detector( "Magnet" );

  // Loading the current value
  // XXX We not have the Measured/Set distinction in the upgrade condition ?
  // auto magnet_meascond = slice.get( magnetdet, LHCb::Detector::item_key( "Measured" ) );
  // XX why the diff in interface between context and slice
  dd4hep::Condition magnet_meascond = context.condition( LHCb::Detector::ConditionKey( magnetdet, "Measured" ) );
  YAML::Node*       magnet_measdoc  = static_cast<YAML::Node*>( magnet_meascond->payload() );
  double            current         = ( *magnet_measdoc )["Current"].as<double>();
  int               polarity        = ( *magnet_measdoc )["Polarity"].as<int>();
  dd4hep::printout( dd4hep::DEBUG, "MagneticFieldExtension", "Current value: %f", current );
  dd4hep::printout( dd4hep::DEBUG, "MagneticFieldExtension", "Current polarity: %d", polarity );

  // Loading the appropriate map file list depending on the polarity
  std::string mapCondName = polarity > 0 ? "FieldMapFilesUp" : "FieldMapFilesDown";
  // auto magnet_filescond  = slice.get( magnetdet, LHCb::Detector::item_key( mapCondName ) );
  dd4hep::Condition magnet_filescond = context.condition( LHCb::Detector::ConditionKey( magnetdet, mapCondName ) );
  YAML::Node*       magnet_filesdoc  = static_cast<YAML::Node*>( magnet_filescond->payload() );
  const auto        filenames        = ( *magnet_filesdoc )["Files"].as<std::vector<std::string>>();

  // Loading the ScaleUp/ScaleDown attributes
  std::string       scaleCondName    = polarity > 0 ? "ScaleUp" : "ScaleDown";
  dd4hep::Condition magnet_scalecond = context.condition( LHCb::Detector::ConditionKey( magnetdet, scaleCondName ) );
  YAML::Node*       magnet_scaledoc  = static_cast<YAML::Node*>( magnet_scalecond->payload() );
  const auto        coeffs           = ( *magnet_scaledoc )["Coeffs"].as<std::vector<double>>();

  // Computing the scale factor for the magnetic field
  double scale_factor = coeffs[0] + ( coeffs[1] * ( current / m_nominalCurrent ) );
  dd4hep::printout( dd4hep::INFO, "MagneticFieldExtension", "Scale factor: %f", scale_factor );
  grid->setScaleFactor( scale_factor );

  // loading the files into the grid
  const auto sc = ( filenames.size() == 1 ? reader.readDC06File( filenames.front(), *grid )
                                          : reader.readFiles( filenames, *grid ) );
  if ( !sc ) {
    dd4hep::printout( dd4hep::ERROR, "MagneticFieldExtension", "Error loading magnetic field map" );
    dd4hep::except( "load_magnetic_field_map", "Error loading magnetic field map" );
  }

  // Imported from the MagneticFieldSvc
  /// Setting constants
  useRealMap = ( filenames.size() == 4 );
  isDown     = grid->fieldVectorClosestPoint( {0, 0, 5200} ).y() < 0;

  // current
  const int sign        = ( isDown ? -1 : +1 );
  signedRelativeCurrent = std::abs( scale_factor ) * sign;

  return grid;
}
