//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================

// Framework include files
#include "Core/ConditionsLoader.h"
#include "Core/ConditionsReaderContext.h"
#include "Core/ConditionsRepository.h"

// Other dd4hep includes
#include "Core/UpgradeTags.h"
#include "DD4hep/Operators.h"
#include "DD4hep/PluginCreators.h"
#include "DD4hep/Printout.h"
#include "DD4hep/detail/ConditionsInterna.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsTags.h"
#include "XML/XML.h"

#include "TTimeStamp.h"

#include <Core/yaml_converters.h>
#include <exception>
#include <set>
#include <sstream>
#include <string_view>
#include <utility>
#include <vector>

/// Standard constructor, initializes variables
LHCb::Detector::ConditionsLoader::ConditionsLoader( dd4hep::Detector& description, dd4hep::cond::ConditionsManager mgr,
                                                    const std::string& nam )
    : ConditionsDataLoader( description, mgr, nam ) {
  declareProperty( "ReaderType", m_readerType );
  declareProperty( "Directory", m_directory );
  declareProperty( "DBTag", m_dbtag );
  declareProperty( "Match", m_match );
  declareProperty( "IOVType", m_iovTypeName );
  declareProperty( "UseOverlay", m_useOverlay );
}

/// Access a conditions loader for a particular class ID
LHCb::Detector::ConditionConverter& LHCb::Detector::ConditionsLoader::conditionConverter( int class_id ) {
  auto icnv = m_converters.find( class_id );
  if ( icnv == m_converters.end() ) {
    std::stringstream str;
    str << "LHCb_ConditionsConverter_" << class_id;
    ConditionConverter* cnv = dd4hep::createPlugin<ConditionConverter>( str.str(), m_detector, 0, 0 );
    if ( cnv ) {
      m_converters.insert( std::make_pair( class_id, cnv ) );
      return *cnv;
    }
    dd4hep::except( "ConditionsLoader", "Failed to create ConditionConverter for class ID:%d", class_id );
  }
  return *( ( *icnv ).second );
}

/// Initialize loader according to user information
void LHCb::Detector::ConditionsLoader::initialize() {
  std::string typ    = m_readerType;
  const void* argv[] = {"ConditionsReader", this, 0};
  m_reader.reset( dd4hep::createPlugin<ConditionsReader>( typ, m_detector, 2, argv ) );
  ( *m_reader )["Directory"] = m_directory;
  ( *m_reader )["Match"]     = m_match;
  ( *m_reader )["DBTag"]     = m_dbtag;
  m_iovType                  = manager().iovType( m_iovTypeName );
  if ( !m_iovType ) {
    // Error
  }
  if ( m_useOverlay ) { dd4hep::printout( dd4hep::DEBUG, "ConditionsLoader", "overlay enabled" ); }
}

/// Optimized update using conditions slice data
size_t LHCb::Detector::ConditionsLoader::load_many( const dd4hep::IOV& req_iov, RequiredItems& conditions_to_load,
                                                    LoadedItems& loaded, dd4hep::IOV& /* conditions_validity */ ) {
  // the context is used to pass the requested event time to the UriReader
  // and to carry back the matching IOV (since and until)...
  // it's both an input and an output variable to the reader calls... if only and interface could be more confusing
  ConditionsReaderContext ctxt;
  ctxt.event_time = req_iov.keyData.first;

  const size_t loaded_initial_size = loaded.size();
  size_t       loaded_len          = loaded_initial_size;
  loaded.clear();

  // this is what is used by XercesC to resolve URIs included by XML documents
  // via "SYSTEM" keyword (needed only for XML parsing but done here instead of inside the loop)
  dd4hep::xml::UriContextReader xml_entity_resolver( m_reader.get(), &ctxt );

  // First collect all required URIs which need loading.
  // Since one file contains many conditions, we have
  // to create a unique set

  // container to keep track of the URLs already visited
  std::set<std::string_view> processed_urls;

  // FIXME using a stable buffer for storing the condition docs to be parsed might look like an optimization,
  // but in the case of GitCondDB (or the ConditionsFileReader) it does not really help as each request
  // creates a new string that then has to be moved into this buffer
  std::string buffer;

  bool print_results = true; // isActivePrintLevel( dd4hep::DEBUG );
  // dd4hep::setPrintLevel( dd4hep::DEBUG );

  // I have the impression ConditionsIdentifier is not a very good name, but I stick by it for the moment
  struct ToDoItem {
    ToDoItem( const dd4hep::cond::ConditionsLoadInfo* load_info )
        : cond_id{reinterpret_cast<const ConditionIdentifier*>( load_info->ptr() )} {}
    bool                       done    = false;
    const ConditionIdentifier* cond_id = nullptr;
  };

  // we convert the list of conditions to load into a map so that we can easily
  // flag as already loaded the conditions we get from each URL that are not
  // what we requested initially. So, if we ask for conditions A and B, and
  // when processing A we load B as it comes from the same URL, then we flag B
  // as done so that we can skip the iteration.
  std::map<dd4hep::Condition::key_type, ToDoItem> todo_list{conditions_to_load.begin(), conditions_to_load.end()};

  try {
    for ( auto [current_key, item] : todo_list ) { // O[N]
      const auto cond_id = item.cond_id;
      // FIXME this early skip for conditions already loaded from a URL does not convince me
      if ( item.done ) { continue; }
      if ( !cond_id ) {
        dd4hep::printout( dd4hep::INFO, "ConditionsLoader", "++ CANNOT update condition: [%16llX] [No load info]",
                          current_key );
        continue;
      }

      // set::insert returns a pair where the second tells if the insertion
      // took place (true) or the entry was already present (false)
      if ( !processed_urls.insert( cond_id->sys_id ).second ) {
        // no insertion
        continue;
      }

      // ready, set, chrono started!
      TTimeStamp start;
      // Load from the git cond db here XXX
      // FIXME: if the interface make sense, we should do something in case of failure
      if ( m_reader->load( cond_id->sys_id, &ctxt, buffer ) ) {
        auto ext = std::string_view{cond_id->sys_id};
        ext.remove_prefix( ext.find_last_of( '.' ) );
        if ( ext == ".xml" ) {
          dd4hep::ConditionKey::KeyMaker kmaker( cond_id->sys_id_hash, 0 );
          const auto*                    repo = cond_id->repository;
          xml_doc_holder_t               doc(
              xml_handler_t().parse( buffer.c_str(), buffer.length(), cond_id->sys_id.c_str(), &xml_entity_resolver ) );
          xml_h                          e = doc.root();
          std::vector<dd4hep::Condition> entity_conditons;
          entity_conditons.reserve( 1000 );
          // We implicitly assume that one file may only contain
          for ( xml_coll_t cond( e, _UC( condition ) ); cond; ++cond ) {
            xml_h       x_c        = cond;
            int         cls        = x_c.attr<int>( _LBU( classID ) );
            std::string nam        = x_c.attr<std::string>( _U( name ) );
            kmaker.values.det_key  = cond_id->sys_id_hash;
            kmaker.values.item_key = dd4hep::detail::hash32( nam );
            auto cond_ident        = repo->locations.find( kmaker.hash ); // O[log(N)]
            if ( cond_ident == repo->locations.end() ) {
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Got stray condition %s from %s", nam.c_str(),
                                cond_id->sys_id.c_str() );
              continue;
            }
            dd4hep::Condition::key_type cond_key = cond_ident->second->hash;
            dd4hep::Condition           c        = conditionConverter( cls )( m_detector, cond_ident->second, x_c );
            if ( !c.isValid() ) {
              // Will eventually have to raise an exception
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Failed to load condition %s from %s",
                                nam.c_str(), cond_id->sys_id.c_str() );
              continue;
            }
            c->hash          = cond_ident->second->hash;
            loaded[cond_key] = c;
            entity_conditons.push_back( c );
            if ( cond_key == current_key ) {
              item.done = true;
              continue;
            }
            auto cond_itr = todo_list.find( cond_key ); // O[log(N)]
            if ( cond_itr != todo_list.end() ) {
              cond_itr->second.done = true;
              continue;
            }
            dd4hep::printout( dd4hep::WARNING, "ConditionsLoader", "++ Got stray but valid condition %s from %s",
                              nam.c_str(), cond_id->sys_id.c_str() );
          }
          dd4hep::IOV::Key              iov_key( ctxt.valid_since, ctxt.valid_until );
          dd4hep::cond::ConditionsPool* pool = manager().registerIOV( *m_iovType, iov_key );
          size_t                        ret  = manager().blockRegister( *pool, entity_conditons );
          if ( ret != entity_conditons.size() ) {
            // Error!
          }
        } else if ( ext == ".yml" || ext == ".yaml" ) {
          dd4hep::ConditionKey::KeyMaker kmaker( cond_id->sys_id_hash, 0 );
          const auto*                    repo = cond_id->repository;

          // condition YAML files are expected to contain a mapping <name> -> <condition data>
          auto data = ( m_useOverlay ) ? m_overlay.get( cond_id->sys_id, buffer ) : YAML::Load( buffer );
          // prepare the store for the conditions we will register to the conditions manager
          // ... it would be nice if we had an actual type here and not a handle
          std::vector<dd4hep::Condition> entity_conditons;
          entity_conditons.reserve( data.size() );

          // loop over all conditions in the YAML document
          for ( const auto& cond_data : data ) {
            const std::string cond_name = cond_data.first.as<std::string>();

            // see if the condition is in the list of locations(?)
            kmaker.values.item_key = dd4hep::detail::hash32( cond_name );
            auto cond_ident        = repo->locations.find( kmaker.hash ); // O[log(N)]
            if ( cond_ident == repo->locations.end() ) {
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Got stray condition %s from %s",
                                cond_name.c_str(), cond_id->sys_id.c_str() );
              continue;
            }

            dd4hep::Condition c = LHCb::YAMLConverters::make_condition( cond_name, cond_data.second );
            if ( !c.isValid() ) {
              // Will eventually have to raise an exception
              dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "++ Failed to load condition %s from %s",
                                cond_name.c_str(), cond_id->sys_id.c_str() );
              continue;
            }

            // FIXME: is this not the same as kmaker.hash?
            const auto cond_key = cond_ident->second->hash;
            c->hash             = cond_key;
            // pass the condition to the caller
            loaded[cond_key] = c;
            // record the condition for bulk registration to the manager
            entity_conditons.push_back( c );
            // if this is the condition we were looking for,
            // flag the ToDoItem as done and move to the next condition in the YAML object
            if ( cond_key == current_key ) {
              item.done = true;
              continue;
            }
            // otherwise see if we are supposed to load it,
            // in which case the pending work item can be marked as already dine
            if ( auto cond_itr = todo_list.find( cond_key ); cond_itr != todo_list.end() ) {
              cond_itr->second.done = true;
              continue;
            }
            // this warning means that we found in the YAML file a condition we were not requesting
            // FIXME I do not see why this is a problem
            dd4hep::printout( dd4hep::WARNING, "ConditionsLoader", "++ Got stray but valid condition %s from %s",
                              cond_name.c_str(), cond_id->sys_id.c_str() );
          }

          // now that we converted the whole file we make a pool for the IOV of the YAML file
          // and we register the conditions we found
          // FIXME why one pool per YAML file? should it not be one pool for all conditions? (given an event time)
          dd4hep::IOV::Key iov_key( ctxt.valid_since, ctxt.valid_until );
          // FIXME where does the IOVType comes from? why it is bound the the ConditionsLoader instance?
          dd4hep::cond::ConditionsPool* pool = manager().registerIOV( *m_iovType, iov_key );
          size_t                        ret  = manager().blockRegister( *pool, entity_conditons );
          // FIXME it looks like blockRegister returns the number of conditions registered and we have to check
          //       that it's what we expect, but clearly we do not know what to do with that information
          if ( ret != entity_conditons.size() ) {
            // Error!
          }
        }
        // now that all is done (for this URL) we can stop the chrono
        TTimeStamp stop;
        dd4hep::printout( dd4hep::INFO, "ConditionsLoader", "++ Loaded %4ld conditions from %-48s [%7.3f sec]",
                          loaded.size() - loaded_len, cond_id->sys_id.c_str(), stop.AsDouble() - start.AsDouble() );
        loaded_len = loaded.size();
      }
      if ( print_results ) {
        // for each requested key we actually load (not skipped) we print all loaded conditions
        // ... including those already printed... (as we are still in the loop over todo_list)
        for ( const auto& e : loaded ) {
          const dd4hep::Condition& cond = e.second;
          dd4hep::printout( dd4hep::DEBUG, "ConditionsLoader", "++ %16llX: %s -> %s", cond.key(), cond->value.c_str(),
                            cond.name() );
        }
      }
    }
  } catch ( const std::exception& e ) {
    dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "+++ Load exception: %s", e.what() );
    throw;
  } catch ( ... ) {
    dd4hep::printout( dd4hep::ERROR, "ConditionsLoader", "+++ UNKNWON Load exception." );
    throw;
  }
  // FIXME somebody will have to explain how this is useful
  return loaded.size() - loaded_initial_size;
}

#include "DD4hep/Factories.h"

//==========================================================================
/// Plugin function
static void* create_loader( dd4hep::Detector& description, int argc, char** argv ) {
  const char*                            name = argc > 0 ? argv[0] : "ConditionsLoader";
  dd4hep::cond::ConditionsManagerObject* m    = (dd4hep::cond::ConditionsManagerObject*)( argc > 0 ? argv[1] : 0 );
  return new LHCb::Detector::ConditionsLoader( description, m, name );
}
DECLARE_DD4HEP_CONSTRUCTOR( LHCb_ConditionsLoader, create_loader )
