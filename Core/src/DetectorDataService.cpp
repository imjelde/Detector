/*****************************************************************************\
* (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TTimeStamp.h"

#include "Core/DetectorDataService.h"
#include "DD4hep/ConditionDerived.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/InstanceCount.h"
#include "DD4hep/Printout.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"

#include "DDCond/ConditionsCleanup.h"
#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsSlice.h"

#include "Core/ConditionsLoader.h"
#include "Core/ConditionsRepository.h"
#include "Core/DeAlignmentCall.h"
#include "Core/DeConditionCall.h"
#include "Core/Keys.h"

#include <stdexcept>
#include <utility>

namespace {
  struct ConditionsCleaner : dd4hep::cond::ConditionsFullCleanup {
    ConditionsCleaner( const dd4hep::IOV& iov ) : m_iov( iov ) {}
    /// Request cleanup operation of regular conditions pool
    bool operator()( const dd4hep::cond::ConditionsPool& pool ) const override { return pool.iov->contains( m_iov ); }
    const dd4hep::IOV& m_iov;
  };
} // namespace

void LHCb::Detector::DetectorDataService::initialize( std::string repositoryUrl, std::string conditionsDBtag,
                                                      bool withOverlay ) {
  m_manager["PoolType"]                 = "DD4hep_ConditionsLinearPool";
  m_manager["UserPoolType"]             = "DD4hep_ConditionsMapUserPool";
  m_manager["UpdatePoolType"]           = "DD4hep_ConditionsLinearUpdatePool";
  m_manager["LoaderType"]               = "LHCb_ConditionsLoader";
  m_manager["OutputUnloadedConditions"] = true;
  m_manager["LoadConditions"]           = true;
  m_manager.initialize();

  m_iov_typ = m_manager.registerIOVType( 0, "run" ).second;
  if ( 0 == m_iov_typ ) { dd4hep::except( "ConditionsPrepare", "++ Unknown IOV type supplied." ); }

  // Parsing the repository URL to extract the tag
  // it overrides specified as an option...
  auto repository = repositoryUrl;
  auto tag        = conditionsDBtag;
  auto found      = repositoryUrl.find( "@" );
  if ( found != std::string::npos ) {
    repository = repositoryUrl.substr( 0, found );
    tag        = repositoryUrl.substr( found + 1 );
  }
  dd4hep::printout( dd4hep::INFO, "DetectorDataService", "Using repository %s, tag:%s", repository.c_str(),
                    tag.c_str() );

  dd4hep::cond::ConditionsDataLoader& loader = m_manager.loader();
  loader["DBTag"]                            = tag;
  loader["ReaderType"]                       = "LHCb_ConditionsGitReader";
  loader["Directory"]                        = repository;
  loader["Match"]                            = "file:";
  loader["IOVType"]                          = "run";
  loader["UseOverlay"]                       = withOverlay;
  loader.initialize();

  if ( withOverlay ) {
    // I do not like it, but I do not see other ways of jumping through the plugin barrier
    // imposed by the ConditionsManager.
    // Note that we can safely static_cast as the loader type is LHCb_ConditionsLoader and
    // the property manager ensures (at least) that the property "UseOverlay" exists.
    auto loader_impl = static_cast<ConditionsLoader*>( &loader );
    m_condOverlay    = &loader_impl->overlay();
  }

  for ( const auto& det : m_detectorNames ) {
    dd4hep::DetElement de       = m_description.detector( det );
    const auto&        de_conds = *de.extension<std::shared_ptr<ConditionsRepository>>();
    m_all_conditions->merge( *de_conds );
  }
}

void LHCb::Detector::DetectorDataService::finalize() {
  // beyond this point we cannot guarantee that the loader still exists, thus we cannot use
  // the overlay
  m_condOverlay = nullptr;
  /// Clear it
  m_manager.clear();
  // Let's do the cleanup
  m_all_conditions.reset();
  m_manager.destroy();
}

std::shared_ptr<dd4hep::cond::ConditionsSlice> LHCb::Detector::DetectorDataService::get_slice( size_t iov ) {
  std::lock_guard<std::mutex> hold( m_cache_mutex );
  for ( const auto& [lowBound, hc] : m_cache ) {
    const auto& [highBound, slice] = hc;
    if ( lowBound <= iov && highBound > iov ) { return slice; }
  }
  std::shared_ptr<dd4hep::cond::ConditionsSlice> slice = load_slice( iov );
  m_cache[slice->iov().key().first]                    = {slice->iov().key().second, slice};
  return slice;
}

void LHCb::Detector::DetectorDataService::drop_slice( size_t iov ) {
  std::lock_guard<std::mutex> hold( m_cache_mutex );
  for ( const auto& [lowBound, hc] : m_cache ) {
    const auto& [highBound, slice] = hc;
    if ( lowBound <= iov && highBound > iov ) {
      // drop slice from the underlying manager
      m_manager.clean( ConditionsCleaner( slice->iov() ) );
      // drop our own shortcut to slice
      m_cache.erase( lowBound );
      break;
    }
  }
}

std::shared_ptr<dd4hep::cond::ConditionsSlice> LHCb::Detector::DetectorDataService::load_slice( size_t iov ) {
  dd4hep::IOV req_iov( m_iov_typ, iov );
  auto        slice = std::make_shared<dd4hep::cond::ConditionsSlice>( m_manager, m_all_conditions );
  TTimeStamp  start;
  /// Load the conditions
  /// dd4hep::cond::ConditionsManager::Result total = manager.prepare(req_iov,*slice);
  dd4hep::cond::ConditionsManager::Result total = m_manager.load( req_iov, *slice );
  TTimeStamp                              comp;
  total += m_manager.compute( req_iov, *slice );
  TTimeStamp                                   stop;
  dd4hep::cond::ConditionsContent::Conditions& missing = slice->missingConditions();
  for ( const auto& m : missing ) {
    dd4hep::printout( dd4hep::ERROR, "TEST", "Failed to load condition [%016llX]: %s", m.first,
                      m.second->toString().c_str() );
  }
  dd4hep::printout( dd4hep::INFO, "Statistics",
                    "+  Created/Accessed a total of %ld conditions "
                    "(S:%6ld,L:%6ld,C:%6ld,M:%ld)  Load:%7.5f sec Compute:%7.5f sec",
                    total.total(), total.selected, total.loaded, total.computed, total.missing,
                    comp.AsDouble() - start.AsDouble(), stop.AsDouble() - comp.AsDouble() );
  return slice;
}

void LHCb::Detector::DetectorDataService::update_condition( const std::string& path, const std::string& name,
                                                            YAML::Node data ) {
  if ( !m_condOverlay )
    throw std::logic_error( "update_condition cannot be invoked if the conditions overlay is not enabled" );
  m_condOverlay->update( path, name, std::move( data ) );
}
void LHCb::Detector::DetectorDataService::dump_conditions( std::filesystem::path root_dir ) {
  if ( !m_condOverlay )
    throw std::logic_error( "update_condition cannot be invoked if the conditions overlay is not enabled" );
  m_condOverlay->dump( std::move( root_dir ) );
}
void LHCb::Detector::DetectorDataService::update_condition( const dd4hep::DetElement& de, const std::string& cond,
                                                            YAML::Node data ) {
  auto key = LHCb::Detector::ConditionKey( de, cond );
  if ( auto el = std::find_if( begin( m_all_conditions->locations ), end( m_all_conditions->locations ),
                               [key]( const auto& item ) { return item.second->hash == key; } );
       el != end( m_all_conditions->locations ) ) {
    update_condition( el->second->sys_id, el->second->object, std::move( data ) );
  } else {
    throw std::invalid_argument{"cannot find " + cond + " in " + de.name() + "'s ConditionsRepository"};
  }
}
void LHCb::Detector::DetectorDataService::update_alignment( const dd4hep::DetElement& de, const dd4hep::Delta& delta ) {
  YAML::Node data;
  data.SetTag( "!alignment" );
  if ( delta.hasTranslation() ) {
    data["position"] =
        std::vector<double>{delta.translation.X() * 10.0, delta.translation.Y() * 10.0, delta.translation.Z() * 10.0};
    data["position"].SetStyle( YAML::EmitterStyle::Flow );
  }
  if ( delta.hasRotation() ) {
    data["rotation"] = std::vector<double>{delta.rotation.Psi(), delta.rotation.Theta(), delta.rotation.Phi()};
    data["rotation"].SetStyle( YAML::EmitterStyle::Flow );
  }
  if ( delta.hasPivot() ) {
    data["pivot"] = std::vector<double>{delta.pivot.Vect().X(), delta.pivot.Vect().Y(), delta.pivot.Vect().Z()};
    data["pivot"].SetStyle( YAML::EmitterStyle::Flow );
  }
  // FIXME: I would prefer to use dd4hep::align::Keys instead of LHCb::Detector::Keys
  update_condition( de, LHCb::Detector::Keys::deltaName, std::move( data ) );
}
