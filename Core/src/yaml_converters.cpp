/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Core/yaml_converters.h>

#include <DD4hep/AlignmentData.h>
#include <DD4hep/GrammarUnparsed.h>
#include <DD4hep/Objects.h>
#include <DD4hep/Printout.h>
#include <DD4hep/detail/ConditionsInterna.h>
#include <Evaluator/Evaluator.h>

#include <exception>
#include <fmt/core.h>
#include <functional>
#include <string>
#include <string_view>
#include <unordered_map>

// this is silly, but DD4help provides this forward declaration only in DD4hep/GrammarParsed.h
// which cannot be included if you want DD4hep/GrammarUnparsed.h
namespace dd4hep {
  const dd4hep::tools::Evaluator& g4Evaluator();
}

namespace {
  auto eval( const YAML::Node& node ) {
    auto [status, result] = dd4hep::g4Evaluator().evaluate( node.as<std::string>() );
    if ( status != dd4hep::tools::Evaluator::OK )
      throw std::runtime_error( fmt::format( "invalid expression '{}'", node.as<std::string>() ) );
    return result;
  }

  // the decltype is to use the return type of the evaluator (whatever it is)
  static constexpr decltype( eval( YAML::Node{} ) ) g4_to_dd4hep_length_ratio =
#ifdef DD4HEP_USE_GEANT4_UNITS
      1.0;
#else
      0.1;
#endif

  static std::unordered_map<std::string, LHCb::YAMLConverters::Converter> s_converters{
      // Converter for default YAML Tag (i.e. tag not specified).
      {"?",
       []( std::string_view name, const YAML::Node& cond_data ) {
         dd4hep::Condition c{std::string{name}, ""};

         auto& payload = c.bind<YAML::Node>();

         payload = cond_data;
         return c;
       }},
      {"!alignment", []( std::string_view name, const YAML::Node& cond_data ) {
         using Delta = dd4hep::Delta;
         dd4hep::Condition c{std::string{name}, "alignment"};
         auto&             delta = c.bind<Delta>();
         for ( const auto& param : cond_data ) {
           const auto param_name = param.first.as<std::string>();
           const auto x = eval( param.second[0] ), y = eval( param.second[1] ), z = eval( param.second[2] );
           if ( param_name == "position" || param_name == "dPosXYZ" ) {
             delta.translation = dd4hep::Position{x, y, z} * g4_to_dd4hep_length_ratio;
             delta.flags |= Delta::HAVE_TRANSLATION;
           } else if ( param_name == "rotation" || param_name == "dRotXYZ" ) {
             delta.rotation = dd4hep::RotationZYX{z, y, x};
             delta.flags |= Delta::HAVE_ROTATION;
           } else if ( param_name == "pivot" || param_name == "pivotXYZ" ) {
             delta.pivot = Delta::Pivot{x * g4_to_dd4hep_length_ratio, y * g4_to_dd4hep_length_ratio,
                                        z * g4_to_dd4hep_length_ratio};
             delta.flags |= Delta::HAVE_PIVOT;
           } else {
             dd4hep::printout( dd4hep::ERROR, "Delta", "++ Unknown alignment conditions field: %s",
                               param_name.c_str() );
           }
         }
         c->setFlag( dd4hep::Condition::ALIGNMENT_DELTA );
         return c;
       }}};
} // namespace

dd4hep::Condition LHCb::YAMLConverters::make_condition( std::string_view name, const YAML::Node& cond_data ) {
  const auto& converter = s_converters.find( cond_data.Tag() );
  if ( converter == end( s_converters ) )
    throw std::runtime_error( fmt::format( "no converter for {}", cond_data.Tag() ) );
  return converter->second( name, cond_data );
}

void LHCb::YAMLConverters::set_converter( std::string_view tag, Converter converter ) {
  s_converters[std::string{tag}] = converter;
}
