/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <exception>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>

#include "Core/DetectorDataService.h"
#include "Core/GeometryTools.h"

#include "DD4hep/Detector.h"
#include "DD4hep/Printout.h"

#include "Math/Vector3D.h"
#include "TGeoManager.h"
#include "TGeoNode.h"
#include "TGeoVolume.h"

int main() {

  dd4hep::Detector& desc = dd4hep::Detector::getInstance();
  desc.fromXML( "tests/testscope/scope.xml" );

  std::map<std::string, std::string> checksums;
  checksums = lhcb::geometrytools::checksum( gGeoManager->GetTopNode(), 5 );

  for ( auto& e : checksums ) { std::cout << e.first << ":" << e.second << std::endl; }

  int retval = 0;
  if ( checksums["/world"] != "beb9460fda2d8dda4d18a5f3d80bddcb8c4d3b2e2d0dd67c0747993357889710" ) { retval = 1; }
  return retval;
}
