/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <iostream>

#include "GitCondDB.h"

int main() {

  auto db = GitCondDB::v1::connect( "file:tests/ConditionsIOV/Conditions" );

  std::cout << "connect done" << std::endl;

  {
    auto [data, iov] = db.get( {"v0", "VP/Alignment/Global.yml", 0} );
    std::cout << "data[0]:" << data << std::endl;
  }

  {
    auto [data, iov] = db.get( {"v0", "VP/Alignment/Global.yml", 200} );
    std::cout << "data[200]:" << data << std::endl;
  }
}
