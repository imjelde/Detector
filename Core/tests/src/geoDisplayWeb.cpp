/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <iostream>

#include "Core/DetectorDataService.h"
#include "DD4hep/Detector.h"
#include "DD4hep/Printout.h"

#include "ROOT/RDirectory.hxx"
#include "ROOT/REveGeomViewer.hxx"

#include "TEnv.h"
#include "TRint.h"

using namespace ROOT::Experimental;

int main( int argc, char* argv[] ) {

  if ( argc < 1 ) {
    std::cerr << "Please specify the compact geometry to load" << std::endl;
    return 1;
  }

  dd4hep::Detector& desc = dd4hep::Detector::getInstance();
  desc.fromXML( argv[1] );

  gGeoManager->SetVisLevel( 4 );

  auto viewer = std::make_shared<REveGeomViewer>( gGeoManager );
  std::cout << gGeoManager->GetTopVolume()->GetName() << std::endl;
  viewer->SelectVolume( gGeoManager->GetTopVolume()->GetName() );

  // specify JSROOT draw options -here clipping on X,Y,Z axes
  // viewer->SetDrawOptions("clipxyz");

  // set default limits for number of visible nodes and faces
  // when viewer created, initial values exported from  TGeoManager
  viewer->SetLimits();

  // start web browser
  viewer->Show();

  // add to global heap to avoid immediate destroy of RGeomViewer
  RDirectory::Heap().Add( "geom_viewer", viewer );

  std::pair<int, char**> a( 0, 0 );
  TRint                  app( "geoDisplayWeb", &a.first, a.second );
  app.Run();
}
