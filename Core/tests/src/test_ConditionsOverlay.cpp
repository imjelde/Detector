/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_MODULE ConditionsOverlay
#include <boost/test/unit_test.hpp>

#include <Core/ConditionsOverlay.h>

#include <filesystem>

using LHCb::Detector::ConditionsOverlay;

BOOST_AUTO_TEST_CASE( simple_get ) {
  ConditionsOverlay ovly;
  BOOST_CHECK( ovly.size() == 0 );

  {
    auto doc = ovly.get( "some/path", R"(---
node1: 42
node2:
  pos: 1
  rot: 2
)" );

    BOOST_CHECK( ovly.size() == 1 );

    BOOST_CHECK( doc.size() == 2 );
    BOOST_CHECK( doc["node1"].as<int>() == 42 );
    BOOST_CHECK( doc["node2"]["pos"].as<int>() == 1 );
    BOOST_CHECK( doc["node2"]["rot"].as<int>() == 2 );
  }

  {
    auto doc1 = ovly.get( "some/path", R"(---
node1: 43
node2:
  pos: 5
  rot: 2
)" );

    auto doc2 = ovly.get( "another/path", R"(---
data: some text
)" );

    BOOST_CHECK( ovly.size() == 2 );

    BOOST_CHECK( doc1.size() == 2 );
    BOOST_CHECK( doc1["node1"].as<int>() == 43 );
    BOOST_CHECK( doc1["node2"]["pos"].as<int>() == 5 );
    BOOST_CHECK( doc1["node2"]["rot"].as<int>() == 2 );

    BOOST_CHECK( doc2.size() == 1 );
    BOOST_CHECK( doc2["data"].as<std::string>() == "some text" );
  }
}

BOOST_AUTO_TEST_CASE( override_get ) {
  ConditionsOverlay ovly;
  BOOST_CHECK( ovly.size() == 0 );

  {
    auto doc = ovly.get( "some/path", R"(---
node1: 42
node2:
  pos: 1
  rot: 2
)" );

    BOOST_CHECK( ovly.size() == 1 );

    BOOST_CHECK( doc.size() == 2 );
    BOOST_CHECK( doc["node1"].as<int>() == 42 );
    BOOST_CHECK( doc["node2"]["pos"].as<int>() == 1 );
    BOOST_CHECK( doc["node2"]["rot"].as<int>() == 2 );
  }

  ovly.update( "some/path", "node2", YAML::Load( R"({"pos": 3, "rot": 7})" ) );

  {
    auto doc = ovly.get( "some/path", R"(---
node1: 43
node2:
  pos: 5
  rot: 2
)" );

    BOOST_CHECK( ovly.size() == 1 );

    BOOST_CHECK( doc.size() == 2 );
    BOOST_CHECK( doc["node1"].as<int>() == 42 );
    BOOST_CHECK( doc["node2"]["pos"].as<int>() == 3 );
    BOOST_CHECK( doc["node2"]["rot"].as<int>() == 7 );
  }
}

BOOST_AUTO_TEST_CASE( add ) {
  ConditionsOverlay ovly;
  BOOST_CHECK( ovly.size() == 0 );

  ovly.add( "some/path", YAML::Load( R"({"initial": 0})" ) );
  BOOST_CHECK( ovly.size() == 1 );

  // an added document is not visible unless also updated
  ovly.update( "some/path", "another", YAML::Load( "1" ) );
  BOOST_CHECK( ovly.size() == 1 );

  {
    auto doc = ovly.get( "some/path", "{}" );

    BOOST_CHECK( doc.size() == 2 );
    BOOST_CHECK( doc["initial"].as<int>() == 0 );
    BOOST_CHECK( doc["another"].as<int>() == 1 );
  }
  // replace the doc
  ovly.add( "some/path", YAML::Load( R"({"whatever": 10})" ) );
  ovly.update( "some/path", "another", YAML::Load( "1" ) );

  {
    auto doc = ovly.get( "some/path", "{}" );

    BOOST_CHECK( doc.size() == 2 );
    BOOST_CHECK( doc["whatever"].as<int>() == 10 );
    BOOST_CHECK( doc["another"].as<int>() == 1 );
  }
}

BOOST_AUTO_TEST_CASE( clear ) {
  ConditionsOverlay ovly;
  BOOST_CHECK( ovly.size() == 0 );

  ovly.add( "some/path", YAML::Load( R"({})" ) );
  BOOST_CHECK( ovly.size() == 1 );

  ovly.clear();
  BOOST_CHECK( ovly.size() == 0 );
}

BOOST_AUTO_TEST_CASE( dump ) {
  namespace fs = std::filesystem;

  // helper to make sure we start with a clean slate and we clean up
  // when we are gone
  struct clean_up {
    clean_up() { fs::remove_all( "overlay_dump" ); }
    ~clean_up() { fs::remove_all( "overlay_dump" ); }
  } cleaner;

  ConditionsOverlay ovly;
  ovly.add( "path/to/a/condition.yaml", YAML::Load( R"(---
condition1: [0, 1, 2]
displacement: !alignment
  position: [0, 0, 0]
  rotation: [0, 0, 0]
)" ) );
  ovly.add( "/this_is/another.yaml", YAML::Load( R"(---
value1: 100
value2: -3
)" ) );
  ovly.dump( "overlay_dump" );

  BOOST_REQUIRE( fs::is_regular_file( "overlay_dump/path/to/a/condition.yaml" ) );
  auto f1 = YAML::LoadFile( "overlay_dump/path/to/a/condition.yaml" );
  BOOST_CHECK( f1.size() == 2 );
  BOOST_CHECK( ( f1["condition1"].as<std::vector<int>>() == std::vector<int>{0, 1, 2} ) );
  auto d = f1["displacement"];
  BOOST_CHECK( d.Tag() == "!alignment" );
  BOOST_CHECK( d.size() == 2 );
  BOOST_CHECK( ( d["position"].as<std::vector<int>>() == std::vector<int>{0, 0, 0} ) );
  BOOST_CHECK( ( d["rotation"].as<std::vector<int>>() == std::vector<int>{0, 0, 0} ) );

  BOOST_REQUIRE( fs::is_regular_file( "overlay_dump/this_is/another.yaml" ) );
  auto f2 = YAML::LoadFile( "overlay_dump/this_is/another.yaml" );
  BOOST_CHECK( f2.size() == 2 );
  BOOST_CHECK( f2["value1"].as<int>() == 100 );
  BOOST_CHECK( f2["value2"].as<int>() == -3 );

  std::size_t files = 0, dirs = 0, others = 0;
  for ( const auto& entry : fs::recursive_directory_iterator( "overlay_dump" ) ) {
    if ( entry.is_regular_file() ) {
      ++files;
    } else if ( entry.is_directory() ) {
      ++dirs;
    } else {
      ++others;
    }
  }
  BOOST_CHECK( files == 2 );
  BOOST_CHECK( dirs == 4 );
  BOOST_CHECK( others == 0 );
}