/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DD4hep/Detector.h"
#include "DD4hep/Factories.h"
#include "DD4hep/Printout.h"

#include "Core/DetectorDataService.h"
#include "Core/Keys.h"
#include "Core/MagneticFieldExtension.h"
#include "Core/yaml_converters.h"

#include "Detector/Magnet/DeMagnet.h"
#include "Detector/VP/DeVP.h"

#include <cstring>
#include <exception>
#include <string>
#include <vector>

static long test_DetectorDataService( dd4hep::Detector& description, int argc, char** argv ) {

  bool        help = false;
  std::string conditions;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( argv[i][0] == '-' || argv[i][0] == '/' ) {
      if ( 0 == ::strncmp( "-help", argv[i], 4 ) )
        help = true;
      else if ( 0 == ::strncmp( "-conditions", argv[i], 4 ) )
        conditions = argv[++i];
      else
        help = true;
    }
  }
  if ( help || conditions.empty() ) {
    /// Help printout describing the basic command line interface
    std::cout << "Usage: -plugin <name> -arg [-arg]                                   \n"
                 "     name:   factory name     LHCb_TEST_cond_content                \n"
                 "     -detector   <name>       Name of the sub-detector to analyze.  \n"
                 "     -conditions <directory>  Top-directory with conditions files.  \n"
                 "                              Fully qualified: <protocol>://<path>  \n"
                 "     -help                    Show this help.                       \n"
                 "\tArguments given: "
              << dd4hep::arguments( argc, argv ) << std::endl;
    ::exit( EINVAL );
  }

  // Attaching the MagneticFieldExtension to the Magnet DetElement
  LHCb::Magnet::setup_magnetic_field_extension( description, "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/FieldMap/v5r7/cdf" );

  // Now creating the DetectorDataService that will load the conditions
  std::vector<std::string>            detector_list{"/world", "VP", "Magnet"};
  LHCb::Detector::DetectorDataService dds( description, detector_list );
  dds.initialize( conditions, "" );

  // Checking the value of a given conditions
  auto get_test_value = [&dds, &description]( int iov ) -> double {
    auto        slice  = dds.get_slice( iov );
    auto        val    = slice->get( description.detector( "VP" ), LHCb::Detector::item_key( "TestCond" ) );
    YAML::Node* doc    = static_cast<YAML::Node*>( val->payload() );
    YAML::Node  values = ( *doc )["values"];
    for ( std::size_t i = 0; i < values.size(); i++ ) {
      dd4hep::printout( dd4hep::INFO, "test_DetectorDataService", "value[%d]: %f", i, values[i].as<double>() );
    }
    return values[0].as<double>();
  };

  double test_value = get_test_value( 100 );
  if ( test_value != 4.2 ) {
    dd4hep::printout( dd4hep::ERROR, "test_DetectorDataService", "Wrong value[0]: %f", test_value );
    ::exit( EINVAL );
  }

  test_value = get_test_value( 200 );
  if ( test_value != 0.1 ) {
    dd4hep::printout( dd4hep::ERROR, "test_DetectorDataService", "Wrong value[0]: %f", test_value );
    ::exit( EINVAL );
  }

  // Now checking the MagneticField
  auto                            slice     = dds.get_slice( 200 );
  dd4hep::DetElement              magnetdet = description.detector( "Magnet" );
  const LHCb::Detector::DeMagnet& m         = slice->get( magnetdet, LHCb::Detector::Keys::deKey );
  const auto                      f         = m.fieldVector( ROOT::Math::XYZPoint{-400, 0, 0} );

  double maxdiff = 1e-5;
  auto   check   = [&maxdiff]( double val, double ref, std::string name ) {
    if ( abs( ( val - ref ) / val ) > maxdiff ) {
      dd4hep::printout( dd4hep::ERROR, "test_DetectorDataService", "Error with %s", name.c_str() );
      ::exit( EINVAL );
    }
  };

  // Field should be -1.18417e-07,-6.66836e-06,1.30723e-06 at that point...
  // std::cout << "field:" << f << std::endl;
  check( f.X(), -1.18417e-07, "field map X component" );
  check( f.Y(), -6.66836e-06, "field map Y component" );
  check( f.Z(), 1.30723e-06, "field map Z component" );

  dds.finalize();
  return 0;
}
DECLARE_APPLY( LHCb_TEST_DetectorDataService, test_DetectorDataService )

static long test_VPPosition( dd4hep::Detector& description, int argc, char** argv ) {

  bool        help = false;
  std::string conditions;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( argv[i][0] == '-' || argv[i][0] == '/' ) {
      if ( 0 == ::strncmp( "-help", argv[i], 4 ) )
        help = true;
      else if ( 0 == ::strncmp( "-conditions", argv[i], 4 ) )
        conditions = argv[++i];
      else
        help = true;
    }
  }
  if ( help || conditions.empty() ) {
    /// Help printout describing the basic command line interface
    std::cout << "Usage: -plugin <name> -arg [-arg]                                   \n"
                 "     name:   factory name     LHCb_TEST_cond_content                \n"
                 "     -detector   <name>       Name of the sub-detector to analyze.  \n"
                 "     -conditions <directory>  Top-directory with conditions files.  \n"
                 "                              Fully qualified: <protocol>://<path>  \n"
                 "     -help                    Show this help.                       \n"
                 "\tArguments given: "
              << dd4hep::arguments( argc, argv ) << std::endl;
    ::exit( EINVAL );
  }

  std::vector<std::string>            detector_list{"/world", "VP"};
  LHCb::Detector::DetectorDataService dds( description, detector_list );
  dds.initialize( conditions, "" );

  auto print_position = []( int slice_id, const char* det, const ROOT::Math::XYZPoint& pos ) {
    dd4hep::printout( dd4hep::INFO, "Test_VPPosition", "Slice %d, %s Position: (%f, %f, %f)", slice_id, det, pos.X(),
                      pos.Y(), pos.Z() );
  };

  // Getting the condition object for the DeVP
  int                         slice1_id = 100;
  auto                        slice     = dds.get_slice( slice1_id );
  dd4hep::DetElement          vpdet     = description.detector( "VP" );
  const LHCb::Detector::DeVP& vp        = slice->get( vpdet, LHCb::Detector::Keys::deKey );
  auto                        vppos     = vp.toGlobal( ROOT::Math::XYZPoint{0, 0, 0} );
  print_position( slice1_id, "VP", vppos );

  // Getting the slice for time 200 with displacement
  int                         slice2_id = 200;
  auto                        slice2    = dds.get_slice( slice2_id );
  const LHCb::Detector::DeVP& vp2       = slice2->get( vpdet, LHCb::Detector::Keys::deKey );
  auto                        vppos2    = vp2.toGlobal( ROOT::Math::XYZPoint{0, 0, 0} );
  print_position( slice2_id, "VP", vppos2 );
  auto diff2 = vppos2 - ROOT::Math::XYZPoint{1, 1, 1}; // LHCb units, so in mm;
  if ( diff2.Mag2() > 0.001 ) {
    std::cout << "slice for time 200 has wrong displacement " << vppos2 << " - expected {1, 1, 1}\n";
    ::exit( EINVAL );
  }
  // Checking the position of the right side of the VP
  const auto side2    = vp2.right();
  auto       sidepos2 = side2.toGlobal( ROOT::Math::XYZPoint{0, 0, 0} );
  print_position( slice2_id, "Right Side", sidepos2 );

  // Checking the position
  int                         slice3_id = 550;
  auto                        slice3    = dds.get_slice( slice3_id );
  const LHCb::Detector::DeVP& vp3       = slice3->get( vpdet, LHCb::Detector::Keys::deKey );
  auto                        vppos3    = vp3.toGlobal( ROOT::Math::XYZPoint{0, 0, 0} );
  print_position( slice3_id, "VP", vppos3 );

  // Checking the position of the right side of the VP
  const auto side3    = vp3.right();
  auto       sidepos3 = side3.toGlobal( ROOT::Math::XYZPoint{0, 0, 0} );
  print_position( slice3_id, "Right Side", sidepos3 );
  auto diff3 = sidepos3 - ROOT::Math::XYZPoint{6, 6, 6}; // LHCb units, so in mm
  if ( diff3.Mag2() > 0.001 ) {
    std::cout << "position of the right side of the VP wrong : " << sidepos3 << " - expected {6, 6, 6}\n";
    ::exit( EINVAL );
  }

  // Finalizing the service and returning
  dds.finalize();
  return 0;
}
DECLARE_APPLY( LHCb_TEST_VPPosition, test_VPPosition )

void _iterate_detector_tree( dd4hep::DetElement det, const dd4hep::cond::ConditionsSlice& slice ) {

  // std::cout << det.path() << "|" << det.name() << "|" << det.volumeID() << "|" << det.placementPath() << std::endl;
  const char* detname = det.name();
  if ( ::strncmp( detname, "ladder_0", ::strlen( detname ) ) == 0 ) {
    std::cout << "Found: " << det.path() << std::endl;
    dd4hep::Alignment a = slice.get( det, LHCb::Detector::Keys::alignmentKey );
    std::cout << a.localToWorld( dd4hep::Position{0, 0, 0} ) << std::endl;
  }

  // Iterating on children
  for ( const auto& [name, child_det] : det.children() ) { _iterate_detector_tree( child_det, slice ); }
}

static long test_VPLadders( dd4hep::Detector& description, int argc, char** argv ) {

  bool        help = false;
  std::string conditions;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( argv[i][0] == '-' || argv[i][0] == '/' ) {
      if ( 0 == ::strncmp( "-help", argv[i], 4 ) )
        help = true;
      else if ( 0 == ::strncmp( "-conditions", argv[i], 4 ) )
        conditions = argv[++i];
      else
        help = true;
    }
  }
  if ( help || conditions.empty() ) {
    /// Help printout describing the basic command line interface
    std::cout << "Usage: -plugin <name> -arg [-arg]                                   \n"
                 "     name:   factory name     LHCb_TEST_cond_content                \n"
                 "     -detector   <name>       Name of the sub-detector to analyze.  \n"
                 "     -conditions <directory>  Top-directory with conditions files.  \n"
                 "                              Fully qualified: <protocol>://<path>  \n"
                 "     -help                    Show this help.                       \n"
                 "\tArguments given: "
              << dd4hep::arguments( argc, argv ) << std::endl;
    ::exit( EINVAL );
  }

  std::vector<std::string>            detector_list{"/world", "VP"};
  LHCb::Detector::DetectorDataService dds( description, detector_list );
  dds.initialize( conditions, "" );
  int  slice1_id = 100;
  auto slice     = dds.get_slice( slice1_id );

  _iterate_detector_tree( description.detector( "VP" ), *slice );

  return 0;
}
DECLARE_APPLY( LHCb_TEST_VPLadders, test_VPLadders )

static long test_VPLadders_conditions( dd4hep::Detector& description, int argc, char** argv ) {

  bool        help = false;
  std::string conditions;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( argv[i][0] == '-' || argv[i][0] == '/' ) {
      if ( 0 == ::strncmp( "-help", argv[i], 4 ) )
        help = true;
      else if ( 0 == ::strncmp( "-conditions", argv[i], 4 ) )
        conditions = argv[++i];
      else
        help = true;
    }
  }
  if ( help || conditions.empty() ) {
    /// Help printout describing the basic command line interface
    std::cout << "Usage: -plugin <name> -arg [-arg]                                   \n"
                 "     name:   factory name     LHCb_TEST_cond_content                \n"
                 "     -detector   <name>       Name of the sub-detector to analyze.  \n"
                 "     -conditions <directory>  Top-directory with conditions files.  \n"
                 "                              Fully qualified: <protocol>://<path>  \n"
                 "     -help                    Show this help.                       \n"
                 "\tArguments given: "
              << dd4hep::arguments( argc, argv ) << std::endl;
    ::exit( EINVAL );
  }

  std::vector<std::string>            detector_list{"/world", "VP"};
  LHCb::Detector::DetectorDataService dds( description, detector_list );
  dds.initialize( conditions, "" );
  int  slice1_id = 100;
  auto slice     = dds.get_slice( slice1_id );

  auto              det      = description.detector( "/world/BeforeMagnetRegion/VP/VPLeft/Module01/ladder_0" );
  dd4hep::Alignment a        = slice->get( det, LHCb::Detector::Keys::alignmentKey );
  auto              position = a.localToWorld( dd4hep::Position{0, 0, 0} );
  std::cout << "Ladder position: " << position << std::endl;
  dd4hep::Position expected{-1.27244, 3.9803, -127.535};
  auto             diff = expected - position;
  if ( diff.r() > 1e-5 ) {
    std::cout << "Incorrect ladder position from Conditions. distance from expected: " << diff.r() << "cm" << std::endl;
    return 1;
  }
  return 0;
}
DECLARE_APPLY( LHCb_TEST_VPLadders_conditions, test_VPLadders_conditions )
