/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#pragma once

#include "Core/DeIOV.h"
#include "Detector/Rich/Types.h"
#include "Detector/Rich/Utilities.h"

namespace LHCb::Detector::detail {

  struct DeRichRadiatorObject : DeIOVObject {

    using TabData = XYTable;

    double  m_Presure_Nominal     = -1;
    double  m_Temperature_Nominal = -1;
    double  m_PhotonMinEnergy     = -1;
    double  m_PhotonMaxEnergy     = -1;
    TabData m_GasRefIndex;

    void SetRichGasRefIndex( const TabData& aRefInd ) { m_GasRefIndex = aRefInd; }

    TabData CurrentScaledRefIndexInObjTab( double aPressure, double aTemperature ) const;

    TabData RetrieveNominalGasRefIndexInDeObjectFromDB() const;

    using DeIOVObject::DeIOVObject;
  };

  template <typename ObjectType>
  struct DeRichRadiatorElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;
    auto Gas_Presure_Nominal() const noexcept { return this->access()->m_Presure_Nominal; }
    auto Gas_Temperature_Nominal() const noexcept { return this->access()->m_Temperature_Nominal; }
    auto Gas_PhotonMinEnergy() const noexcept { return toLHCbEnergyUnits( this->access()->m_PhotonMinEnergy ); }
    auto Gas_PhotonMaxEnergy() const noexcept { return toLHCbEnergyUnits( this->access()->m_PhotonMaxEnergy ); }
    auto GasRefIndex() const noexcept { return scalePhotonEnergies( this->access()->m_GasRefIndex ); }
  };

} // namespace LHCb::Detector::detail
