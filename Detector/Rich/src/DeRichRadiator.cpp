/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/Rich/DeRichRadiator.h"
#include "Detector/Rich1/RichMatOPD.h"

using namespace LHCb::Detector::detail;

DeRichRadiatorObject::TabData DeRichRadiatorObject::CurrentScaledRefIndexInObjTab( double aPressure,
                                                                                   double aTemperature ) const {
  // This to scale the ref index once the conditions DB is available with different pressure and temperature.
  TabData aScaledRefInd;

  auto aRhoFact = ( aPressure / m_Presure_Nominal ) * ( m_Temperature_Nominal / aTemperature );

  auto aRichMatCF4EnRi = RetrieveNominalGasRefIndexInDeObjectFromDB();

  for ( auto itH = aRichMatCF4EnRi.begin(); itH != aRichMatCF4EnRi.end(); ++itH ) {
    auto aEnValH = itH->first;
    auto aRiValH = itH->second;
    auto aScaVal = ( ( aRiValH - 1.0 ) * aRhoFact ) + 1.0;
    aScaledRefInd.emplace( aEnValH, aScaVal );
  }

  return aScaledRefInd;
}

DeRichRadiatorObject::TabData DeRichRadiatorObject::RetrieveNominalGasRefIndexInDeObjectFromDB() const {
  auto aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
  return aRichMatOpd->GetRichMatProperty( detector.volume().material(), RichMatPropType::CkvRndx );
}
