/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

struct CardParam final {

  // To be dropped once everything is implemented
  struct NotImplemented : std::exception {
    const char* what() const noexcept override { return "not implemented"; };
  };

  int  downNumber() const { throw NotImplemented(); }
  int  leftNumber() const { throw NotImplemented(); }
  int  cornerNumber() const { throw NotImplemented(); }
  int  previousNumber() const { throw NotImplemented(); }
  int  validationNumber() const { throw NotImplemented(); }
  int  area() const { throw NotImplemented(); }
  int  firstRow() const { throw NotImplemented(); }
  int  firstColumn() const { throw NotImplemented(); }
  int  lastRow() const { throw NotImplemented(); }
  int  lastColumn() const { throw NotImplemented(); }
  bool isPinCard() const { throw NotImplemented(); }
  int  tell1() const { throw NotImplemented(); }
  int  number() const { throw NotImplemented(); }
  int  crate() const { throw NotImplemented(); }
  int  slot() const { throw NotImplemented(); }
  int  code() const { throw NotImplemented(); }
  int  selectionType() const { throw NotImplemented(); }
  bool isParasitic() const { throw NotImplemented(); }
  bool isPmtCard() const { throw NotImplemented(); }
};
