/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace LHCb::Detector::Calo {

  struct Pin final {

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    CellID                     id() const { throw NotImplemented(); }
    const std::vector<int>&    leds() const { throw NotImplemented(); }
    const std::vector<CellID>& cells() const { throw NotImplemented(); }
    int                        index() const { throw NotImplemented(); }
  };

} // namespace LHCb::Detector::Calo
