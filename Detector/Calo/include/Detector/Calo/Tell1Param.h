/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "boost/container/static_vector.hpp"

namespace LHCb::Detector::Calo {

  struct Tell1Param final {

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    struct FECards {
      using Container = boost::container::static_vector<unsigned char, 24>;

      // To be dropped once everything is implemented
      struct NotImplemented : std::exception {
        const char* what() const noexcept override { return "not implemented"; };
      };

      unsigned int                size() const { throw NotImplemented(); }
      unsigned char               operator[]( size_t ) const { throw NotImplemented(); }
      Container::iterator         begin() const { throw NotImplemented(); }
      Container::iterator         end() const { throw NotImplemented(); }
      void                        push_back( unsigned char ) { throw NotImplemented(); }
      Container::iterator         erase( Container::const_iterator ) { throw NotImplemented(); }
      friend inline std::ostream& operator<<( std::ostream&, FECards const& ) { throw NotImplemented(); }
    };

    int     number() const { throw NotImplemented(); }
    FECards feCards() const { throw NotImplemented(); }
    bool    readPin() const { throw NotImplemented(); }
  };

} // namespace LHCb::Detector::Calo
