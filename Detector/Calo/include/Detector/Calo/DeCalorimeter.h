/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Calo/CaloCardParams.h"
#include "Detector/Calo/CaloCellID.h"
#include "Detector/Calo/CaloLed.h"
#include "Detector/Calo/CaloPin.h"
#include "Detector/Calo/CaloVector.h"
#include "Detector/Calo/CardParam.h"
#include "Detector/Calo/CellParam.h"
#include "Detector/Calo/DeSubCalorimeter.h"
#include "Detector/Calo/Tell1Param.h"

#include "Core/DeIOV.h"

#include <vector>

namespace LHCb::Detector::Calo {

  /// definition of calorimeter planes
  enum Plane { Front = 0, Middle, ShowerMax, Back };

  inline std::string toString( Plane e ) {
    switch ( e ) {
    case Plane::Front:
      return "Front";
    case Plane::ShowerMax:
      return "ShowerMax";
    case Plane::Middle:
      return "Middle";
    case Plane::Back:
      return "Back";
    default:
      throw "Not a correct plane in Calo";
    }
  }

  inline std::ostream& toStream( Plane e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }

  inline std::ostream& operator<<( std::ostream& s, Plane e ) { return toStream( e, s ); }

  namespace detail {

    struct DeCaloObject : LHCb::Detector::detail::DeIOVObject {
      DeCaloObject( dd4hep::DetElement const& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // namespace detail

  namespace DeCalorimeterLocation {
    inline const std::string Ecal = "/world/DownstreamRegion/Ecal";
    inline const std::string Hcal = "/world/DownstreamRegion/Hcal";
  } // namespace DeCalorimeterLocation

  template <typename ObjectType>
  class DeCaloElement : public DeIOVElement<ObjectType> {
  public:
    using DeIOVElement<ObjectType>::DeIOVElement;

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    // general
    const std::string& caloName() const { throw NotImplemented(); }
    /// calorimeter index, @see namespace CaloCellCode
    CellCode::Index index() const { throw NotImplemented(); }
    // accessing the geometry parameters
    double       xSize() const { throw NotImplemented(); }
    double       ySize() const { throw NotImplemented(); }
    double       zSize() const { throw NotImplemented(); }
    double       zOffset() const { throw NotImplemented(); }
    unsigned int numberOfAreas() const { throw NotImplemented(); }
    // reference plane in the global frame
    inline ROOT::Math::Plane3D plane( const double ) const { throw NotImplemented(); }
    inline ROOT::Math::Plane3D plane( const ROOT::Math::XYZPoint& ) const { throw NotImplemented(); }
    inline ROOT::Math::Plane3D plane( const Plane ) const { throw NotImplemented(); }

    // accessing the calibration parameters
    double maxEtInCenter( unsigned int = 0 ) const { throw NotImplemented(); }
    double maxEtSlope( unsigned int = 0 ) const { throw NotImplemented(); }
    double pedestalShift() const { throw NotImplemented(); }
    double pinPedestalShift() const { throw NotImplemented(); }
    double L0EtGain() const { throw NotImplemented(); }
    double coherentNoise() const { throw NotImplemented(); }
    double incoherentNoise() const { throw NotImplemented(); }
    double stochasticTerm() const { throw NotImplemented(); }
    double gainError() const { throw NotImplemented(); }

    // for simulation only
    int    zSupMethod() const { throw NotImplemented(); }
    double zSupThreshold() const { throw NotImplemented(); }
    double zSupNeighbourThreshold() const { throw NotImplemented(); }
    bool   hasQuality( const CellID&, CellQuality::Flag ) const { throw NotImplemented(); }
    double spdThreshold( const CellID& ) const { throw NotImplemented(); }
    double l0Threshold() const { throw NotImplemented(); }
    double mipDeposit() const { throw NotImplemented(); }
    double dynamicsSaturation() const { throw NotImplemented(); }
    double fractionFromPrevious() const { throw NotImplemented(); }
    double numberOfPhotoElectrons( unsigned int = 0 ) const { throw NotImplemented(); }
    double l0EtCorrection( unsigned int = 0 ) const { throw NotImplemented(); }
    double activeToTotal() const { throw NotImplemented(); }

    // accessing the hardware parameter(s)
    unsigned int adcMax() const { throw NotImplemented(); }

    // accessing the reconstruction parameter(s)
    double zShowerMax() const { throw NotImplemented(); }

    // accessing readout parameters
    unsigned int numberOfCells() const { throw NotImplemented(); }
    unsigned int numberOfCards() const { throw NotImplemented(); }
    unsigned int numberOfTell1s() const { throw NotImplemented(); }
    unsigned int numberOfPins() const { throw NotImplemented(); }
    unsigned int numberOfLeds() const { throw NotImplemented(); }
    unsigned int numberOfInvalidCells() const { throw NotImplemented(); }
    unsigned int pinArea() const { throw NotImplemented(); }

    ///  Cell Parameters
    bool   valid( const CellID& ) const { throw NotImplemented(); }
    double cellX( const CellID& ) const { throw NotImplemented(); }
    double cellY( const CellID& ) const { throw NotImplemented(); }
    double cellZ( const CellID& ) const { throw NotImplemented(); }
    double cellSize( unsigned int ) const { throw NotImplemented(); }
    double cellSize( const CellID& ) const { throw NotImplemented(); }
    double cellSine( const CellID& ) const { throw NotImplemented(); }
    double cellGain( const CellID& ) const { throw NotImplemented(); }
    // convert ADC to energy in MeV for a given cellID
    double cellEnergy( int, CellID ) const { throw NotImplemented(); }
    // reverse operation : convert energy in MeV to ADC
    double cellADC( double, CellID ) const { throw NotImplemented(); }
    bool   isSaturated( double, CellID ) const { throw NotImplemented(); }

    double                     cellTime( const CellID& ) const { throw NotImplemented(); }
    const ROOT::Math::XYZPoint cellCenter( const CellID& ) const { throw NotImplemented(); }
    const std::vector<CellID>& neighborCells( const CellID& ) const { throw NotImplemented(); }
    const std::vector<CellID>& zsupNeighborCells( const CellID& ) const { throw NotImplemented(); }
    bool                       isDead( const CellID& ) const { throw NotImplemented(); }
    bool                       isNoisy( const CellID& ) const { throw NotImplemented(); }
    bool                       isShifted( const CellID& ) const { throw NotImplemented(); }
    bool                       hasDeadLED( const CellID& ) const { throw NotImplemented(); }
    bool                       isVeryNoisy( const CellID& ) const { throw NotImplemented(); }
    bool                       isVeryShifted( const CellID& ) const { throw NotImplemented(); }
    // from cellId to  serial number and vice-versa
    int    cellIndex( const CellID& ) const { throw NotImplemented(); }
    CellID cellIdByIndex( const unsigned int ) const { throw NotImplemented(); }
    // from cell to FEB
    bool isReadout( const CellID& ) const { throw NotImplemented(); }
    int  cardNumber( const CellID& ) const { throw NotImplemented(); }
    int  cardRow( const CellID& ) const { throw NotImplemented(); }
    int  cardColumn( const CellID& ) const { throw NotImplemented(); }
    void cardAddress( const CellID&, int&, int&, int& ) const { throw NotImplemented(); }
    //  FEB card
    int                        nCards() const { throw NotImplemented(); }
    int                        downCardNumber( const int ) const { throw NotImplemented(); }
    int                        leftCardNumber( const int ) const { throw NotImplemented(); }
    int                        cornerCardNumber( const int ) const { throw NotImplemented(); }
    int                        previousCardNumber( const int ) const { throw NotImplemented(); }
    void                       cardNeighbors( const int, int&, int&, int& ) const { throw NotImplemented(); }
    int                        validationNumber( const int ) const { throw NotImplemented(); }
    int                        selectionType( const int ) const { throw NotImplemented(); }
    int                        cardArea( const int ) const { throw NotImplemented(); }
    int                        cardFirstRow( const int ) const { throw NotImplemented(); }
    int                        cardLastColumn( const int ) const { throw NotImplemented(); }
    int                        cardLastRow( const int ) const { throw NotImplemented(); }
    int                        cardFirstColumn( const int ) const { throw NotImplemented(); }
    int                        cardFirstValidRow( const int ) const { throw NotImplemented(); }
    int                        cardLastValidRow( const int ) const { throw NotImplemented(); }
    int                        cardFirstValidColumn( const int ) const { throw NotImplemented(); }
    int                        cardLastValidColumn( const int ) const { throw NotImplemented(); }
    CellID                     firstCellID( const int ) const { throw NotImplemented(); }
    CellID                     lastCellID( const int ) const { throw NotImplemented(); }
    CellID                     cardCellID( const int, const int, const int ) const { throw NotImplemented(); }
    int                        cardCrate( const int ) const { throw NotImplemented(); }
    int                        cardSlot( const int ) const { throw NotImplemented(); }
    int                        cardCode( const int ) const { throw NotImplemented(); }
    const std::vector<CellID>& cardChannels( const int ) const { throw NotImplemented(); }
    const std::vector<CellID>& pinChannels( const CellID ) const { throw NotImplemented(); }
    const std::vector<CellID>& ledChannels( const int ) const { throw NotImplemented(); }
    const std::vector<int>&    pinLeds( const CellID ) const { throw NotImplemented(); }
    const std::vector<int>&    cellLeds( const CellID ) const { throw NotImplemented(); }
    int                        cardIndexByCode( const int, const int ) const { throw NotImplemented(); }
    int                        cardToTell1( const int ) const { throw NotImplemented(); }
    // from validation to Hcal FEB
    int validationToHcalFEB( const int, const unsigned int ) const { throw NotImplemented(); }

    const std::map<int, std::vector<int>>& getSourceIDsMap() const { throw NotImplemented(); }

    // Tell1s
    int                 nTell1s() const { throw NotImplemented(); }
    Tell1Param::FECards tell1ToCards( const int ) const { throw NotImplemented(); }
    // CardParam/Tell1Param/CellParam
    CardParam  cardParam( const int ) const { throw NotImplemented(); }
    Tell1Param tell1Param( const int ) const { throw NotImplemented(); }
    CellParam  cellParam( CellID ) const { throw NotImplemented(); }
    Pin        caloPin( CellID ) const { throw NotImplemented(); }
    Led        caloLed( const int ) const { throw NotImplemented(); }
    //  More complex functions
    CellID             Cell( const ROOT::Math::XYZPoint& ) const { throw NotImplemented(); }
    const CellParam*   Cell_( const ROOT::Math::XYZPoint& ) const { throw NotImplemented(); }
    CellCode::CaloArea Area( const ROOT::Math::XYZPoint& ) const { throw NotImplemented(); }
    // Collections
    const CaloVector<CellParam>&   cellParams() const { throw NotImplemented(); }
    const CaloVector<Pin>&         caloPins() const { throw NotImplemented(); }
    const std::vector<Led>&        caloLeds() const { throw NotImplemented(); }
    const std::vector<CardParam>&  cardParams() const { throw NotImplemented(); }
    const std::vector<Tell1Param>& tell1Params() const { throw NotImplemented(); }
    // PIN flag
    bool isParasiticCard( const int ) const { throw NotImplemented(); }
    bool isPmtCard( const int ) const { throw NotImplemented(); }
    bool isPinCard( const int ) const { throw NotImplemented(); }
    bool isPinTell1( const int ) const { throw NotImplemented(); }
    bool isPinId( CellID ) const { throw NotImplemented(); }
    // pileUp subtraction parameters
    int pileUpSubstractionMethod() const { throw NotImplemented(); }
    int pileUpSubstractionBin() const { throw NotImplemented(); }
    int pileUpSubstractionMin() const { throw NotImplemented(); }
  };
  using DeCalorimeter = DeCaloElement<detail::DeCaloObject>;

} // End namespace LHCb::Detector::Calo
