/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Calo/DeSubSubCalorimeter.h"

#include "Core/DeIOV.h"

namespace LHCb::Detector::Calo {

  namespace detail {

    struct DeSubCaloObject : LHCb::Detector::detail::DeIOVObject {
      DeSubCaloObject( dd4hep::DetElement const& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // namespace detail

  template <typename ObjectType>
  struct DeSubCaloElement : public DeIOVElement<ObjectType> {};

  using DeSubCalorimeter = DeSubCaloElement<detail::DeSubCaloObject>;

} // End namespace LHCb::Detector::Calo
