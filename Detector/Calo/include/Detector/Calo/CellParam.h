/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Calo/CaloCellID.h"

#include "Math/Point3D.h"

namespace LHCb::Detector::Calo {

  namespace CellQuality {
    enum Flag {
      OK                = 0,
      Dead              = 1,
      Noisy             = 2,
      Shifted           = 4,
      DeadLED           = 8,
      VeryNoisy         = 16,
      VeryShifted       = 32,
      LEDSaturated      = 64,
      BadLEDTiming      = 128,
      VeryBadLEDTiming  = 256,
      BadLEDRatio       = 512,
      BadLEDOpticBundle = 1024,
      UnstableLED       = 2048,
      StuckADC          = 4096,
      OfflineMask       = 8192
    };
    inline const int         Number       = 15;
    inline const std::string Name[Number] = {
        "OK",          "Dead",         "Noisy",        "Shifted",          "DeadLED",     "VeryNoisy",
        "VeryShifted", "LEDSaturated", "BadLEDTiming", "VeryBadLEDTiming", "BadLEDRatio", "BadLEDOpticBundle",
        "UnstableLED", "StuckADC",     "OfflineMask"};
    inline const std::string qName( int i ) {
      if ( i < Number && i >= 0 ) return Name[i];
      return std::string( "??" );
    }
  } // namespace CellQuality

  struct CellParam final {

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    bool                       valid() const { throw NotImplemented(); }
    CellID                     cellID() const { throw NotImplemented(); }
    double                     x() const { throw NotImplemented(); }
    double                     y() const { throw NotImplemented(); }
    double                     z() const { throw NotImplemented(); }
    const ROOT::Math::XYZPoint center() const { throw NotImplemented(); }
    double                     size() const { throw NotImplemented(); }
    double                     sine() const { throw NotImplemented(); }
    double                     nominalGain() const { throw NotImplemented(); }
    double                     time() const { throw NotImplemented(); }
    int                        cardNumber() const { throw NotImplemented(); }
    int                        cardRow() const { throw NotImplemented(); }
    int                        cardColumn() const { throw NotImplemented(); }
    double                     deltaTime() const { throw NotImplemented(); }
    double                     zShower() const { throw NotImplemented(); }
    int                        quality() const { throw NotImplemented(); }
    int                        l0Constant() const { throw NotImplemented(); }
    double calibration() const { throw NotImplemented(); } // absolute calibration from 'Calibration' condition (T0)
    double ledDataRef() const { throw NotImplemented(); }  // <LED> data from Calibration condition (Ref T0)
    double ledMoniRef() const { throw NotImplemented(); }  // <LED/PIN> data from Calibration condition (Ref T0)
    double ledData() const { throw NotImplemented(); }     // <LED> data from Quality condition (current T)
    double ledDataRMS() const { throw NotImplemented(); }  // RMS(LED) from Quality condition  (current T)
    double ledMoni() const { throw NotImplemented(); }     // <LED/PIN> data from Quality condition (current T)
    double ledMoniRMS() const { throw NotImplemented(); }  // RMS(LED/PIN) from Quality condition (current T)
    double ledDataShift() const { throw NotImplemented(); }
    double gainShift() const { throw NotImplemented(); }
    double gain() const { throw NotImplemented(); }
    double pileUpOffset() const { throw NotImplemented(); }
    double pileUpOffsetRMS() const { throw NotImplemented(); }

    const std::vector<CellID>& pins() const { throw NotImplemented(); }
    const std::vector<int>&    leds() const { throw NotImplemented(); }
    const std::vector<CellID>& neighbors() const { throw NotImplemented(); }
    const std::vector<CellID>& zsupNeighbors() const { throw NotImplemented(); }
  };

} // namespace LHCb::Detector::Calo
