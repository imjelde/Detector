/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"

namespace LHCb::Detector {

  namespace detail {

    struct DeLHCbObject : DeIOVObject {

      DeLHCbObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };
  } // End namespace detail

  template <typename ObjectType>
  struct DeLHCbElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;

    dd4hep::DetElement geometry() const { return this->access()->detector; }
  };

  using DeLHCb = DeLHCbElement<detail::DeLHCbObject>;
} // End namespace LHCb::Detector
