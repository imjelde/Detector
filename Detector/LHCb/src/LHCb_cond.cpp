/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/ConditionHelper.h"
#include "Core/ConditionsRepository.h"
#include "Core/DeConditionCall.h"
#include "Core/Keys.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/ExtensionEntry.h"
#include "DD4hep/Printout.h"
#include "Detector/LHCb/DeLHCb.h"

#include <yaml-cpp/yaml.h>

// Callback to setup  the DeLHCb object
namespace LHCb::Detector {

  /// Condition derivation call to build the top level VP DetElement condition information
  // It can process two keys
  struct DeLHCbConditionCall : DeConditionCall {
    using DeConditionCall::DeConditionCall;
    virtual dd4hep::Condition operator()( const dd4hep::ConditionKey&           key,
                                          dd4hep::cond::ConditionUpdateContext& context ) override final {

      if ( key.item_key() == LHCb::Detector::Keys::deKey ) {
        // This is the pointer we need to return at the end of the callback
        auto lhcbdet = dd4hep::Detector::getInstance().world();
        // Creating the Detector element with the desired field
        auto delhcbobj = new detail::DeLHCbObject( lhcbdet, context );
        return DeIOV( delhcbobj );
      }
      throw std::logic_error( "Key unknown to DeLHCbConditionCall" );
    }
  };

} // namespace LHCb::Detector

// Set up the conditions
static long create_conditions_recipes( dd4hep::Detector& description, xml_h /* e */ ) {

  // We looking the repository in case it's already been created
  using Ext_t   = std::shared_ptr<LHCb::Detector::ConditionsRepository>;
  auto de       = description.world();
  auto requests = de.extension<Ext_t>( false );
  if ( !requests ) {
    requests = new Ext_t( new LHCb::Detector::ConditionsRepository() );
    de.addExtension( new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>( requests ) );
  }

  // Adding the dependency on the
  auto iovUpdate = std::make_shared<LHCb::Detector::DeLHCbConditionCall>( *requests );
  // yes, we have a pointer to a shared pointer !!!
  ( *requests )->addDependency( de, LHCb::Detector::Keys::deKey, iovUpdate );

  return 1;
}
DECLARE_XML_DOC_READER( LHCb_LHCb_cond, create_conditions_recipes )
