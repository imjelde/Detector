/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*******************************************************************************/
#pragma once

#include "Detector/Rich/DeRichPDModule.h"
#include "Detector/Rich/Rich_Geom_Constants_De.h"
#include "Detector/Rich1/DetElemAccess/DeRich1Mapmt.h"
#include <array>
#include <optional>
#include <vector>

namespace LHCb::Detector {

  //====================================================================================//
  namespace detail {
    /**
     *  Rich1 Mapmt Module  data
     *  \author  Sajan Easo
     *  \date    2022-02-10
     *  \version  1.0
     */

    struct DeRich1StdPDModuleObject : DeRichPDModuleObject {
      using PMTs = std::array<std::optional<DeRich1MapmtObject>, Rich1::NumRich1MapmtsInStdModule>;
      PMTs m_Rich1StdMapmtDetElem;
      DeRich1StdPDModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

    struct DeRich1EdgePDModuleObject : DeRichPDModuleObject {
      using PMTs = std::array<std::optional<DeRich1MapmtObject>, Rich1::NumRich1MapmtsInEdgeModule>;
      PMTs m_Rich1EdgeMapmtDetElem;
      DeRich1EdgePDModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

    struct DeRich1OuterCornerPDModuleObject : DeRichPDModuleObject {
      using PMTs = std::array<std::optional<DeRich1MapmtObject>, Rich1::NumRich1MapmtsInOuterCornerModule>;
      PMTs m_Rich1OuterCornerMapmtDetElem;
      DeRich1OuterCornerPDModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // End namespace detail

  template <typename ObjectType>
  struct DeRich1StdPDModuleElement : detail::DeRichPDModuleElement<ObjectType> {
    using detail::DeRichPDModuleElement<ObjectType>::DeRichPDModuleElement;
    // extra test for valid pmt , avoided for now.
    // auto Mapmt ( int aP ) const noexcept {
    //  return (RichPmtGeoAux::getRichPmtGeoAuxInstance()->Rich1PmtModulePmtValid(Rich1StdPDModuleCopyNum(),aP)) ?
    //    (DeRich1Mapmt( &( this->access()->m_Rich1StdMapmtDetElem[aP] ) )) : 0   ;
    //  }
    auto Mapmt( const unsigned int aP ) const noexcept {
      assert( aP < this->access()->m_Rich1StdMapmtDetElem.size() );
      assert( this->access()->m_Rich1StdMapmtDetElem[aP].has_value() );
      return DeRich1Mapmt( &( this->access()->m_Rich1StdMapmtDetElem[aP].value() ) );
    }
    auto Mapmts() const noexcept {
      std::vector<DeRich1Mapmt> pmts;
      const auto                nPmts = this->access()->m_Rich1StdMapmtDetElem.size();
      pmts.reserve( nPmts );
      for ( std::size_t i = 0; i < nPmts; ++i ) { pmts.emplace_back( this->Mapmt( i ) ); }
      return pmts;
    }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich1StdPDModule = DeRich1StdPDModuleElement<detail::DeRich1StdPDModuleObject>;
  using DeRich1Mapmt       = DeRich1MapmtElement<detail::DeRich1MapmtObject>;

  template <typename ObjectType>
  struct DeRich1EdgePDModuleElement : detail::DeRichPDModuleElement<ObjectType> {
    using detail::DeRichPDModuleElement<ObjectType>::DeRichPDModuleElement;
    auto Mapmt( const unsigned int aP ) const noexcept {
      assert( aP < this->access()->m_Rich1EdgeMapmtDetElem.size() );
      assert( this->access()->m_Rich1EdgeMapmtDetElem[aP].has_value() );
      return DeRich1Mapmt( &( this->access()->m_Rich1EdgeMapmtDetElem[aP].value() ) );
    }
    auto Mapmts() const noexcept {
      std::vector<DeRich1Mapmt> pmts;
      const auto                nPmts = this->access()->m_Rich1EdgeMapmtDetElem.size();
      pmts.reserve( nPmts );
      for ( std::size_t i = 0; i < nPmts; ++i ) { pmts.emplace_back( this->Mapmt( i ) ); }
      return pmts;
    }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich1EdgePDModule = DeRich1EdgePDModuleElement<detail::DeRich1EdgePDModuleObject>;

  template <typename ObjectType>
  struct DeRich1OuterCornerPDModuleElement : detail::DeRichPDModuleElement<ObjectType> {
    using detail::DeRichPDModuleElement<ObjectType>::DeRichPDModuleElement;
    auto Mapmt( const unsigned int aP ) const noexcept {
      assert( aP < this->access()->m_Rich1OuterCornerMapmtDetElem.size() );
      assert( this->access()->m_Rich1OuterCornerMapmtDetElem[aP].has_value() );
      return DeRich1Mapmt( &( this->access()->m_Rich1OuterCornerMapmtDetElem[aP].value() ) );
    }
    auto Mapmts() const noexcept {
      std::vector<DeRich1Mapmt> pmts;
      const auto                nPmts = this->access()->m_Rich1OuterCornerMapmtDetElem.size();
      pmts.reserve( nPmts );
      for ( std::size_t i = 0; i < nPmts; ++i ) { pmts.emplace_back( this->Mapmt( i ) ); }
      return pmts;
    }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich1OuterCornerPDModule = DeRich1OuterCornerPDModuleElement<detail::DeRich1OuterCornerPDModuleObject>;

} // End namespace LHCb::Detector
