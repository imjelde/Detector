//==========================================================================
//  LHCb Rich1 Detector description implementation using DD4HEP
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
//
// Author     : Sajan Easo
// Date       : 2020-07-03
//
//==========================================================================
#include "Core/UpgradeTags.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/PropertyTable.h"
#include "Detector/Rich1/RichGeoTransAux.h"
#include "Detector/Rich1/RichGeoUtil.h"
#include "Detector/Rich1/RichMatOPD.h"
#include "Detector/Rich1/RichMatPropData.h"
#include "Detector/Rich1/RichPmtGeoAux.h"
#include "Detector/Rich1/RichSurfaceUtil.h"
#include "XML/Utilities.h"

namespace {

  // Global parameters for activating various parts

  bool m_activate_MagneticShield_build       = true;
  bool m_activate_PhotonDetector_build       = true;
  bool m_activate_QuartzWindow_build         = true;
  bool m_activate_ExitWall_build             = true;
  bool m_activate_ExitWallDiaphram_build     = true;
  bool m_activate_Rich1Mirror1_build         = true;
  bool m_activate_Rich1Mirror1_Segment_build = true;
  bool m_activate_Rich1Mirror1_CaFiCyl_build = true;
  bool m_activate_Rich1Mirror2_build         = true;
  bool m_activate_Rich1SubMaster_build       = true;
  bool m_activate_Rich1BeamPipe_build        = true;

  bool m_activate_RichMatPropTable_build = true;

  bool m_activate_RichStd_MatPropTable_Attach      = true;
  bool m_activate_RichSpecific_MatPropTable_Attach = true;

  bool m_activate_RichMatGeneralPropTable_build = true;
  bool m_activate_RichMatPmtPT_ExtraSet_build   = false;

  bool m_activate_Rich1Surface_build    = true;
  bool m_activate_Rich1PmtSurface_build = true;
  bool m_activate_Rich1QWSurface_build  = true;

  bool m_activate_RetrieveAndPrintForDebug_MatTable     = false;
  bool m_activate_RetrieveAndPrintForDebug_GeneralTable = false;

  bool m_debugActivate                          = true;
  bool m_VolumeBuilderDebugActivate             = true;
  bool m_debugLvListActivate                    = true;
  bool m_generalutilDebugActivate               = true;
  bool m_pmtutilDebugActivate                   = true;
  bool m_R1MirrorDebugActivate                  = true;
  bool m_R1magShieldDebugActivate               = true;
  bool m_RichSurfaceDebugActivate               = true;
  bool m_RichPmtSurfaceDebugActivate            = true;
  bool m_activate_Rich1_DetElem_For_CurrentAppl = true;
  bool m_activate_Rich1_DetElem_For_PmtEcr      = true;

  bool        m_activateVolumeDebug = false;
  std::string m_attachVolumeForDebug;

  /// Helper class to build the Rich1 detector of LHCb
  struct Rich1Build : public dd4hep::xml::tools::VolumeBuilder {

    std::string select_Rich1_Volume{"lvRich1Master"};

    /// Initializing constructor
    Rich1Build( dd4hep::Detector& description, xml_elt_t e, dd4hep::SensitiveDetector sens );

    /// Default destructor
    virtual ~Rich1Build(){};

    /// Rich1 build methods

    void build_Rich1Mirror1();
    void build_Rich1Mirror1CaFiCyl();
    void build_Rich1Mirror2();

    void build_Rich1PhDetSupFrames();
    void build_Rich1GasQuartzWindows();
    void build_Rich1PmtFullSystem();
    void build_Rich1PmtModuleVols();
    void build_Rich1PmtECRVols();
    void build_Rich1PmtMasterVols();
    void build_Rich1MagSh();
    void build_Rich1MagneticShieldComponents();
    void build_Rich1ExitWall();
    void build_Rich1SubMaster();
    void build_Rich1Master();

    void build_Rich_OpticalProperties();
    void build_Rich_General_Properties();

    void build_RichSingle_QE_Table( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichMatQE aQEType );
    void build_Rich_SingleMaterial_StdOpticalProperty( RichMatNameWOP aMatNameW, RichMatPropType aPropType );
    void Attach_RichMaterial_OpticalPropertiesTable( RichMatNameWOP aMatNameW, RichMatPropType aPropType );
    void Attach_RichMaterial_ScintPropertiesTable( RichMatNameWOP aMatNameW, RichMatPropType aPropType,
                                                   RichMatScint aScType );

    void build_RichMaterial_OpticalProperties();
    void RetrieveRichMatProperty( RichMatNameWOP aMatNameW, RichMatPropType aPropType );
    void RetriveAndPrintRichPmtQE( RichMatPropType aPropType, RichMatQE aQEType );
    void RetriveAndPrintRichPmtHV( RichMatPropType aPropType, RichMatHV aHvType );
    void RetriveAndPrintRichGasQWARCoatingRefl( RichMatPropType aPropType, RichSurfCoat aARType );

    void build_Rich_PmtHV( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichMatHV ahvType );
    void build_Rich_ScProp( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichMatScint aScType );
    void build_Rich_QWARCoatRefl( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichSurfCoat aARType );

    void   build_Rich1_Main();
    size_t loadRichTransforms( xml_h element, const std::string& tag );
    void   build_RichTransforms( xml_h tr_handle );

    void BuildARich1PhysVol( const std::string aMotherLVName, const std::string aChildLVName,
                             RichGeomTransformLabel aTransformLabel );

    void BuildARichPmtPhysVol( const std::string aMotherLVName, const std::string aChildLVName,
                               RichPmtGeomTransformLabel aTransformLabel );

    dd4hep::PlacedVolume BuildARich1PhysVolWithPVRet( const std::string aMotherLVName, const std::string aChildLVName,
                                                      RichGeomTransformLabel aTransformLabel );
    dd4hep::PlacedVolume BuildARichPmtPhysVolWithPVRet( const std::string aMotherLVName, const std::string aChildLVName,
                                                        RichPmtGeomTransformLabel aTransformLabel );
    std::vector<dd4hep::PlacedVolume> BuildRich1StdMapmtCompPhysVols();

    dd4hep::PlacedVolume BuildARich1PmtModulePhysVol( int iModule );

    dd4hep::PlacedVolume BuildARich1PmtEcrPhysVol( int aModule, int aEcr );
    dd4hep::PlacedVolume BuildARich1PmtMasterPhysVol( int iModule, int aEcr, int aPmt );
    dd4hep::PlacedVolume BuildARich1PmtSMasterPhysVol( std::string aPmtMasterName );
    void                 BuildARich1PmtShieldingPhysVol( std::string aEcrName );
    void                 build_Rich1PmtAllPhysVols();
  };
  //=====================================================================================

  /// Initializing constructor
  Rich1Build::Rich1Build( dd4hep::Detector& dsc, xml_elt_t e, dd4hep::SensitiveDetector sens )
      : dd4hep::xml::tools::VolumeBuilder( dsc, e, sens ) {

    xml_comp_t x_dbg = x_det.child( _U( debug ), false );
    if ( x_dbg ) {
      for ( xml_coll_t i( x_dbg, _U( item ) ); i; ++i ) {
        xml_comp_t  c( i );
        std::string aS = c.nameStr();

        if ( aS == "activate_MagneticShield_build" ) {
          m_activate_MagneticShield_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_PhotonDetector_build" ) {
          m_activate_PhotonDetector_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_QuartzWindow_build" ) {
          m_activate_QuartzWindow_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_ExitWall_build" ) {
          m_activate_ExitWall_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_ExitWallDiaphram_build" ) {
          m_activate_ExitWallDiaphram_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "debugActivate" ) {
          m_debugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "VolumeBuilderDebugActivate" ) {
          m_VolumeBuilderDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "debugLvListActivate" ) {
          m_debugLvListActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "generalutilDebugActivate" ) {
          m_generalutilDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "pmtutilDebugActivate" ) {
          m_pmtutilDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "R1MirrorDebugActivate" ) {
          m_R1MirrorDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "R1magShieldDebugActivate" ) {
          m_R1magShieldDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "RichSurfaceDebugActivate" ) {
          m_RichSurfaceDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "RichPmtSurfaceDebugActivate" ) {
          m_RichPmtSurfaceDebugActivate = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1Mirror1_build" ) {
          m_activate_Rich1Mirror1_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1Mirror2_build" ) {
          m_activate_Rich1Mirror2_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1Mirror1_Segment_build" ) {
          m_activate_Rich1Mirror1_Segment_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1Mirror1_CaFiCyl_build" ) {
          m_activate_Rich1Mirror1_CaFiCyl_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1SubMaster_build" ) {
          m_activate_Rich1SubMaster_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1BeamPipe_build" ) {
          m_activate_Rich1BeamPipe_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activateVolumeDebug" ) {
          m_activateVolumeDebug = c.attr<bool>( _U( value ) );
        } else if ( aS == "attachVolumeForDebug" ) {
          m_attachVolumeForDebug = c.attr<std::string>( _U( value ) );
        } else if ( aS == "activate_RichMatPropTable_build" ) {
          m_activate_RichMatPropTable_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_RichStd_MatPropTable_Attach" ) {
          m_activate_RichStd_MatPropTable_Attach = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_RichSpecific_MatPropTable_Attach" ) {
          m_activate_RichSpecific_MatPropTable_Attach = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_RichMatGeneralPropTable_build" ) {
          m_activate_RichMatGeneralPropTable_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_RichMatPmtPT_ExtraSet_build" ) {
          m_activate_RichMatPmtPT_ExtraSet_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1Surface_build" ) {
          m_activate_Rich1Surface_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1PmtSurface_build" ) {
          m_activate_Rich1PmtSurface_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1QWSurface_build" ) {
          m_activate_Rich1QWSurface_build = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_RetrieveAndPrintForDebug_MatTable" ) {
          m_activate_RetrieveAndPrintForDebug_MatTable = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_RetrieveAndPrintForDebug_GeneralTable" ) {
          m_activate_RetrieveAndPrintForDebug_GeneralTable = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1_DetElem_For_CurrentAppl" ) {
          m_activate_Rich1_DetElem_For_CurrentAppl = c.attr<bool>( _U( value ) );
        } else if ( aS == "activate_Rich1_DetElem_For_PmtEcr" ) {
          m_activate_Rich1_DetElem_For_PmtEcr = c.attr<bool>( _U( value ) );
        }
      }
    }
    debug = m_VolumeBuilderDebugActivate;
  }
  //=====================================================================================//
  void Rich1Build::BuildARich1PhysVol( const std::string aMotherLVName, const std::string aChildLVName,
                                       RichGeomTransformLabel aTransformLabel ) {

    dd4hep::PlacedVolume aPvol = BuildARich1PhysVolWithPVRet( aMotherLVName, aChildLVName, aTransformLabel );
    if ( m_debugActivate ) {
      const char* aPvolName = aPvol->GetName();

      printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                "Rich1_Geo : ", "Created Rich1 PhysVol with the name  %s", aPvolName );
    }
  }
  //=====================================================================================//
  dd4hep::PlacedVolume Rich1Build::BuildARich1PhysVolWithPVRet( const std::string      aMotherLVName,
                                                                const std::string      aChildLVName,
                                                                RichGeomTransformLabel aTransformLabel ) {
    dd4hep::Volume      lvMother     = volume( aMotherLVName );
    dd4hep::Volume      lvChild      = volume( aChildLVName );
    RichGeoUtil*        aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();
    dd4hep::Transform3D TransformForChild =
        aRichGeoUtil->getRichGeomTransform( aRichGeoUtil->getRichTransformName( aTransformLabel ) );

    int aChildCopyNumber = aRichGeoUtil->getPhysVolCopyNumber( aTransformLabel );

    dd4hep::PlacedVolume pvChild = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );
    // dd4hep::PlacedVolume pvChild =  lvMother.placeVolume(lvChild,TransformForChild);

    pvChild->SetName( ( aRichGeoUtil->getRichPhysVolName( aTransformLabel ) ).c_str() );

    // Now setup debug printout options

    if ( m_debugActivate ) {
      bool aCdebug = m_debugActivate;

      if ( aChildLVName.find( "lvRich1Mgs" ) != std::string::npos ) aCdebug = m_R1magShieldDebugActivate;
      printout( aCdebug ? dd4hep::ALWAYS : dd4hep::DEBUG,
                "Rich1_Geo : ", "Created PhysVol with name %s , copynumber %d , TransformName  %s ",
                ( aRichGeoUtil->getRichPhysVolName( aTransformLabel ) ).c_str(), aChildCopyNumber,
                ( aRichGeoUtil->getRichTransformName( aTransformLabel ) ).c_str() );
    }
    return pvChild;
  }
  //=====================================================================================//
  void Rich1Build::BuildARichPmtPhysVol( const std::string aMotherLVName, const std::string aChildLVName,
                                         RichPmtGeomTransformLabel aTransformLabel ) {

    dd4hep::PlacedVolume aPvol = BuildARichPmtPhysVolWithPVRet( aMotherLVName, aChildLVName, aTransformLabel );
    if ( m_debugActivate ) {
      const char* aPvolName = aPvol->GetName();

      printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                "Rich1_Geo : ", "Created Rich1 Pmt PhysVol with the name  %s", aPvolName );
    }
  }

  //=====================================================================================//
  dd4hep::PlacedVolume Rich1Build::BuildARichPmtPhysVolWithPVRet( const std::string         aMotherLVName,
                                                                  const std::string         aChildLVName,
                                                                  RichPmtGeomTransformLabel aTransformLabel ) {
    dd4hep::Volume      lvMother       = volume( aMotherLVName );
    dd4hep::Volume      lvChild        = volume( aChildLVName );
    auto                aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();
    dd4hep::Transform3D TransformForChild =
        aRichPmtGeoAux->getRichPmtCompGeomTransform( aRichPmtGeoAux->getRichPmtCompTransformName( aTransformLabel ) );

    int aChildCopyNumber = aRichPmtGeoAux->getRichPmtCompPhysVolCopyNumber( aTransformLabel );

    dd4hep::PlacedVolume pvChild = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );
    // dd4hep::PlacedVolume pvChild =  lvMother.placeVolume(lvChild,TransformForChild);

    pvChild->SetName( ( aRichPmtGeoAux->getRichPmtCompPhysVolName( aTransformLabel ) ).c_str() );

    // Now setup debug printout options

    if ( m_debugActivate ) {

      printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                "Rich1_Geo : ", "Created Pmt PhysVol with name %s , copynumber %d , TransformName  %s ",
                ( aRichPmtGeoAux->getRichPmtCompPhysVolName( aTransformLabel ) ).c_str(), aChildCopyNumber,
                ( aRichPmtGeoAux->getRichPmtCompTransformName( aTransformLabel ) ).c_str() );
    }
    return pvChild;
  }

  //=====================================================================================//
  dd4hep::PlacedVolume Rich1Build::BuildARich1PmtModulePhysVol( int iModule ) {
    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();

    dd4hep::Volume lvMother = ( iModule < aRichPmtGeoAux->Rh1TotalNumPmtModulesInPanel() )
                                  ? volume( "lvRich1PhDetSupFrameH0" )
                                  : volume( "lvRich1PhDetSupFrameH1" );

    dd4hep::Volume lvChild = volume( aRichPmtGeoAux->Rich1PmtModuleLogVolName( iModule ) );
    // dd4hep::Volume  lvChild  = aPmtModuleLV;
    dd4hep::Transform3D  TransformForChild = aRichPmtGeoAux->getRich1PmtModuleTransform( iModule );
    int                  aChildCopyNumber  = iModule;
    dd4hep::PlacedVolume pvChild           = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );
    pvChild->SetName( ( aRichPmtGeoAux->Rich1PmtModulePhysVolName( iModule ) ).c_str() );
    // Now for the detector element structure
    // if(m_activate_Rich1_DetElem_For_CurrentAppl ) {
    std::string aR1PmtModuleDetName = aRichPmtGeoAux->Rich1PmtModuleDetName( iModule );
    pvChild.addPhysVolID( aR1PmtModuleDetName + "Det", aChildCopyNumber );

    // }

    printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
              "Rich1_Geo : ", "Created Rich1 PhysVol for MaPMT Module %d", iModule );

    return pvChild;
  }

  //=====================================================================================//
  dd4hep::PlacedVolume Rich1Build::BuildARich1PmtEcrPhysVol( int aModule, int aEcr ) {
    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();
    int  aNe            = aRichPmtGeoAux->RhMaxNumECRInModule();

    dd4hep::Volume lvMother = volume( aRichPmtGeoAux->Rich1PmtModuleLogVolName( aModule ) );
    dd4hep::Volume lvChild  = volume( aRichPmtGeoAux->Rich1PmtECRLogVolName( aModule, aEcr ) );

    dd4hep::Transform3D TransformForChild = aRichPmtGeoAux->getRich1PmtECRTransform( aEcr );

    int aChildCopyNumber = ( aModule * aNe ) + aEcr;

    dd4hep::PlacedVolume pvChild = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );
    pvChild->SetName( ( aRichPmtGeoAux->Rich1PmtECRPhysVolName( aModule, aEcr ) ).c_str() );
    std::string aR1PmtEcrDetName = aRichPmtGeoAux->Rich1PmtECRDetName( aModule, aEcr );
    pvChild.addPhysVolID( aR1PmtEcrDetName + "Det", aChildCopyNumber );

    printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
              "Rich1_Geo : ", "Created Rich1 PhysVol for MaPMT Ecr %d in Module %d", aEcr, aModule );
    return pvChild;
  }

  //=====================================================================================//

  dd4hep::PlacedVolume Rich1Build::BuildARich1PmtMasterPhysVol( int aModule, int aEca, int aPmt ) {

    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();
    // int            aMe            = aRichPmtGeoAux->RhNumPmtInStdModule();
    // int            aPe            = aRichPmtGeoAux->RhNumPMTInECR();
    dd4hep::Volume lvMother = volume( aRichPmtGeoAux->Rich1PmtECRLogVolName( aModule, aEca ) );
    dd4hep::Volume lvChild  = volume( aRichPmtGeoAux->Rich1PmtMasterLogVolName( aModule, aEca, aPmt ) );

    dd4hep::Transform3D TransformForChild = aRichPmtGeoAux->getRich1PmtMasterTransform( aPmt );

    // int aChildCopyNumber = ( aModule * aMe ) + ( aEca * aPe ) + aPmt;
    int aChildCopyNumber = aRichPmtGeoAux->getRich1PmtMasterCopyNumber( aModule, aEca, aPmt );

    dd4hep::PlacedVolume pvChild = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );

    pvChild->SetName( ( aRichPmtGeoAux->Rich1PmtMasterPhysVolName( aModule, aEca, aPmt ) ).c_str() );
    std::string aR1PmtMasterDetName = aRichPmtGeoAux->Rich1PmtMasterDetName( aModule, aEca, aPmt );
    pvChild.addPhysVolID( aR1PmtMasterDetName + "Det", aChildCopyNumber );

    printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
              "Rich1_Geo : ", "Created Rich1 PhysVol for MaPMT Master %d in Ecr %d  in Module %d", aPmt, aEca,
              aModule );
    return pvChild;
  }

  //=====================================================================================//

  dd4hep::PlacedVolume Rich1Build::BuildARich1PmtSMasterPhysVol( std::string aPmtMasterName ) {

    //  The following line commented out and replaced by the lines below until it is determined
    // if each pv for pmtSMaster need to have a different name.
    //   BuildARichPmtPhysVol( aPmtMasterName,"lvRichPMTSMaster",
    //			RichPmtGeomTransformLabel::pvRichPMTSMasterTN);

    auto                      lvMother         = volume( aPmtMasterName );
    auto                      lvChild          = volume( "lvRichPMTSMaster" );
    auto                      aRichPmtGeoAux   = RichPmtGeoAux::getRichPmtGeoAuxInstance();
    RichPmtGeomTransformLabel aPmtSMasterLabel = RichPmtGeomTransformLabel::pvRichPMTSMasterTN;
    dd4hep::Transform3D       TransformForChild =
        aRichPmtGeoAux->getRichPmtCompGeomTransform( aRichPmtGeoAux->getRichPmtCompTransformName( aPmtSMasterLabel ) );

    int aChildCopyNumber = aRichPmtGeoAux->getRichPmtCompPhysVolCopyNumber( aPmtSMasterLabel );

    dd4hep::PlacedVolume pvSMaster = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );

    // Possibility here to give different physvol names to different PmtSMaster in different pmt master.
    // For that one may need extra arguements for the module ecr numbers etc to this method.
    // for now same name is assigned.
    pvSMaster->SetName( ( aRichPmtGeoAux->getRichPmtCompPhysVolName( aPmtSMasterLabel ) ).c_str() );

    printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
              "Rich1_Geo : ", "Created Rich1 Pmt PhysVol for MaPMT SMaster in PmtMaster %s", aPmtMasterName.c_str() );

    return pvSMaster;
  }

  //=====================================================================================//

  void Rich1Build::BuildARich1PmtShieldingPhysVol( std::string aEcrVolName ) {

    BuildARichPmtPhysVol( aEcrVolName, "lvRichPmtShieldingLongPlate",
                          RichPmtGeomTransformLabel::pvRichPmtSingleShieldingLongPlateTN );
    BuildARichPmtPhysVol( aEcrVolName, "lvRichPmtShieldingSemiPlate",
                          RichPmtGeomTransformLabel::pvRichPmtSingleShieldingSemiPlateTopTN );
    BuildARichPmtPhysVol( aEcrVolName, "lvRichPmtShieldingSemiPlate",
                          RichPmtGeomTransformLabel::pvRichPmtSingleShieldingSemiPlateBottomTN );

    printout( m_pmtutilDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
              "Rich1_Geo : ", "Created Rich1 PhysVol for MaPMT Shielding Vols in PmtEcr %s", aEcrVolName.c_str() );
  }

  //=====================================================================================//

  void Rich1Build::build_Rich1Mirror1() {

    auto                          aRichGeoUtil     = RichGeoUtil::getRichGeoUtilInstance();
    auto                          aRichSurfaceUtil = RichSurfaceUtil::getRichSurfaceUtilInstance();
    dd4hep::OpticalSurfaceManager surfMgr          = description.surfaceManager();

    dd4hep::Volume lvMother = volume( aRichGeoUtil->Rich1Mirror1MasterLVName() );

    for ( int iq = 0; iq < ( aRichGeoUtil->Rich1NumMirror1Seg() ); iq++ ) {
      dd4hep::Volume      lvD = volume( aRichGeoUtil->Rich1Mirror1LogVolName( 0, iq ) );
      dd4hep::Transform3D TransformForD =
          aRichGeoUtil->getRichGeomTransform( aRichGeoUtil->Rich1Mirror1TransformName( 0, iq ) );
      int                  aDCopyNumber = aRichGeoUtil->Rich1Mirror1CopyNumber( 0, iq );
      dd4hep::PlacedVolume pvD          = lvMother.placeVolume( lvD, aDCopyNumber, TransformForD );
      pvD->SetName( ( aRichGeoUtil->Rich1Mirror1PhysVolName( 0, iq ) ).c_str() );
      // Now for DetElem structure for this quadrant
      dd4hep::DetElement aRich1M1QDet;
      if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
        std::string aR1M1QDetName = aRichGeoUtil->Rich1Mirror1DetName( 0, iq );
        pvD.addPhysVolID( aR1M1QDetName + "Det", aDCopyNumber );
        aRich1M1QDet = dd4hep::DetElement( detector, aR1M1QDetName, aDCopyNumber );
        aRich1M1QDet.setPlacement( pvD );
      }

      if ( m_activate_Rich1Mirror1_Segment_build ) {
        for ( int itp = 1; itp < ( aRichGeoUtil->Rich1NumMirror1VolTypes() ); itp++ ) {
          dd4hep::Volume      lvChild = volume( aRichGeoUtil->Rich1Mirror1LogVolName( itp, iq ) );
          dd4hep::Transform3D TransformForChild =
              aRichGeoUtil->getRichGeomTransform( aRichGeoUtil->Rich1Mirror1TransformName( itp, iq ) );
          int                  aChildCopyNumber = aRichGeoUtil->Rich1Mirror1CopyNumber( itp, iq );
          dd4hep::PlacedVolume pvChild          = lvD.placeVolume( lvChild, aChildCopyNumber, TransformForChild );
          pvChild->SetName( ( aRichGeoUtil->Rich1Mirror1PhysVolName( itp, iq ) ).c_str() );
          printout( m_R1MirrorDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                    "Rich1_Geo : ", "Created Rich1 PhysVol for Rich1 Mirror1 Segment Vol %s in %d ",
                    ( aRichGeoUtil->Rich1Mirror1PhysVolName( itp, iq ) ).c_str(), iq );

          // Now for DetElem structure for the inner layer of this segment
          if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
            if ( itp == 1 ) {
              std::string aR1M1CfDetName = aRichGeoUtil->Rich1Mirror1DetName( itp, iq );
              pvChild.addPhysVolID( aR1M1CfDetName + "Det", aChildCopyNumber );
              dd4hep::DetElement aRich1M1CfDet = dd4hep::DetElement( aRich1M1QDet, aR1M1CfDetName, aChildCopyNumber );
              aRich1M1CfDet.setPlacement( pvChild );
            }
          }
          // Create the Rich1 Mirror1 surfaces
          if ( m_activate_Rich1Surface_build ) {
            if ( itp == 1 ) {
              dd4hep::OpticalSurface aCurM1Surface =
                  surfMgr.opticalSurface( aRichSurfaceUtil->Rich1Mirror1SurfaceName( iq ) );
              std::string           aCurBorderSurfaceName = aRichSurfaceUtil->Rich1Mirror1BorderSurfaceName( iq );
              dd4hep::BorderSurface aR1M1QuadBorderSurface =
                  dd4hep::BorderSurface( description, detector, aCurBorderSurfaceName, aCurM1Surface, pvD, pvChild );
              aR1M1QuadBorderSurface.isValid();

              printout( m_RichSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                        "Rich1_Geo : ", "Created Rich1Mirror1 Surface in quadrant %d with Name %s using %s", iq,
                        aCurBorderSurfaceName.c_str(), ( aRichSurfaceUtil->Rich1Mirror1SurfaceName( iq ) ).c_str() );
              if ( m_RichSurfaceDebugActivate && m_R1MirrorDebugActivate ) {

                aRichSurfaceUtil->PrintRichSurfProperty( aCurM1Surface, RichSurfPropType::Refl );
                aRichSurfaceUtil->PrintRichSurfProperty( aCurM1Surface, RichSurfPropType::Effic );
              }
            }
          }
        }
      }
    }

    if ( m_activate_Rich1Mirror1_CaFiCyl_build ) build_Rich1Mirror1CaFiCyl();

    BuildARich1PhysVol( ( aRichGeoUtil->Rich1SubMasterLVName() ), ( aRichGeoUtil->Rich1Mirror1MasterLVName() ),
                        RichGeomTransformLabel::pvRich1Mirror1MasterTN );
  }
  //=====================================================================================//
  void Rich1Build::build_Rich1Mirror1CaFiCyl() {

    auto aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();

    dd4hep::Volume lvCylChild = volume( aRichGeoUtil->Rich1Mirror1CaFiLVName() );

    for ( int iQ = 0; iQ < ( aRichGeoUtil->Rich1NumMirror1Seg() ); iQ++ ) {
      dd4hep::Volume lvMQ = volume( aRichGeoUtil->Rich1Mirror1LogVolName( 0, iQ ) );
      for ( int ic = 0; ic < ( aRichGeoUtil->Rich1Mirror1NumCaFiCylInASeg() ); ++ic ) {
        dd4hep::Transform3D  TransformForCylChild = aRichGeoUtil->Rich1Mirror1CaFiCylinderTransform( iQ, ic );
        int                  aCylChildCopyNumber  = aRichGeoUtil->Rich1Mirror1CaFiCylinderCopyNumber( iQ, ic );
        dd4hep::PlacedVolume pvCylChild = lvMQ.placeVolume( lvCylChild, aCylChildCopyNumber, TransformForCylChild );
        pvCylChild->SetName( ( aRichGeoUtil->Rich1Mirror1CaFiCylinderPhysVolName( iQ, ic ) ).c_str() );
        printout( m_R1MirrorDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                  "Rich1_Geo : ", "Created Rich1 PhysVol for Rich1 Mirror1 CaFi Vol %d in quadrant %d ", ic, iQ );
      }
    }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1Mirror2() {

    auto aRichGeoUtil     = RichGeoUtil::getRichGeoUtilInstance();
    auto aRichSurfaceUtil = RichSurfaceUtil::getRichSurfaceUtilInstance();
    auto surfMgr          = description.surfaceManager();

    int         aNumM2Systems = aRichGeoUtil->Rich1NumMirror2Systems();
    int         aNumSegInHalf = aRichGeoUtil->Rich1NumMirror2SegInAHalf();
    std::string aR1SubMLVName = aRichGeoUtil->Rich1SubMasterLVName();

    for ( int iMM = 0; iMM < aNumM2Systems; iMM++ ) {
      std::string    aRich1Mirror2MasterLVName = aRichGeoUtil->Rich1Mirror2MasterLVName( iMM );
      dd4hep::Volume lvMother                  = volume( aRich1Mirror2MasterLVName );

      RichGeomTransformLabel aR1M2MasterTN = ( iMM == 0 ) ? ( RichGeomTransformLabel::pvRich1Mirror2MasterTopTN )
                                                          : ( RichGeomTransformLabel::pvRich1Mirror2MasterBotTN );
      dd4hep::PlacedVolume pvRich1Mirror2CurMaster =
          BuildARich1PhysVolWithPVRet( aR1SubMLVName, aRich1Mirror2MasterLVName, aR1M2MasterTN );

      RichGeomTransformLabel aR1M2SupTN = ( iMM == 0 ) ? ( RichGeomTransformLabel::pvRich1Mirror2SupportTopTN )
                                                       : ( RichGeomTransformLabel::pvRich1Mirror2SupportBotTN );

      BuildARich1PhysVol( aR1SubMLVName, ( aRichGeoUtil->Rich1Mirror2SupportLVName( iMM ) ), aR1M2SupTN );
      // Now for the detector element structure for the current Rich1 Mirror2 master
      dd4hep::DetElement aRich1M2MasterDet;
      if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
        std::string aR1M2MasterDetName = aRichGeoUtil->Rich1Mirror2MasterDetName( iMM );
        int         aR1M2MasterDetId   = aRichGeoUtil->getPhysVolCopyNumber( aR1M2MasterTN );
        pvRich1Mirror2CurMaster.addPhysVolID( aR1M2MasterDetName + "Det", aR1M2MasterDetId );
        aRich1M2MasterDet = dd4hep::DetElement( detector, aR1M2MasterDetName, aR1M2MasterDetId );
        aRich1M2MasterDet.setPlacement( pvRich1Mirror2CurMaster );
      }

      for ( int iseg = 0; iseg < aNumSegInHalf; iseg++ ) {
        int                 isegF   = ( iMM * aNumSegInHalf ) + iseg;
        dd4hep::Volume      lvChild = volume( aRichGeoUtil->Rich1Mirror2LogVolName( isegF ) );
        dd4hep::Transform3D TransformForChild =
            aRichGeoUtil->getRichGeomTransform( aRichGeoUtil->Rich1Mirror2TransformName( isegF ) );
        int                  aChildCopyNumber = aRichGeoUtil->Rich1Mirror2CopyNumber( isegF );
        dd4hep::PlacedVolume pvChild          = lvMother.placeVolume( lvChild, aChildCopyNumber, TransformForChild );
        pvChild->SetName( ( aRichGeoUtil->Rich1Mirror2PhysVolName( isegF ) ).c_str() );

        printout( m_R1MirrorDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                  "Rich1_Geo : ", "Created Rich1 PhysVol for Rich1 Mirror2 Segment %d in Half %d ", iseg, iMM );

        // Now for the detector element structure for the current Rich1 Mirror2 segment
        if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
          std::string aR1M2SegDetName = aRichGeoUtil->Rich1Mirror2DetName( isegF );
          pvChild.addPhysVolID( aR1M2SegDetName + "Det", aChildCopyNumber );
          dd4hep::DetElement aRich1M2SegDet =
              dd4hep::DetElement( aRich1M2MasterDet, aR1M2SegDetName, aChildCopyNumber );
          aRich1M2SegDet.setPlacement( pvChild );
        }
        // Now create Rich1Mirror2 surface
        if ( m_activate_Rich1Surface_build ) {

          dd4hep::OpticalSurface aCurM2Surface =
              surfMgr.opticalSurface( aRichSurfaceUtil->Rich1Mirror2SurfaceName( isegF ) );
          std::string           aCurM2BorderSurfaceName = aRichSurfaceUtil->Rich1Mirror2BorderSurfaceName( isegF );
          dd4hep::BorderSurface aR1M2SegBorderSurface   = dd4hep::BorderSurface(
              description, detector, aCurM2BorderSurfaceName, aCurM2Surface, pvRich1Mirror2CurMaster, pvChild );
          aR1M2SegBorderSurface.isValid();
          printout( m_RichSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                    "Rich1_Geo : ", "Created Rich1Mirror2 Surface in segment %d with Name %s using %s", isegF,
                    aCurM2BorderSurfaceName.c_str(), ( aRichSurfaceUtil->Rich1Mirror2SurfaceName( isegF ) ).c_str() );

          if ( m_RichSurfaceDebugActivate && m_R1MirrorDebugActivate ) {

            aRichSurfaceUtil->PrintRichSurfProperty( aCurM2Surface, RichSurfPropType::Refl );
            aRichSurfaceUtil->PrintRichSurfProperty( aCurM2Surface, RichSurfPropType::Effic );
          }
        }
      }
    }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1PhDetSupFrames() {
    auto aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();

    for ( int iSide = 0; iSide < ( aRichGeoUtil->Rich1NumSides() ); iSide++ ) {

      std::string aMagShLvName = aRichGeoUtil->Rich1MagShLVName( iSide );

      RichGeomTransformLabel aRich1PhDetSupFrameTN = ( iSide == 0 ) ? RichGeomTransformLabel::pvRich1PhDetSupFrameH0TN
                                                                    : RichGeomTransformLabel::pvRich1PhDetSupFrameH1TN;

      std::string aRich1PhDetSupFrameLvName = aRichGeoUtil->Rich1PhDetSupFrameLVName( iSide );

      // BuildARich1PhysVol( aMagShLvName, aRich1PhDetSupFrameLvName, aRich1PhDetSupFrameTN );

      dd4hep::PlacedVolume aRich1PhDetSupFramePv =
          BuildARich1PhysVolWithPVRet( aMagShLvName, aRich1PhDetSupFrameLvName, aRich1PhDetSupFrameTN );
      // Now for the detector element structure
      if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
        std::string aR1PhDetSupDetName = aRichGeoUtil->Rich1PhDetSupFrameDetName( iSide );
        int         aR1PhDetSupId      = aRichGeoUtil->getPhysVolCopyNumber( aRich1PhDetSupFrameTN );
        aRich1PhDetSupFramePv.addPhysVolID( aR1PhDetSupDetName + "Det", aR1PhDetSupId );
        dd4hep::DetElement aR1PhDetSupDet = dd4hep::DetElement( detector, aR1PhDetSupDetName, aR1PhDetSupId );
        aR1PhDetSupDet.setPlacement( aRich1PhDetSupFramePv );
      }
    }

    if ( m_activate_PhotonDetector_build ) { build_Rich1PmtFullSystem(); }
  }

  //=====================================================================================//

  void Rich1Build::build_Rich1GasQuartzWindows() {

    auto aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();

    auto                          aRichSurfaceUtil = RichSurfaceUtil::getRichSurfaceUtilInstance();
    dd4hep::OpticalSurfaceManager surfMgr          = description.surfaceManager();
    dd4hep::Volume                lvRich1SubM      = volume( aRichGeoUtil->Rich1SubMasterLVName() );
    dd4hep::OpticalSurface        aR1GasQWSurface =
        surfMgr.opticalSurface( aRichSurfaceUtil->Rich1GasQuartzWindowSurfaceName() );

    for ( int iSide = 0; iSide < ( aRichGeoUtil->Rich1NumSides() ); iSide++ ) {

      std::string            aMagShLvName = aRichGeoUtil->Rich1MagShLVName( iSide );
      RichGeomTransformLabel aRich1GQuartzWTN =
          ( iSide == 0 ) ? RichGeomTransformLabel::pvRich1GQuartzWH0TN : RichGeomTransformLabel::pvRich1GQuartzWH1TN;
      std::string aRich1GQuartzWLvName = aRichGeoUtil->Rich1GasQuartzWLVName();

      dd4hep::PlacedVolume aPvRich1GQuartzWCur =
          BuildARich1PhysVolWithPVRet( aMagShLvName, aRich1GQuartzWLvName, aRich1GQuartzWTN );

      if ( m_activate_Rich1QWSurface_build ) {

        RichGeomTransformLabel aRich1MagShTN =
            ( iSide == 0 ) ? RichGeomTransformLabel::pvRich1MagShH0TN : RichGeomTransformLabel::pvRich1MagShH1TN;
        std::string aPvMagShName = aRichGeoUtil->getRichPhysVolName( aRich1MagShTN );

        dd4hep::PlacedVolume     pvMagSHCur     = dd4hep::PlacedVolume( lvRich1SubM->GetNode( aPvMagShName.c_str() ) );
        std::vector<std::string> aGasQWBorderSN = {
            aRichSurfaceUtil->Rich1GasQuartzWindowBorderSurfaceName( iSide ),
            aRichSurfaceUtil->Rich1GasQuartzWindowBorderBackSurfaceName( iSide )};
        for ( int iss = 0; iss < (int)aGasQWBorderSN.size(); iss++ ) {

          dd4hep::BorderSurface aR1GasQWBorderSurface =
              ( iss == 0 ) ? dd4hep::BorderSurface( description, detector, aGasQWBorderSN[iss], aR1GasQWSurface,
                                                    pvMagSHCur, aPvRich1GQuartzWCur )
                           : dd4hep::BorderSurface( description, detector, aGasQWBorderSN[iss], aR1GasQWSurface,
                                                    aPvRich1GQuartzWCur, pvMagSHCur );
          aR1GasQWBorderSurface.isValid();

          printout( m_RichSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                    "Rich1_Geo : ", "Created Rich1GasQW Surface with Name %s using %s", ( aGasQWBorderSN[iss] ).c_str(),
                    ( aRichSurfaceUtil->Rich1GasQuartzWindowSurfaceName() ).c_str() );
        }
      }
    }

    if ( m_activate_Rich1QWSurface_build && m_RichSurfaceDebugActivate ) {

      aRichSurfaceUtil->PrintRichSurfProperty( aR1GasQWSurface, RichSurfPropType::Refl );
      aRichSurfaceUtil->PrintRichSurfProperty( aR1GasQWSurface, RichSurfPropType::Effic );
      aRichSurfaceUtil->PrintRichSurfProperty( aR1GasQWSurface, RichSurfPropType::Trans );
    }
  }
  //=====================================================================================//
  void Rich1Build::build_Rich1PmtFullSystem() {
    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();

    aRichPmtGeoAux->initRich1PmtStructures();
    if ( aRichPmtGeoAux->Rich1PmtStructureCreated() ) {

      build_Rich1PmtModuleVols();
      build_Rich1PmtECRVols();
      build_Rich1PmtMasterVols();
      build_Rich1PmtAllPhysVols();
    }
  }
  //=====================================================================================//
  void Rich1Build::build_Rich1PmtModuleVols() {
    if ( m_debugActivate ) { dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", "Begin to build PMT modules " ); }

    // get a single pmtModule
    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();

    dd4hep::Volume           lvSinglePmtModule = volume( "lvRich1PmtSingleStdModule" );
    const dd4hep::Material   aMaterial         = lvSinglePmtModule.material();
    const dd4hep::Solid_type aShape            = lvSinglePmtModule.solid();
    const dd4hep::VisAttr    aVis              = lvSinglePmtModule.visAttributes();

    for ( int iM = 0; iM < ( aRichPmtGeoAux->Rich1TotalNumPmtModules() ); iM++ ) {

      const std::string aVolName   = aRichPmtGeoAux->Rich1PmtModuleLogVolName( iM );
      const std::string aShapeName = aRichPmtGeoAux->Rich1PmtModuleShapeName( iM );
      if ( m_pmtutilDebugActivate ) {
        dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", " Rich1Pmt LogVolName ShapeName %s %s", aVolName.c_str(),
                          aShapeName.c_str() );
      }

      dd4hep::Solid_type aShapeM( aShape );
      aShapeM.setName( aShapeName.c_str() );

      dd4hep::Volume aLV( aVolName.c_str(), aShapeM, aMaterial );
      aLV.setVisAttributes( aVis );

      registerShape( aShapeName, aShapeM );
      registerVolume( aVolName, aLV );

    } // end loop over modules

    if ( m_debugActivate ) { dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", "End build PMT modules " ); }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1PmtECRVols() {
    if ( m_debugActivate ) { dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", "Begin to build PMT ECR " ); }
    // get a single pmtECR
    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();

    dd4hep::Volume           lvSinglePmtEcr = volume( "lvRich1PmtSingleECR" );
    const dd4hep::Material   aMaterial      = lvSinglePmtEcr.material();
    const dd4hep::Solid_type aShape         = lvSinglePmtEcr.solid();
    const dd4hep::VisAttr    aVis           = lvSinglePmtEcr.visAttributes();

    // Loop over modules

    for ( int iM = 0; iM < ( aRichPmtGeoAux->Rich1TotalNumPmtModules() ); iM++ ) {

      std::vector<int> curRange = aRichPmtGeoAux->get_Rich1EcrRange( iM );
      int              ibeg     = curRange[0];
      int              iend     = curRange[1];

      // first loop over ecr
      for ( int iec = ibeg; iec < iend; iec++ ) {
        const std::string aEcVolName   = aRichPmtGeoAux->Rich1PmtECRLogVolName( iM, iec );
        const std::string aEcShapeName = aRichPmtGeoAux->Rich1PmtECRShapeName( iM, iec );
        if ( m_pmtutilDebugActivate ) {
          dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", " Rich1Pmt ECR LogVolName ShapeName %s %s", aEcVolName.c_str(),
                            aEcShapeName.c_str() );
        }

        dd4hep::Solid_type aShapeE( aShape );
        aShapeE.setName( aEcShapeName.c_str() );

        dd4hep::Volume aLVE( aEcVolName.c_str(), aShapeE, aMaterial );
        aLVE.setVisAttributes( aVis );
        registerShape( aEcShapeName, aShapeE );
        registerVolume( aEcVolName, aLVE );

      } // end first loop over ECR

    } // end loop over Modules
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1PmtMasterVols() {
    if ( m_debugActivate ) { dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", "Begin to build PMT Master " ); }
    // get a single pmt Master
    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();

    dd4hep::Volume           lvSinglePmtMaster = volume( "lvRich1PmtSingleStdMaster" );
    const dd4hep::Material   aMaterial         = lvSinglePmtMaster.material();
    const dd4hep::Solid_type aShape            = lvSinglePmtMaster.solid();
    const dd4hep::VisAttr    aVis              = lvSinglePmtMaster.visAttributes();

    // Loop over modules

    for ( int iM = 0; iM < ( aRichPmtGeoAux->Rich1TotalNumPmtModules() ); iM++ ) {

      std::vector<int> curRange = aRichPmtGeoAux->get_Rich1EcrRange( iM );
      int              ibeg     = curRange[0];
      int              iend     = curRange[1];

      // first loop over ecr
      for ( int iec = ibeg; iec < iend; iec++ ) {

        // Loop over Pmts
        for ( int ip = 0; ip < ( aRichPmtGeoAux->RhNumPMTInECR() ); ip++ ) {

          const std::string aPmtVolName   = aRichPmtGeoAux->Rich1PmtMasterLogVolName( iM, iec, ip );
          const std::string aPmtShapeName = aRichPmtGeoAux->Rich1PmtMasterShapeName( iM, iec, ip );
          if ( m_pmtutilDebugActivate ) {
            dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", " Rich1Pmt Master LogVolName ShapeName %d %d %d %s %s", iM,
                              iec, ip, aPmtVolName.c_str(), aPmtShapeName.c_str() );
          }

          dd4hep::Solid_type aShapeP( aShape );
          aShapeP.setName( aPmtShapeName.c_str() );

          dd4hep::Volume aLVP( aPmtVolName.c_str(), aShapeP, aMaterial );
          aLVP.setVisAttributes( aVis );
          registerShape( aPmtShapeName, aShapeP );
          registerVolume( aPmtVolName, aLVP );
        }
      }
    }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1PmtAllPhysVols() {
    if ( m_debugActivate ) {
      dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", "Begin to build PMT array structure Phys Vols " );
    }
    auto aRichPmtGeoAux   = RichPmtGeoAux::getRichPmtGeoAuxInstance();
    auto aRichSurfaceUtil = RichSurfaceUtil::getRichSurfaceUtilInstance();

    // Acquire the info needed to create pmt surfaces
    dd4hep::OpticalSurfaceManager surfMgr       = description.surfaceManager();
    dd4hep::OpticalSurface        aRMapmtQWSurf = surfMgr.opticalSurface( aRichSurfaceUtil->RichMapmtQWSurfaceName() );

    dd4hep::OpticalSurface aRMapmtMTSurf = surfMgr.opticalSurface( aRichSurfaceUtil->RichMapmtMTSurfaceName() );
    dd4hep::OpticalSurface aRMapmtANSurf = surfMgr.opticalSurface( aRichSurfaceUtil->RichMapmtAnodeSurfaceName() );

    int                      aNumSurfPairInPmt  = 2; // For the pair of surfaces in the vectors listed below.
    std::vector<std::string> aR1MapmtQWBorderSN = {"RichMaPMTQWBorderSurfaceIn", "RichMaPMTQWBorderBackSurfaceIn"};

    std::vector<std::string> aR1MapmtMTBorderSN = {"RichMaPMTSideEnvMTBorderSurfaceIn",
                                                   "RichMapmtBackEnvMTBorderSurfaceIn"};
    std::string              aR1MapmtANBorderSN = "RichMaPMTAnodeBorderSurfaceIn";

    std::vector<dd4hep::PlacedVolume> aPvRichMapmtComp       = BuildRich1StdMapmtCompPhysVols();
    std::string                       aR1PmtAnodeDetName     = aRichPmtGeoAux->RichMaPmtAnodeDetName();
    std::string                       aR1PmtPhCathodeDetName = aRichPmtGeoAux->RichMaPmtPhCathodeDetName();
    int                               aR1PmtAnodeIdSuffix =
        aRichPmtGeoAux->getRichPmtCompPhysVolCopyNumber( RichPmtGeomTransformLabel::pvRichPMTAnode0000TN );
    int aR1PmtPhCathodeIdSuffix =
        aRichPmtGeoAux->getRichPmtCompPhysVolCopyNumber( RichPmtGeomTransformLabel::pvRichPMTPhCathodeTN );

    dd4hep::DetElement aR1PmtModuleDet;
    for ( int iM = 0; iM < ( aRichPmtGeoAux->Rich1TotalNumPmtModules() ); iM++ ) {
      if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
        std::string aR1PmtModuleDetName = aRichPmtGeoAux->Rich1PmtModuleDetName( iM );
        aR1PmtModuleDet                 = dd4hep::DetElement(
            detector.child( aRichPmtGeoAux->getRich1PhDetSupDetNameFromModuleNum( iM ) ), aR1PmtModuleDetName, iM );
      }
      std::vector<int> curRange = aRichPmtGeoAux->get_Rich1EcrRange( iM );
      int              ibeg     = curRange[0];
      int              iend     = curRange[1];

      // first loop over ecr
      for ( int iec = ibeg; iec < iend; iec++ ) {

        // Loop over Pmts
        for ( int ip = 0; ip < ( aRichPmtGeoAux->RhNumPMTInECR() ); ip++ ) {

          std::string          curPmtMasterVolName     = aRichPmtGeoAux->Rich1PmtMasterLogVolName( iM, iec, ip );
          dd4hep::PlacedVolume aRichMapmtSMPhysVol     = BuildARich1PmtSMasterPhysVol( curPmtMasterVolName );
          dd4hep::PlacedVolume aRichMapmtMasterPhysVol = BuildARich1PmtMasterPhysVol( iM, iec, ip );

          if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
            std::string        aR1PmtMasterDetName = aRichPmtGeoAux->Rich1PmtMasterDetName( iM, iec, ip );
            int                aPmtMasterId        = aRichMapmtMasterPhysVol.copyNumber();
            dd4hep::DetElement aR1PmtMasterDet =
                dd4hep::DetElement( aR1PmtModuleDet, aR1PmtMasterDetName, aPmtMasterId );
            aR1PmtMasterDet.setPlacement( aRichMapmtMasterPhysVol );
            // defining Anode Id to avoid duplication
            int CurPmtMasterCopyNum = aRichPmtGeoAux->getRich1PmtMasterCopyNumber( iM, iec, ip );
            int aR1PmtAnodeId = ( ( aRichPmtGeoAux->RhPmtAnodeDetIdShiftFactor() ) * ( 1 + aR1PmtAnodeIdSuffix ) ) +
                                CurPmtMasterCopyNum;
            int aR1PmtPhCathodeId =
                ( ( aRichPmtGeoAux->RhPmtPhCathodeDetIdShiftFactor() ) * ( 1 + aR1PmtPhCathodeIdSuffix ) ) +
                CurPmtMasterCopyNum;

            dd4hep::DetElement aR1PmtAnodeDet =
                dd4hep::DetElement( aR1PmtMasterDet, aR1PmtAnodeDetName, aR1PmtAnodeId );
            dd4hep::DetElement aR1PmtPhCathodeDet =
                dd4hep::DetElement( aR1PmtMasterDet, aR1PmtPhCathodeDetName, aR1PmtPhCathodeId );

            aR1PmtAnodeDet.setPlacement( aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtAnode )] );
            aR1PmtPhCathodeDet.setPlacement(
                aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtPhc )] );
          }
          aRichPmtGeoAux->setRich1PmtMasterValid( iM, iec, ip );

          if ( m_activate_Rich1PmtSurface_build ) {
            for ( int isPair = 0; isPair < aNumSurfPairInPmt; isPair++ ) {
              dd4hep::BorderSurface aR1MapmtQWBorderSurface =
                  ( isPair == 0 )
                      ? dd4hep::BorderSurface(
                            description, detector, ( aR1MapmtQWBorderSN[isPair] + curPmtMasterVolName ), aRMapmtQWSurf,
                            aRichMapmtSMPhysVol,
                            aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtQW )] )
                      : dd4hep::BorderSurface(
                            description, detector, ( aR1MapmtQWBorderSN[isPair] + curPmtMasterVolName ), aRMapmtQWSurf,
                            aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtQW )],
                            aRichMapmtSMPhysVol );

              dd4hep::BorderSurface aR1MapmtMTBorderSurface =
                  ( isPair == 0 )
                      ? dd4hep::BorderSurface(
                            description, detector, ( aR1MapmtMTBorderSN[isPair] + curPmtMasterVolName ), aRMapmtMTSurf,
                            aRichMapmtSMPhysVol,
                            aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtSideEnv )] )
                      : dd4hep::BorderSurface(
                            description, detector, ( aR1MapmtMTBorderSN[isPair] + curPmtMasterVolName ), aRMapmtMTSurf,
                            aRichMapmtSMPhysVol,
                            aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtBackEnv )] );

              aR1MapmtQWBorderSurface.isValid();
              aR1MapmtMTBorderSurface.isValid();
              if ( m_RichPmtSurfaceDebugActivate ) {

                printout( m_RichPmtSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                          "Rich1_Geo : ", "Created Rich1Pmt QW Surface with Name %s using %s",
                          ( ( aR1MapmtQWBorderSN[isPair] + curPmtMasterVolName ).c_str() ),
                          ( ( aRichSurfaceUtil->RichMapmtQWSurfaceName() ).c_str() ) );

                printout( m_RichPmtSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                          "Rich1_Geo : ", "Created Rich1Pmt MT Surface with Name %s using %s",
                          ( ( aR1MapmtMTBorderSN[isPair] + curPmtMasterVolName ).c_str() ),
                          ( ( aRichSurfaceUtil->RichMapmtMTSurfaceName() ).c_str() ) );
              }
            }
            dd4hep::BorderSurface aR1MapmtAnodeBorderSurface = dd4hep::BorderSurface(
                description, detector, ( aR1MapmtANBorderSN + curPmtMasterVolName ), aRMapmtANSurf, aRichMapmtSMPhysVol,
                aPvRichMapmtComp[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtAnode )] );
            aR1MapmtAnodeBorderSurface.isValid();
            if ( m_RichPmtSurfaceDebugActivate ) {
              printout( m_RichPmtSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                        "Rich1_Geo : ", "Created Rich1Pmt Anode Surface with Name %s using %s",
                        ( ( aR1MapmtANBorderSN + curPmtMasterVolName ).c_str() ),
                        ( ( aRichSurfaceUtil->RichMapmtAnodeSurfaceName() ).c_str() ) );
            }
          }
        }

        std::string curEcrVolname = aRichPmtGeoAux->Rich1PmtECRLogVolName( iM, iec );

        BuildARich1PmtShieldingPhysVol( curEcrVolname );
        dd4hep::PlacedVolume pvPmtEcr = BuildARich1PmtEcrPhysVol( iM, iec );
        if ( m_activate_Rich1_DetElem_For_CurrentAppl && m_activate_Rich1_DetElem_For_PmtEcr ) {
          std::string        aR1PmtEcrDetName = aRichPmtGeoAux->Rich1PmtECRDetName( iM, iec );
          int                aEcrId           = pvPmtEcr.copyNumber();
          dd4hep::DetElement aR1PmtEcrDet     = dd4hep::DetElement( aR1PmtModuleDet, aR1PmtEcrDetName, aEcrId );
          aR1PmtEcrDet.setPlacement( pvPmtEcr );
        }

        aRichPmtGeoAux->setRich1PmtECRValid( iM, iec );
      }
      dd4hep::PlacedVolume pvPmtModule = BuildARich1PmtModulePhysVol( iM );
      if ( m_activate_Rich1_DetElem_For_CurrentAppl ) { aR1PmtModuleDet.setPlacement( pvPmtModule ); }
    }

    if ( m_activate_Rich1PmtSurface_build && m_RichPmtSurfaceDebugActivate ) {
      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtQWSurf, RichSurfPropType::Refl );
      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtQWSurf, RichSurfPropType::Effic );
      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtQWSurf, RichSurfPropType::Trans );

      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtMTSurf, RichSurfPropType::Refl );
      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtMTSurf, RichSurfPropType::Effic );
      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtANSurf, RichSurfPropType::Refl );
      aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtANSurf, RichSurfPropType::Effic );
    }
  }
  //=====================================================================================//

  std::vector<dd4hep::PlacedVolume> Rich1Build::BuildRich1StdMapmtCompPhysVols() {

    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();

    std::vector<dd4hep::PlacedVolume> aPmtCompPvVect( aRichPmtGeoAux->NumRichPmtCompForSurf() );
    int                               aNumSurfPairInPmt = 2; // For the pair of surfaces in the vectors listed below.
    auto                              aRichSurfaceUtil  = RichSurfaceUtil::getRichSurfaceUtilInstance();
    dd4hep::OpticalSurfaceManager     surfMgr           = description.surfaceManager();
    dd4hep::OpticalSurface   aRMapmtPCSurf      = surfMgr.opticalSurface( aRichSurfaceUtil->RichMapmtPCSurfaceName() );
    std::vector<std::string> aR1MapmtPCBorderSN = {"RichMaPMTPCBorderSurfaceIn", "RichMaPMTPCBorderBackSurfaceIn"};

    std::string aPmtSMLvName = aRichPmtGeoAux->RichMaPmtSubMasterLVName();

    dd4hep::Volume aPmtSMLv          = volume( aPmtSMLvName );
    int            NumVolInPmtSM     = (int)( aPmtSMLv->GetNdaughters() );
    bool           PmtCompPvNotBuilt = ( NumVolInPmtSM == 0 ) ? true : false;

    // First the pmt components
    if ( PmtCompPvNotBuilt ) {
      dd4hep::PlacedVolume aPvRichMapmtAnode =
          BuildARichPmtPhysVolWithPVRet( aPmtSMLvName, ( aRichPmtGeoAux->RichMaPmtStdAnodeLVName() ),
                                         RichPmtGeomTransformLabel::pvRichPMTAnode0000TN );
      std::string aR1PmtAnodeDetName = aRichPmtGeoAux->RichMaPmtAnodeDetName();
      int         aR1PmtAnodeId =
          aRichPmtGeoAux->getRichPmtCompPhysVolCopyNumber( RichPmtGeomTransformLabel::pvRichPMTAnode0000TN );

      aPvRichMapmtAnode.addPhysVolID( aR1PmtAnodeDetName + "Det", aR1PmtAnodeId );

      dd4hep::PlacedVolume aPvRichMapmtQuartz = BuildARichPmtPhysVolWithPVRet(
          aPmtSMLvName, ( aRichPmtGeoAux->RichMaPmtQuartzLVName() ), RichPmtGeomTransformLabel::pvRichPMTQuartzTN );
      dd4hep::PlacedVolume aPvRichMapmtPhCathode =
          BuildARichPmtPhysVolWithPVRet( aPmtSMLvName, ( aRichPmtGeoAux->RichMaPmtPhCathodeLVName() ),
                                         RichPmtGeomTransformLabel::pvRichPMTPhCathodeTN );
      std::string aR1PmtPhCathodeDetName = aRichPmtGeoAux->RichMaPmtPhCathodeDetName();
      int         aR1PmtPhCathodeId =
          aRichPmtGeoAux->getRichPmtCompPhysVolCopyNumber( RichPmtGeomTransformLabel::pvRichPMTPhCathodeTN );

      aPvRichMapmtAnode.addPhysVolID( aR1PmtAnodeDetName + "Det", aR1PmtAnodeId );
      aPvRichMapmtPhCathode.addPhysVolID( aR1PmtPhCathodeDetName + "Det", aR1PmtPhCathodeId );

      dd4hep::PlacedVolume aPvRichMapmtSideEnv =
          BuildARichPmtPhysVolWithPVRet( aPmtSMLvName, ( aRichPmtGeoAux->RichMaPmtSideEnvelopeLVName() ),
                                         RichPmtGeomTransformLabel::pvRichPMTSideEnvelopeTN );
      dd4hep::PlacedVolume aPvRichMapmtBackEnv =
          BuildARichPmtPhysVolWithPVRet( aPmtSMLvName, ( aRichPmtGeoAux->RichMaPmtBackEnvelopeLVName() ),
                                         RichPmtGeomTransformLabel::pvRichPMTBackEnvelopeTN );
      BuildARichPmtPhysVol( aPmtSMLvName, ( aRichPmtGeoAux->RichMaPmtFrontRingLVName() ),
                            RichPmtGeomTransformLabel::pvRichPMTFrontRingTN );
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtAnode )] = aPvRichMapmtAnode;
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtQW )]    = aPvRichMapmtQuartz;

      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtSideEnv )] = aPvRichMapmtSideEnv;
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtBackEnv )] = aPvRichMapmtBackEnv;
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtPhc )]     = aPvRichMapmtPhCathode;

      if ( m_activate_Rich1PmtSurface_build ) {
        for ( int isPair = 0; isPair < aNumSurfPairInPmt; isPair++ ) {

          dd4hep::BorderSurface aR1MapmtPCBorderSurface =
              ( isPair == 0 ) ? dd4hep::BorderSurface( description, detector, ( aR1MapmtPCBorderSN[isPair] ),
                                                       aRMapmtPCSurf, aPvRichMapmtQuartz, aPvRichMapmtPhCathode )
                              : dd4hep::BorderSurface( description, detector, ( aR1MapmtPCBorderSN[isPair] ),
                                                       aRMapmtPCSurf, aPvRichMapmtPhCathode, aPvRichMapmtQuartz );

          aR1MapmtPCBorderSurface.isValid();
          if ( m_RichPmtSurfaceDebugActivate ) {
            printout( m_RichPmtSurfaceDebugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG,
                      "Rich1_Geo : ", "Created Rich1Pmt PC Surface with Name %s using %s",
                      ( ( aR1MapmtPCBorderSN[isPair] ).c_str() ),
                      ( ( aRichSurfaceUtil->RichMapmtPCSurfaceName() ).c_str() ) );

            aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtPCSurf, RichSurfPropType::Refl );
            aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtPCSurf, RichSurfPropType::Effic );
            aRichSurfaceUtil->PrintRichSurfProperty( aRMapmtPCSurf, RichSurfPropType::Trans );
          }
        }
      }

    } else {
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtAnode )] = aPmtSMLv->GetNode(
          ( aRichPmtGeoAux->getRichPmtCompPhysVolName( RichPmtGeomTransformLabel::pvRichPMTAnode0000TN ) ).c_str() );
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtQW )] = aPmtSMLv->GetNode(
          ( aRichPmtGeoAux->getRichPmtCompPhysVolName( RichPmtGeomTransformLabel::pvRichPMTQuartzTN ) ).c_str() );

      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtSideEnv )] = aPmtSMLv->GetNode(
          ( aRichPmtGeoAux->getRichPmtCompPhysVolName( RichPmtGeomTransformLabel::pvRichPMTSideEnvelopeTN ) ).c_str() );
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtBackEnv )] = aPmtSMLv->GetNode(
          ( aRichPmtGeoAux->getRichPmtCompPhysVolName( RichPmtGeomTransformLabel::pvRichPMTBackEnvelopeTN ) ).c_str() );
      aPmtCompPvVect[aRichPmtGeoAux->toInTg( RichStdPmtCompLabel::stdPmtPhc )] = aPmtSMLv->GetNode(
          ( aRichPmtGeoAux->getRichPmtCompPhysVolName( RichPmtGeomTransformLabel::pvRichPMTPhCathodeTN ) ).c_str() );
    }
    return aPmtCompPvVect;
  }

  //=====================================================================================//

  void Rich1Build::build_Rich1MagSh() {
    auto aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();
    auto aR1SMLName   = aRichGeoUtil->Rich1SubMasterLVName();

    build_Rich1PhDetSupFrames();
    for ( int iSide = 0; iSide < ( aRichGeoUtil->Rich1NumSides() ); iSide++ ) {

      std::string            aMagShLvName = aRichGeoUtil->Rich1MagShLVName( iSide );
      RichGeomTransformLabel aRich1MagShTN =
          ( iSide == 0 ) ? RichGeomTransformLabel::pvRich1MagShH0TN : RichGeomTransformLabel::pvRich1MagShH1TN;

      BuildARich1PhysVol( aR1SMLName, aMagShLvName, aRich1MagShTN );
    }
    if ( m_activate_QuartzWindow_build ) { build_Rich1GasQuartzWindows(); }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1MagneticShieldComponents() {
    auto        aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();
    std::string aR1SMLName   = aRichGeoUtil->Rich1SubMasterLVName();
    std::string aR1MLName    = aRichGeoUtil->Rich1MasterLVName();

    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsOuterH0", RichGeomTransformLabel::pvRich1MgsOuterH0TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsOuterH1", RichGeomTransformLabel::pvRich1MgsOuterH1TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsUpstrH0", RichGeomTransformLabel::pvRich1MgsUpstrH0TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsUpstrH1", RichGeomTransformLabel::pvRich1MgsUpstrH1TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsSide", RichGeomTransformLabel::pvRich1MgsSideQ0TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsSide", RichGeomTransformLabel::pvRich1MgsSideQ1TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsSide", RichGeomTransformLabel::pvRich1MgsSideQ2TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsSide", RichGeomTransformLabel::pvRich1MgsSideQ3TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsUpstrCorner", RichGeomTransformLabel::pvRich1MgsUpstrCornerH0TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsUpstrCorner", RichGeomTransformLabel::pvRich1MgsUpstrCornerH1TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsTeeth", RichGeomTransformLabel::pvRich1MgsTeethQ0TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsTeeth", RichGeomTransformLabel::pvRich1MgsTeethQ1TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsTeeth", RichGeomTransformLabel::pvRich1MgsTeethQ2TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsTeeth", RichGeomTransformLabel::pvRich1MgsTeethQ3TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsMidH0", RichGeomTransformLabel::pvRich1MgsMidH0TN );
    BuildARich1PhysVol( aR1SMLName, "lvRich1MgsMidH1", RichGeomTransformLabel::pvRich1MgsMidH1TN );

    BuildARich1PhysVol( aR1MLName, "lvRich1MgsDnstrUTH0", RichGeomTransformLabel::pvRich1MgsDnstrUTH0TN );
    BuildARich1PhysVol( aR1MLName, "lvRich1MgsDnstrUTH1", RichGeomTransformLabel::pvRich1MgsDnstrUTH1TN );
  }

  //=====================================================================================//
  void Rich1Build::build_Rich1ExitWall() {
    auto        aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();
    std::string aR1SMLName   = aRichGeoUtil->Rich1SubMasterLVName();

    if ( m_activate_ExitWall_build ) {
      BuildARich1PhysVol( "lvRich1ExitWallMaster", "lvRich1ExitG10Upstr",
                          RichGeomTransformLabel::pvRich1ExitG10UpstrTN );
      BuildARich1PhysVol( "lvRich1ExitWallMaster", "lvRich1ExitG10Dnstr",
                          RichGeomTransformLabel::pvRich1ExitG10DnstrTN );
      BuildARich1PhysVol( "lvRich1ExitWallMaster", "lvRich1ExitPMI", RichGeomTransformLabel::pvRich1ExitPMITN );

      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitWallMaster", RichGeomTransformLabel::pvRich1ExitWallMasterTN );
    }
    if ( m_activate_ExitWallDiaphram_build ) {
      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitDiaphramCentral",
                          RichGeomTransformLabel::pvRich1ExitDiaphramCentralUpsTN );
      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitDiaphramCentral",
                          RichGeomTransformLabel::pvRich1ExitDiaphramCentralDnsTN );
      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitDiaphramMiddle",
                          RichGeomTransformLabel::pvRich1ExitDiaphramMiddleTN );
      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitDiaphramPeriphery",
                          RichGeomTransformLabel::pvRich1ExitDiaphramPeripheryTN );
      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitDiaphramCentralPlugUps",
                          RichGeomTransformLabel::pvRich1ExitDiaphramCentralPlugUpsTN );
      BuildARich1PhysVol( aR1SMLName, "lvRich1ExitDiaphramCentralPlugDns",
                          RichGeomTransformLabel::pvRich1ExitDiaphramCentralPlugDnsTN );
    }
  }

  //=====================================================================================//

  void Rich1Build::build_Rich1SubMaster() {
    auto        aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();
    std::string aR1SMLName   = aRichGeoUtil->Rich1SubMasterLVName();
    std::string aR1MLName    = aRichGeoUtil->Rich1MasterLVName();

    build_Rich1MagSh();

    if ( m_activate_Rich1Mirror1_build ) { build_Rich1Mirror1(); }
    if ( m_activate_Rich1Mirror2_build ) { build_Rich1Mirror2(); }

    if ( m_activate_MagneticShield_build ) { build_Rich1MagneticShieldComponents(); }

    if ( m_activate_ExitWall_build || m_activate_ExitWallDiaphram_build ) { build_Rich1ExitWall(); }

    int aRich1BeamPipeInSubMId =
        aRichGeoUtil->getPhysVolCopyNumber( RichGeomTransformLabel::pvUX851InRich1SubMasterTN );
    std::string          aRich1BeamPipeInSubMDetName = aRichGeoUtil->Rich1BeamPipeInSubMDetName();
    dd4hep::PlacedVolume aRich1BeamPipeInSubMPhys;
    if ( m_activate_Rich1BeamPipe_build ) {
      // The beampipe segment in Rich1SubMaster
      aRich1BeamPipeInSubMPhys = BuildARich1PhysVolWithPVRet( aR1SMLName, aRichGeoUtil->Rich1BeamPipeSegmentLVName( 0 ),
                                                              RichGeomTransformLabel::pvUX851InRich1SubMasterTN );
      aRich1BeamPipeInSubMPhys.addPhysVolID( aRich1BeamPipeInSubMDetName + "Det", aRich1BeamPipeInSubMId );

      // Now the beampipe sections in Rich1Master
      std::vector<RichGeomTransformLabel> aBPLabelInMaster = {RichGeomTransformLabel::pvUX851InRich1BeforeSubMTN,
                                                              RichGeomTransformLabel::pvUX851InRich1AfterSubMTN};

      for ( int ib = 1; ib < aRichGeoUtil->Rich1NumBeamPipeSeg(); ib++ ) {
        BuildARich1PhysVol( aR1MLName, ( aRichGeoUtil->Rich1BeamPipeSegmentLVName( ib ) ), aBPLabelInMaster[ib - 1] );
      }
    }

    if ( m_activate_Rich1SubMaster_build ) {

      dd4hep::PlacedVolume aRich1SubMasterPhys =
          BuildARich1PhysVolWithPVRet( aR1MLName, aR1SMLName, RichGeomTransformLabel::pvRich1SubMasterTN );

      // Now for DetElem structure
      if ( m_activate_Rich1_DetElem_For_CurrentAppl ) {
        std::string aR1SubMDetName = aRichGeoUtil->Rich1SubMasterDetName();
        int         aR1SubMDetId   = aRichGeoUtil->getPhysVolCopyNumber( RichGeomTransformLabel::pvRich1SubMasterTN );
        aRich1SubMasterPhys.addPhysVolID( aR1SubMDetName + "Det", aR1SubMDetId );
        dd4hep::DetElement aRich1SubMDet = dd4hep::DetElement( detector, aR1SubMDetName, aR1SubMDetId );
        aRich1SubMDet.setPlacement( aRich1SubMasterPhys );

        if ( m_activate_Rich1BeamPipe_build ) {
          dd4hep::DetElement aRich1BeamPipeInSubMDet =
              dd4hep::DetElement( aRich1SubMDet, aRich1BeamPipeInSubMDetName, aRich1BeamPipeInSubMId );
          aRich1BeamPipeInSubMDet.setPlacement( aRich1BeamPipeInSubMPhys );
        }
      }
    }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1Master() {

    build_Rich1SubMaster();

    // test volumes names
    size_t len = volumes.size();
    printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, " Rich1_geo", " Number of Volumes  %d ", ( (int)len ) );

    if ( m_debugLvListActivate ) {
      for ( auto iv = volumes.begin(); iv != volumes.end(); ++iv ) {
        dd4hep::Volume vol     = ( ( *iv ).second.second );
        auto           VolName = vol.name();

        printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, "Rich1_geo", " Volume name %s ", VolName );
        int aNumDaughters = (int)( ( static_cast<TGeoVolume*>( vol ) )->GetNdaughters() );
        int aNumNodes     = (int)( ( static_cast<TGeoVolume*>( vol ) )->CountNodes() );
        printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, "Rich1_geo", "Number of daughters  nodes %d   %d ",
                  aNumDaughters, aNumNodes );

        //(static_cast<TGeoVolume*> (vol))->PrintNodes();
      }
    }
    // end test volumes names
  }
  //=====================================================================================//
  void Rich1Build::build_Rich_OpticalProperties() {
    if ( m_activate_RichMatPropTable_build ) { build_RichMaterial_OpticalProperties(); }

    if ( m_activate_RichMatGeneralPropTable_build ) { build_Rich_General_Properties(); }
  }

  //=====================================================================================//

  void Rich1Build::build_RichSingle_QE_Table( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichMatQE aQEType ) {

    auto        aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
    std::string aTableName  = "DummyNonMatQETableName";

    int aNumTabCol  = aRichMatOpd->NumTabCol(); // column size of property tables
    int aNumTabRows = 0;

    if ( aMatNameW == RichMatNameWOP::RhNonMat ) {
      if ( aPropType == RichMatPropType::pmtQE ) {
        aTableName  = aRichMatOpd->GetQEGeneralTableName( aPropType, aQEType );
        aNumTabRows = aRichMatOpd->GetQENumBinsInGeneralTable( aPropType, aQEType );

        dd4hep::PropertyTable aPropertyTable( description, aTableName, "", aNumTabRows, aNumTabCol );

        RichMatPropData* aRichMatPropData = RichMatPropData::getRichMatPropDataInstance();
        aRichMatPropData->SelectAndFillQETable( aPropertyTable, aQEType );
      }
    }
  }
  //=====================================================================================//

  void Rich1Build::build_Rich_PmtHV( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichMatHV ahvType ) {

    auto        aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
    std::string aTableName  = "DummyNonMatHVTableName";
    int         aNumTabCol  = aRichMatOpd->NumTabCol(); // column size of property tables
    int         aNumTabRows = 0;

    if ( aMatNameW == RichMatNameWOP::RhNonMat ) {
      if ( aPropType == RichMatPropType::pmtHV ) {
        aTableName  = aRichMatOpd->getPmtHVTableName( ahvType );
        aNumTabRows = aRichMatOpd->GetHVNumBinsInGeneralTable( aPropType, ahvType );
      }
    }

    dd4hep::PropertyTable aPropertyTable( description, aTableName, "", aNumTabRows, aNumTabCol );

    RichMatPropData* aRichMatPropData = RichMatPropData::getRichMatPropDataInstance();
    aRichMatPropData->SelectAndFillHVTable( aPropertyTable, ahvType );
  }

  //=============================================================================================================//
  void Rich1Build::build_Rich_QWARCoatRefl( RichMatNameWOP aMatNameW, RichMatPropType aPropType,
                                            RichSurfCoat aARType ) {

    auto        aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
    std::string aTableName  = "DummyNonMatARReflTableName";
    int         aNumTabCol  = aRichMatOpd->NumTabCol(); // column size of property tables
    int         aNumTabRows = 0;

    if ( aMatNameW == RichMatNameWOP::RhNonMat ) {
      if ( aPropType == RichMatPropType::FresAR ) {
        aTableName  = aRichMatOpd->getFresARTableName( aARType );
        aNumTabRows = aRichMatOpd->GetQWARNumBinsInGeneralTable( aPropType, aARType );
      }
    }

    dd4hep::PropertyTable aPropertyTable( description, aTableName, "", aNumTabRows, aNumTabCol );

    RichMatPropData* aRichMatPropData = RichMatPropData::getRichMatPropDataInstance();
    aRichMatPropData->SelectAndFillQWARTable( aPropertyTable, aARType );
  }

  //=============================================================================================================//

  void Rich1Build::build_Rich_ScProp( RichMatNameWOP aMatNameW, RichMatPropType aPropType, RichMatScint aScType ) {

    auto        aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
    std::string aTableName  = "DummyCf4ScintTableName";

    int aNumTabCol  = aRichMatOpd->NumTabCol(); // column size of property tables
    int aNumTabRows = 0;

    if ( ( aMatNameW == RichMatNameWOP::R2cf4 ) || ( aMatNameW == RichMatNameWOP::R2RGas ) ) {
      if ( aPropType == RichMatPropType::scintGeneral ) {

        aTableName  = aRichMatOpd->getCF4ScintTableName( aScType );
        aNumTabRows = aRichMatOpd->GetSCNumBinsInGeneralTable( aPropType, aScType );
      }
    }

    dd4hep::PropertyTable aPropertyTable( description, aTableName, "", aNumTabRows, aNumTabCol );
    RichMatPropData*      aRichMatPropData = RichMatPropData::getRichMatPropDataInstance();
    aRichMatPropData->SelectAndFillScTable( aPropertyTable, aScType );
  }
  //=====================================================================================//
  void Rich1Build::RetriveAndPrintRichPmtQE( RichMatPropType aPropType, RichMatQE aQEType ) {

    auto aRichMatOpd = RichMatOPD::getRichMatOPDInstance();

    std::string aTableName = "ADummyQETableName";

    if ( aPropType == RichMatPropType::pmtQE ) {
      aTableName = aRichMatOpd->GetQEGeneralTableName( aPropType, aQEType );
    }

    TGDMLMatrix* aTableQE = description.manager().GetGDMLMatrix( aTableName.c_str() );

    dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", " Current Table Name Type and QE Type  %s %d %d", aTableName.c_str(),
                      aRichMatOpd->toInTy( aPropType ), aRichMatOpd->toInTy( aQEType ) );

    aRichMatOpd->PrintAGDMLMatrix( aTableQE );
  }
  //=====================================================================================//
  void Rich1Build::RetriveAndPrintRichPmtHV( RichMatPropType aPropType, RichMatHV ahvType ) {

    auto        aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
    std::string aTableName  = "ADummyHVTableName";

    if ( aPropType == RichMatPropType::pmtHV ) { aTableName = aRichMatOpd->getPmtHVTableName( ahvType ); }
    TGDMLMatrix* aTableHV = description.manager().GetGDMLMatrix( aTableName.c_str() );

    dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", " Current Table Name Type and HV Type  %s %d %d", aTableName.c_str(),
                      aRichMatOpd->toInTy( aPropType ), aRichMatOpd->toInTy( ahvType ) );

    aRichMatOpd->PrintAGDMLMatrix( aTableHV );
  }
  //=====================================================================================//
  void Rich1Build::RetriveAndPrintRichGasQWARCoatingRefl( RichMatPropType aPropType, RichSurfCoat aARType ) {

    auto        aRichMatOpd = RichMatOPD::getRichMatOPDInstance();
    std::string aTableName  = "ADummyQWARCoatingReflTableName";

    if ( aPropType == RichMatPropType::FresAR ) { aTableName = aRichMatOpd->getFresARTableName( aARType ); }
    TGDMLMatrix* aTableAR = description.manager().GetGDMLMatrix( aTableName.c_str() );

    dd4hep::printout( dd4hep::DEBUG, "Rich1_geo", " Current Table Name Type and AR Coating Refl Type  %s %d %d",
                      aTableName.c_str(), aRichMatOpd->toInTy( aPropType ), aRichMatOpd->toInTy( aARType ) );

    aRichMatOpd->PrintAGDMLMatrix( aTableAR );
  }

  //=====================================================================================//
  void Rich1Build::build_Rich_General_Properties() {
    // These property tables are not attached to a specific material.

    build_RichSingle_QE_Table( RichMatNameWOP::RhNonMat, RichMatPropType::pmtQE, RichMatQE::qePmtNominal );
    build_Rich_PmtHV( RichMatNameWOP::RhNonMat, RichMatPropType::pmtHV, RichMatHV::hvPmtNominal );
    build_Rich_QWARCoatRefl( RichMatNameWOP::RhNonMat, RichMatPropType::FresAR, RichSurfCoat::arR1GasQW );

    if ( m_activate_RichMatPmtPT_ExtraSet_build ) {
      build_RichSingle_QE_Table( RichMatNameWOP::RhNonMat, RichMatPropType::pmtQE, RichMatQE::qePmtCBAUV );
      build_RichSingle_QE_Table( RichMatNameWOP::RhNonMat, RichMatPropType::pmtQE, RichMatQE::qePmtMeanMeas );
      build_RichSingle_QE_Table( RichMatNameWOP::RhNonMat, RichMatPropType::pmtQE, RichMatQE::qePmtHamamatsuNominal );

      build_Rich_PmtHV( RichMatNameWOP::RhNonMat, RichMatPropType::pmtHV, RichMatHV::hvPmtClassic );
    }

    if ( m_activate_RetrieveAndPrintForDebug_GeneralTable ) {
      RetriveAndPrintRichPmtQE( RichMatPropType::pmtQE, RichMatQE::qePmtNominal );
      RetriveAndPrintRichPmtHV( RichMatPropType::pmtHV, RichMatHV::hvPmtNominal );
      RetriveAndPrintRichGasQWARCoatingRefl( RichMatPropType::FresAR, RichSurfCoat::arR1GasQW );
      if ( m_activate_RichMatPmtPT_ExtraSet_build ) {

        RetriveAndPrintRichPmtQE( RichMatPropType::pmtQE, RichMatQE::qePmtCBAUV );
        RetriveAndPrintRichPmtQE( RichMatPropType::pmtQE, RichMatQE::qePmtMeanMeas );
        RetriveAndPrintRichPmtHV( RichMatPropType::pmtHV, RichMatHV::hvPmtClassic );
      }
    }
  }

  //=====================================================================================//
  void Rich1Build::build_Rich_SingleMaterial_StdOpticalProperty( RichMatNameWOP aMatNameW, RichMatPropType aPropType ) {

    auto aRichMatOpd = RichMatOPD::getRichMatOPDInstance();

    // std::cout<< "material and property index "<<  aRichMatOpd -> toInTy(aMatNameW)  <<"  "
    //     <<  aRichMatOpd  -> toInTy(aPropType) <<std::endl;

    int aNumTabCol = aRichMatOpd->NumTabCol(); // column size of property tables

    int aTableNumRows = aRichMatOpd->GetNumBinsInTable( aMatNameW, aPropType );

    std::string aTableName = aRichMatOpd->GetTableName( aMatNameW, aPropType );

    // Create,register and populate a Table

    dd4hep::PropertyTable aPropertyTable( description, aTableName, "", aTableNumRows, aNumTabCol );
    aRichMatOpd->FillGeneralTable( aPropertyTable, aMatNameW, aPropType );
  }
  //=====================================================================================//
  void Rich1Build::Attach_RichMaterial_OpticalPropertiesTable( RichMatNameWOP aMatNameW, RichMatPropType aPropType ) {

    auto aRichMatOpd = RichMatOPD::getRichMatOPDInstance();

    std::string      cMatName    = aRichMatOpd->RichMatNameWithOptProp( aMatNameW );
    dd4hep::Material aRichCurMat = description.material( cMatName.c_str() );
    std::string      aTableName  = aRichMatOpd->GetTableName( aMatNameW, aPropType );

    // Retrieve the newly created Table,test its existence and attach it to the material

    TGDMLMatrix* aTableT = description.manager().GetGDMLMatrix( aTableName.c_str() );
    aRichMatOpd->AttachTableToMaterial( aTableT, aPropType, aTableName, aRichCurMat );
  }
  //=====================================================================================//

  void Rich1Build::Attach_RichMaterial_ScintPropertiesTable( RichMatNameWOP aMatNameW, RichMatPropType aPropType,
                                                             RichMatScint aScType ) {

    auto aRichMatOpd = RichMatOPD::getRichMatOPDInstance();

    std::string      cMatName    = aRichMatOpd->RichMatNameWithOptProp( aMatNameW );
    dd4hep::Material aRichCurMat = description.material( cMatName.c_str() );
    std::string      aTableName  = aRichMatOpd->getCF4ScintTableName( aScType );

    // Retrieve the newly created Table,test its existence and attach it to the material

    TGDMLMatrix* aTableT = description.manager().GetGDMLMatrix( aTableName.c_str() );
    aRichMatOpd->AttachTableToMaterial( aTableT, aPropType, aTableName, aRichCurMat );
  }
  //=====================================================================================//
  void Rich1Build::build_RichMaterial_OpticalProperties() {

    std::vector<RichMatNameWOP> RhMABL = {
        RichMatNameWOP::Rhair,      RichMatNameWOP::R1GasQw, RichMatNameWOP::PmtVac,  RichMatNameWOP::R1M2Glass,
        RichMatNameWOP::PmtQw,      RichMatNameWOP::PmtPhc,  RichMatNameWOP::R1c4f10, RichMatNameWOP::R2cf4,
        RichMatNameWOP::R1Nitrogen, RichMatNameWOP::Rhco2};
    for ( int ia = 0; ia < (int)RhMABL.size(); ia++ ) {
      build_Rich_SingleMaterial_StdOpticalProperty( RhMABL[ia], RichMatPropType::AbLe );
    }

    std::vector<RichMatNameWOP> RhMRDX = {
        RichMatNameWOP::Rhair,      RichMatNameWOP::R1GasQw, RichMatNameWOP::PmtVac,  RichMatNameWOP::R1M2Glass,
        RichMatNameWOP::PmtQw,      RichMatNameWOP::PmtPhc,  RichMatNameWOP::R1c4f10, RichMatNameWOP::R2cf4,
        RichMatNameWOP::R1Nitrogen, RichMatNameWOP::Rhco2};
    for ( int ir = 0; ir < (int)RhMRDX.size(); ir++ ) {
      build_Rich_SingleMaterial_StdOpticalProperty( RhMRDX[ir], RichMatPropType::Rindex );
    }

    std::vector<RichMatNameWOP> RhMCKR = {RichMatNameWOP::R1GasQw, RichMatNameWOP::PmtQw,      RichMatNameWOP::R1c4f10,
                                          RichMatNameWOP::R2cf4,   RichMatNameWOP::R1Nitrogen, RichMatNameWOP::Rhco2};
    for ( int ic = 0; ic < (int)RhMCKR.size(); ic++ ) {
      build_Rich_SingleMaterial_StdOpticalProperty( RhMCKR[ic], RichMatPropType::CkvRndx );
    }

    std::vector<RichMatScint> RhScT = {RichMatScint::scFast, RichMatScint::scFastTime, RichMatScint::scYield,
                                       RichMatScint::scRes};

    for ( int ik = 0; ik < (int)RhScT.size(); ik++ ) {
      build_Rich_ScProp( RichMatNameWOP::R2cf4, RichMatPropType::scintGeneral, RhScT[ik] );
    }

    std::vector<RichMatNameWOP> RhMABLAth = {
        RichMatNameWOP::Rhair,      RichMatNameWOP::R1GasQw,    RichMatNameWOP::R2GasQw, RichMatNameWOP::PmtVac,
        RichMatNameWOP::R1M2Glass,  RichMatNameWOP::R2MGlass,   RichMatNameWOP::PmtQw,   RichMatNameWOP::PmtPhc,
        RichMatNameWOP::R1c4f10,    RichMatNameWOP::R1RGas,     RichMatNameWOP::R2cf4,   RichMatNameWOP::R2RGas,
        RichMatNameWOP::R2Nitrogen, RichMatNameWOP::R1Nitrogen, RichMatNameWOP::Rhco2,
    };

    std::vector<RichMatNameWOP> RhMRDXAth = {
        RichMatNameWOP::Rhair,      RichMatNameWOP::R1GasQw,    RichMatNameWOP::R2GasQw, RichMatNameWOP::PmtVac,
        RichMatNameWOP::R1M2Glass,  RichMatNameWOP::R2MGlass,   RichMatNameWOP::PmtQw,   RichMatNameWOP::PmtPhc,
        RichMatNameWOP::R1c4f10,    RichMatNameWOP::R1RGas,     RichMatNameWOP::R2cf4,   RichMatNameWOP::R2RGas,
        RichMatNameWOP::R1Nitrogen, RichMatNameWOP::R2Nitrogen, RichMatNameWOP::Rhco2};

    std::vector<RichMatNameWOP> RhMCKRAth = {
        RichMatNameWOP::R1GasQw,    RichMatNameWOP::R2GasQw, RichMatNameWOP::PmtQw,  RichMatNameWOP::R1c4f10,
        RichMatNameWOP::R1RGas,     RichMatNameWOP::R2cf4,   RichMatNameWOP::R2RGas, RichMatNameWOP::R1Nitrogen,
        RichMatNameWOP::R2Nitrogen, RichMatNameWOP::Rhco2};

    std::vector<RichMatPropType> RhPSC = {RichMatPropType::scFast, RichMatPropType::scFastTime,
                                          RichMatPropType::scYield, RichMatPropType::scRes};

    if ( m_activate_RichStd_MatPropTable_Attach ) {

      for ( int ib = 0; ib < (int)RhMABLAth.size(); ib++ ) {
        Attach_RichMaterial_OpticalPropertiesTable( RhMABLAth[ib], RichMatPropType::AbLe );
      }
      for ( int ix = 0; ix < (int)RhMRDXAth.size(); ix++ ) {
        Attach_RichMaterial_OpticalPropertiesTable( RhMRDXAth[ix], RichMatPropType::Rindex );
      }

      if ( m_activate_RichSpecific_MatPropTable_Attach ) {

        for ( int iy = 0; iy < (int)RhMCKRAth.size(); iy++ ) {
          Attach_RichMaterial_OpticalPropertiesTable( RhMCKRAth[iy], RichMatPropType::CkvRndx );
        }

        for ( int isc = 0; isc < (int)RhScT.size(); isc++ ) {
          Attach_RichMaterial_ScintPropertiesTable( RichMatNameWOP::R2cf4, RhPSC[isc], RhScT[isc] );
          Attach_RichMaterial_ScintPropertiesTable( RichMatNameWOP::R2RGas, RhPSC[isc], RhScT[isc] );
        }
      }
    }
    // Retrieve the tables and print the contents for testing

    if ( m_activate_RetrieveAndPrintForDebug_MatTable ) {
      for ( int ija = 0; ija < (int)RhMABLAth.size(); ija++ ) {
        RetrieveRichMatProperty( RhMABLAth[ija], RichMatPropType::AbLe );
      }
      for ( int ijr = 0; ijr < (int)RhMRDXAth.size(); ijr++ ) {
        RetrieveRichMatProperty( RhMRDXAth[ijr], RichMatPropType::Rindex );
      }
      if ( m_activate_RichSpecific_MatPropTable_Attach ) {
        for ( int ijc = 0; ijc < (int)RhMCKRAth.size(); ijc++ ) {
          RetrieveRichMatProperty( RhMCKRAth[ijc], RichMatPropType::CkvRndx );
        }

        for ( int itc = 0; itc < (int)RhPSC.size(); itc++ ) {
          RetrieveRichMatProperty( RichMatNameWOP::R2cf4, RhPSC[itc] );
          RetrieveRichMatProperty( RichMatNameWOP::R2RGas, RhPSC[itc] );
        }
      }
    }
  }

  //=====================================================================================//
  void Rich1Build::RetrieveRichMatProperty( RichMatNameWOP aMatNameW, RichMatPropType aPropType ) {
    // Retrieve the property from the material for test
    auto aRichMatOpd = RichMatOPD::getRichMatOPDInstance();

    std::string aMatName = aRichMatOpd->RichMatNameWithOptProp( aMatNameW );

    dd4hep::Material aRichMat = description.material( aMatName.c_str() );
    aRichMatOpd->PrintRichMatProperty( aRichMat, aPropType );
  }
  //=====================================================================================//

  void Rich1Build::build_Rich1_Main() {

    // sensitive.setType( "Rich" );
    dd4hep::PlacedVolume pv;
    // dd4hep::DetElement   deSubM;

    load( x_det, "include" );

    auto aRichGeoUtil = RichGeoUtil::getRichGeoUtilInstance();
    aRichGeoUtil->setDebugLevel( m_generalutilDebugActivate );

    printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, " Rich1_geo ", " Initialized RichGeoUtil " );

    auto aRichPmtGeoAux = RichPmtGeoAux::getRichPmtGeoAuxInstance();
    aRichPmtGeoAux->setPmtDebugLevel( m_pmtutilDebugActivate );

    // Create Optical properties for Rich Materials
    build_Rich_OpticalProperties();

    auto aRichGeoTransAux = RichGeoTransAux::getRichGeoTransAuxInstance();
    aRichGeoTransAux->setRichGeoTransDebug( m_generalutilDebugActivate );
    size_t aNumR1F = aRichGeoTransAux->loadRichTransforms( x_det, "include", "Rich1" );
    printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, " Rich1_geo ",
              " Uploaded RichTransforms for Rich1 in RichGeoUtil from %zu Rich1 Transform files ", aNumR1F );

    if ( !( aRichPmtGeoAux->IsRichStdPmtCompUploaded() ) ) {
      size_t aNumRPmF = aRichGeoTransAux->loadRichTransforms( x_det, "include", "RichPMT" );
      aRichPmtGeoAux->setRichNumStdPmtCompUploaded( aNumRPmF );
      printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, " Rich1_geo ",
                " Uploaded RichTransforms for Rich StdPmt in RichPmtGeoAux from  %zu  "
                "RichPmt Transform Files ",
                aNumRPmF );

    } else {
      printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, " Rich1_geo ",
                "  Rich StdPmt Transforms are already uploaded  from elsewhere for  %zu  "
                "RichPmt Transform Files ",
                ( aRichPmtGeoAux->RichNumStdPmtCompUploaded() ) );
    }

    // aRichGeoUtil->TestRichGeomParameters();

    // Initiate Rich1 Surface utility
    if ( m_activate_Rich1Surface_build || m_activate_Rich1PmtSurface_build || m_activate_Rich1QWSurface_build ) {
      auto aRichSurfaceUtil = RichSurfaceUtil::getRichSurfaceUtilInstance();
      aRichSurfaceUtil->setRichSurfaceUtilDebug( m_RichSurfaceDebugActivate );
      aRichSurfaceUtil->InitRich1SurfNames();
    }

    buildVolumes( x_det );
    placeDaughters( detector, dd4hep::Volume(), x_det );
    build_Rich1Master();

    if ( m_activateVolumeDebug ) {
      if ( !( m_attachVolumeForDebug.empty() ) ) {
        select_Rich1_Volume = m_attachVolumeForDebug;
        printout( m_debugActivate ? dd4hep::ALWAYS : dd4hep::DEBUG, " Rich1_geo ",
                  " Now selecting the volume %s for display and debug . ", select_Rich1_Volume.c_str() );
      }
    }
    dd4hep::Volume vol = volume( select_Rich1_Volume );
    pv                 = placeDetector( vol );
    if ( x_det.hasAttr( _U( id ) ) ) { pv.addPhysVolID( "system", x_det.id() ); }
  }
} // end namespace

//=======================================================================================//

static dd4hep::Ref_t create_element( dd4hep::Detector& description, xml_h e, dd4hep::SensitiveDetector sens_det ) {
  Rich1Build builder( description, e, sens_det );

  builder.build_Rich1_Main();
  return builder.detector;
}
DECLARE_DETELEMENT( LHCb_Rich1_Geometry, create_element )
