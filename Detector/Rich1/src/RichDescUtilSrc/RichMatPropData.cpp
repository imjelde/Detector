//==========================================================================
//  LHCb Rich Detector geometry utility class  using DD4HEP
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
//
// Author     : Sajan Easo
// Date       : 2020-09-29
//
//==========================================================================//

#include "Detector/Rich1/RichMatPropData.h"
#include <cassert>
#include <memory>
#include <mutex>

using namespace LHCb::Detector;

//==========================================================================//
void RichMatPropData::SelectAndFillAbleTable( dd4hep::PropertyTable& aTable, RichMatAbLe aAbleType ) {
  if ( aAbleType == RichMatAbLe::alGasQw ) { FillATableFromMap( aTable, m_RichMatGasQwEnAble ); }
}

//==========================================================================//
void RichMatPropData::FillGasQwRiTable( dd4hep::PropertyTable& aTable ) {

  // This may be generalized into single value table filling in the future for different tables.

  double aQwRiValue = detail::dd4hep_param<double>( "RichGasQwRindexValue" );

  for ( int i = 0; i < m_RichMatNumGasQWEnDBins; ++i ) {

    aTable->Set( i, 0, m_RichMatGasQWEnVect[i] );
    aTable->Set( i, 1, aQwRiValue );
  }
}
//==========================================================================//
void RichMatPropData::FillGasQwCkvRiTable( dd4hep::PropertyTable& aTable ) {

  double aQwRiValue = detail::dd4hep_param<double>( "RichGasQwRindexValue" );
  int    iW         = 0;
  for ( int i = 0; i < m_RichMatNumGasQWEnDBins; ++i ) {
    double aEnVal = m_RichMatGasQWEnVect[i];
    if ( ( aEnVal >= m_minCkvRndxEn ) && ( aEnVal <= m_maxCkvRndxEn ) ) {
      aTable->Set( iW, 0, aEnVal );
      aTable->Set( iW, 1, aQwRiValue );
      iW++;
    }
  }
}
//==========================================================================//

void RichMatPropData::SelectAndFillQETable( dd4hep::PropertyTable& aTable, RichMatQE aQEType ) {

  if ( aQEType == RichMatQE::qePmtNominal ) {
    FillATableFromMap( aTable, m_RichPmtNominal_QE_Percent );
  } else if ( aQEType == RichMatQE::qePmtCBAUV ) {
    FillATableFromMap( aTable, m_RichPmtCBAUVGlass_QE_Percent );
  } else if ( aQEType == RichMatQE::qePmtMeanMeas ) {
    FillATableFromMap( aTable, m_RichPmtMeanMeas_QE_Percent );
  } else if ( aQEType == RichMatQE::qePmtHamamatsuNominal ) {
    FillATableFromMap( aTable, m_RichPmtNominal_Hamamatsu_QE_Percent );
  }
}
//==========================================================================//
void RichMatPropData::SelectAndFillHVTable( dd4hep::PropertyTable& aTable, RichMatHV ahvType ) {
  if ( ahvType == RichMatHV::hvPmtNominal ) {
    FillATableFromMap( aTable, m_RichPmtHVNominal );
  } else if ( ahvType == RichMatHV::hvPmtClassic ) {
    FillATableFromMap( aTable, m_RichPmtHVClassic );
  }
}
//==========================================================================//
void RichMatPropData::SelectAndFillQWARTable( dd4hep::PropertyTable& aTable, RichSurfCoat aARType ) {
  if ( aARType == RichSurfCoat::arR1GasQW ) { FillATableFromMap( aTable, m_Rich1GasQWARCoatingRefl ); }
}
//==========================================================================//

void RichMatPropData::SelectAndFillScTable( dd4hep::PropertyTable& aTable, RichMatScint aScType ) {

  if ( aScType == RichMatScint::scFast ) {
    FillATableFromMap( aTable, m_RichCF4ScintFast );
  } else if ( aScType == RichMatScint::scFastTime ) {
    FillATableFromMap( aTable, m_RichCF4ScFaTimeConst );
  } else if ( aScType == RichMatScint::scYield ) {
    FillATableFromMap( aTable, m_RichCF4ScintYield );
  } else if ( aScType == RichMatScint::scRes ) {
    FillATableFromMap( aTable, m_RichCF4ScintRes );
  }
}
//==========================================================================//

void RichMatPropData::SelectAndFillRindexTables( dd4hep::PropertyTable& aTable, RichMatRindex aRindexType ) {
  if ( aRindexType == RichMatRindex::riC4F10 ) {
    FillATableFromMap( aTable, m_RichMatC4F10EnRi );
  } else if ( aRindexType == RichMatRindex::riNitrogen ) {
    FillATableFromMap( aTable, m_RichMatNitrogenEnRi );
  } else if ( aRindexType == RichMatRindex::riCF4 ) {
    FillATableFromMap( aTable, m_RichMatCF4EnRi );
  } else if ( aRindexType == RichMatRindex::riCO2 ) {
    FillATableFromMap( aTable, m_RichMatCO2EnRi );
  } else if ( aRindexType == RichMatRindex::riPmtPhCath ) {
    FillATableFromMap( aTable, m_RichMatPmtPhCathRi );
  } else if ( aRindexType == RichMatRindex::riPmtQw ) {
    FillATableFromMap( aTable, m_RichMatPmtQwEnRi );
  }
}
//==========================================================================//

void RichMatPropData::SelectAndFillCkvRndxTables( dd4hep::PropertyTable& aTable, RichMatCkvRndx aCkvRndxType ) {

  if ( aCkvRndxType == RichMatCkvRndx::ciC4F10 ) {
    FillACkvRndxTable( aTable, m_RichMatC4F10EnRi );
  } else if ( aCkvRndxType == RichMatCkvRndx::ciNitrogen ) {
    FillACkvRndxTable( aTable, m_RichMatNitrogenEnRi );
  } else if ( aCkvRndxType == RichMatCkvRndx::ciCF4 ) {
    FillACkvRndxTable( aTable, m_RichMatCF4EnRi );
  } else if ( aCkvRndxType == RichMatCkvRndx::ciCO2 ) {
    FillACkvRndxTable( aTable, m_RichMatCO2EnRi );
  } else if ( aCkvRndxType == RichMatCkvRndx::ciPmtQw ) {
    FillACkvRndxTable( aTable, m_RichMatPmtQwEnRi );
  }
}

//==========================================================================//

void RichMatPropData::FillATableFromMap( dd4hep::PropertyTable& aTable, TabData& aRiMap ) {

  // void RichMatPropData::FillARindexOrALTable(dd4hep::PropertyTable & aTable,
  //                                     std::map<double,double> & aRiMap ){

  int iH = 0;
  for ( auto itH = aRiMap.begin(); itH != aRiMap.end(); ++itH ) {
    double aEnValH = itH->first;
    double aRiValH = itH->second;

    aTable->Set( iH, 0, aEnValH );
    aTable->Set( iH, 1, aRiValH );
    iH++;
  }
}
//==========================================================================//
void RichMatPropData::FillACkvRndxTable( dd4hep::PropertyTable& aTable, TabData& aCkvRiMap ) {

  int iJ = 0;
  for ( auto itR = aCkvRiMap.begin(); itR != aCkvRiMap.end(); ++itR ) {
    double aEnVal = itR->first;
    if ( ( aEnVal >= m_minCkvRndxEn ) && ( aEnVal <= m_maxCkvRndxEn ) ) {
      double aRiVal = itR->second;
      aTable->Set( iJ, 0, aEnVal );
      aTable->Set( iJ, 1, aRiVal );
      iJ++;
    }
  }
}

//==========================================================================//
void RichMatPropData::InitRichMatPropertyValues() {
  m_RichMatGasQWEnVect.clear();

  double ueV = 1.0 * dd4hep::eV;
  // Modification for DD4HEP structures
  //  m_minCkvRndxEn = 1.74 * ueV;
  // m_maxCkvRndxEn = 6.6 * ueV;
  m_minCkvRndxEn = 1.6 * ueV;
  m_maxCkvRndxEn = 6.5 * ueV;

  //  m_RichMatGasQWEnVect = {1.2 * ueV,      1.5 * ueV,       1.75 * ueV,      2.0 * ueV,     2.5 * ueV, 3.0 * ueV,
  //                        3.5 * ueV,      4.0 * ueV,       4.5 * ueV,       5.0 * ueV,     5.5 * ueV, 5.6505682 * ueV,
  //                        6.215625 * ueV, 6.5427632 * ueV, 6.7195946 * ueV, 6.90625 * ueV, 7.0 * ueV, 7.5 * ueV};

  m_RichMatGasQWEnVect = {1.6 * ueV, 1.75 * ueV, 2.0 * ueV, 2.5 * ueV,       3.0 * ueV,      3.5 * ueV, 4.0 * ueV,
                          4.5 * ueV, 5.0 * ueV,  5.5 * ueV, 5.6505682 * ueV, 6.215625 * ueV, 6.5 * ueV};

  // m_RichMatNumGasQWEnDBins = (int)m_RichMatGasQWEnVect.size(); // This table has 18 bins
  m_RichMatNumGasQWEnDBins = (int)m_RichMatGasQWEnVect.size(); // This table has 13 bins

  int iNumGasQwCkvBins = 0;

  for ( int ij = 0; ij < m_RichMatNumGasQWEnDBins; ++ij ) {
    double aE = m_RichMatGasQWEnVect[ij];
    // if ( ( aE > m_minCkvRndxEn ) && ( aE < m_maxCkvRndxEn ) ) { iNumGasQwCkvBins++; }
    if ( ( aE >= m_minCkvRndxEn ) && ( aE <= m_maxCkvRndxEn ) ) { iNumGasQwCkvBins++; }
  }

  m_RichMatNumGasQWCkvEnDBins = iNumGasQwCkvBins;
  // Temp modif for DD4HEP
  // m_RichMatGasEnVect = {1.5 * ueV,  1.75 * ueV, 2.0 * ueV,  2.25 * ueV, 2.5 * ueV,  2.75 * ueV, 3.0 * ueV,
  //                      3.25 * ueV, 3.5 * ueV,  3.75 * ueV, 4.0 * ueV,  4.25 * ueV, 4.5 * ueV,  4.75 * ueV,
  //                      5.0 * ueV,  5.25 * ueV, 5.5 * ueV,  5.75 * ueV, 6.0 * ueV,  6.25 * ueV, 6.5 * ueV,
  //                      6.75 * ueV, 7.0 * ueV,  7.25 * ueV, 7.5 * ueV};

  m_RichMatGasEnVect = {1.6 * ueV,  2.0 * ueV,  2.25 * ueV, 2.5 * ueV,  2.75 * ueV, 3.0 * ueV,  3.25 * ueV,
                        3.5 * ueV,  3.75 * ueV, 4.0 * ueV,  4.25 * ueV, 4.5 * ueV,  4.75 * ueV, 5.0 * ueV,
                        5.25 * ueV, 5.5 * ueV,  5.75 * ueV, 6.0 * ueV,  6.25 * ueV, 6.5 * ueV};

  m_RichMatNumGasEnDBins = (int)m_RichMatGasEnVect.size();

  int iNumGasCkvEnBins = 0;

  for ( int jj = 0; jj < m_RichMatNumGasEnDBins; ++jj ) {
    double aG = m_RichMatGasEnVect[jj];
    //    if ( ( aG > m_minCkvRndxEn ) && ( aG < m_maxCkvRndxEn ) ) { iNumGasCkvEnBins++; }
    if ( ( aG >= m_minCkvRndxEn ) && ( aG <= m_maxCkvRndxEn ) ) { iNumGasCkvEnBins++; }
  }

  m_RichMatNumGasCkvEnDBins = iNumGasCkvEnBins;
}
//==========================================================================//
void RichMatPropData::CreateGasSellRefIndexValues() {

  double ueV = 1.0 * dd4hep::eV;

  double aSellLorGasFac = detail::dd4hep_param<double>( "SellLorGasFac" );
  double aScaleFactor   = 1.0;
  double nMinus1        = 0.0;
  double aSellE1        = 0.0;
  double aSellE2        = 0.0;
  double aSellF1        = 0.0;
  double aSellF2        = 0.0;
  double aGasMolWeight  = 0.0;
  double aGasRhoCur     = 0.0;

  enum GasName { NitrogenGas = 0, C4F10Gas, CF4Gas };
  std::vector<GasName> aGasInd = {GasName::NitrogenGas, GasName::C4F10Gas, GasName::CF4Gas};

  // std::vector<int> aGasInd ={ 0, 1, 2 };
  // Here assume the labels to indicate 0 = "Nitrogen" , 1="C4F10", 2="CF4"

  m_RichMatNitrogenEnRi.clear();
  m_RichMatC4F10EnRi.clear();
  m_RichMatCF4EnRi.clear();

  for ( int ig = 0; ig < (int)aGasInd.size(); ++ig ) {
    if ( aGasInd[ig] == ( GasName::NitrogenGas ) ) { // This for Nitrogen

      aSellE1       = detail::dd4hep_param<double>( "SellN2E1" );
      aSellE2       = detail::dd4hep_param<double>( "SellN2E2" );
      aSellF1       = detail::dd4hep_param<double>( "SellN2F1" );
      aSellF2       = detail::dd4hep_param<double>( "SellN2F2" );
      aGasMolWeight = detail::dd4hep_param<double>( "GasMolWeightN2" );
      aGasRhoCur    = detail::dd4hep_param<double>( "N2GasRhoCur" );

    } else if ( aGasInd[ig] == ( GasName::C4F10Gas ) ) { // This is for C4F10

      aSellE1       = detail::dd4hep_param<double>( "SellC4F10E1" );
      aSellE2       = detail::dd4hep_param<double>( "SellC4F10E2" );
      aSellF1       = detail::dd4hep_param<double>( "SellC4F10F1" );
      aSellF2       = detail::dd4hep_param<double>( "SellC4F10F2" );
      aGasMolWeight = detail::dd4hep_param<double>( "GasMolWeightC4F10" );
      aGasRhoCur    = detail::dd4hep_param<double>( "RhoEffectiveSellC4F10" );

    } else if ( aGasInd[ig] == GasName::CF4Gas ) { // This is for CF4

      aSellE1       = detail::dd4hep_param<double>( "SellCF4E1" );
      aSellE2       = detail::dd4hep_param<double>( "SellCF4E2" );
      aSellF1       = detail::dd4hep_param<double>( "SellCF4F1" );
      aSellF2       = detail::dd4hep_param<double>( "SellCF4F2" );
      aGasMolWeight = detail::dd4hep_param<double>( "GasMolWeightCF4" );
      aGasRhoCur    = detail::dd4hep_param<double>( "CF4GasRhoCur" );
    }

    int maxBins = m_RichMatNumGasEnDBins;

    for ( int jNi = 0; jNi < maxBins; ++jNi ) {

      double ePho   = ( m_RichMatGasEnVect[jNi] );
      double ePhoeV = ePho / ueV;

      double pfe = ( ( aSellF1 / ( ( aSellE1 * aSellE1 ) - ( ePhoeV * ePhoeV ) ) ) +
                     ( aSellF2 / ( ( aSellE2 * aSellE2 ) - ( ePhoeV * ePhoeV ) ) ) );

      double cpfe = aSellLorGasFac * ( aGasRhoCur / aGasMolWeight ) * pfe;
      nMinus1     = aScaleFactor * ( std::sqrt( ( 1.0 + 2 * cpfe ) / ( 1.0 - cpfe ) ) - 1.0 );

      if ( aGasInd[ig] == ( GasName::NitrogenGas ) ) {
        m_RichMatNitrogenEnRi.emplace( ePho, ( nMinus1 + 1.0 ) );
      } else if ( aGasInd[ig] == ( GasName::C4F10Gas ) ) {
        m_RichMatC4F10EnRi.emplace( ePho, ( nMinus1 + 1.0 ) );
      } else if ( aGasInd[ig] == ( GasName::CF4Gas ) ) {
        m_RichMatCF4EnRi.emplace( ePho, ( nMinus1 + 1.0 ) );
      }
    }
  }
}
//==========================================================================//
void RichMatPropData::InitRichCO2RiVal() {
  m_RichMatCO2EnRi.clear();
  // m_RichMatCO2EnCkvRi.clear();
  const double ueV = 1.0 * dd4hep::eV;
  // Temporary modif for DD4HEP structure
  //  m_RichMatCO2EnRi.insert( raPair( 1.5 * ueV, 1.000363563 ) );
  m_RichMatCO2EnRi.emplace( 1.6 * ueV, 1.000364 );
  m_RichMatCO2EnRi.emplace( 2.1 * ueV, 1.000366881 );
  m_RichMatCO2EnRi.emplace( 2.7 * ueV, 1.0003714 );
  m_RichMatCO2EnRi.emplace( 3.3 * ueV, 1.000377208 );
  m_RichMatCO2EnRi.emplace( 3.9 * ueV, 1.000384422 );
  m_RichMatCO2EnRi.emplace( 4.5 * ueV, 1.000393195 );
  m_RichMatCO2EnRi.emplace( 5.1 * ueV, 1.000403724 );
  m_RichMatCO2EnRi.emplace( 5.7 * ueV, 1.000416265 );
  m_RichMatCO2EnRi.emplace( 6.3 * ueV, 1.000431145 );
  m_RichMatCO2EnRi.emplace( 6.5 * ueV, 1.000437028 );

  //  m_RichMatCO2EnRi.insert( raPair( 6.9 * ueV, 1.000448793 ) );
  // m_RichMatCO2EnRi.insert( raPair( 7.5 * ueV, 1.00046901 ) );
  m_RichMatNumCO2EnDBins = (int)m_RichMatCO2EnRi.size();

  int iK = 0;
  for ( auto itC = m_RichMatCO2EnRi.begin(); itC != m_RichMatCO2EnRi.end(); ++itC ) {
    double aEnVal = itC->first;
    if ( ( aEnVal >= m_minCkvRndxEn ) && ( aEnVal <= m_maxCkvRndxEn ) ) { iK++; }
  }
  m_RichMatNumCO2CkvEnDBins = iK;
}
//==========================================================================//
void RichMatPropData::InitPmtQwPhRiVal() {
  m_RichMatPmtQwEnRi.clear();
  m_RichMatPmtPhCathRi.clear();

  double ueV = 1.0 * dd4hep::eV;

  // Now for the PmtQw Rindex
  // Modif for DD4HEP structure

  //  m_RichMatPmtQwEnRi.emplace(  1.55 * ueV, 1.453371 );
  m_RichMatPmtQwEnRi.emplace( 1.6 * ueV, 1.45337 );
  m_RichMatPmtQwEnRi.emplace( 1.78 * ueV, 1.455347 );
  m_RichMatPmtQwEnRi.emplace( 1.80 * ueV, 1.455579 );
  m_RichMatPmtQwEnRi.emplace( 1.83 * ueV, 1.455818 );
  m_RichMatPmtQwEnRi.emplace( 1.86 * ueV, 1.456066 );
  m_RichMatPmtQwEnRi.emplace( 1.89 * ueV, 1.45646 );
  m_RichMatPmtQwEnRi.emplace( 2.11 * ueV, 1.45851 );
  m_RichMatPmtQwEnRi.emplace( 2.12 * ueV, 1.45857 );
  m_RichMatPmtQwEnRi.emplace( 2.28 * ueV, 1.46021 );
  m_RichMatPmtQwEnRi.emplace( 2.56 * ueV, 1.46324 );
  m_RichMatPmtQwEnRi.emplace( 2.85 * ueV, 1.46679 );
  m_RichMatPmtQwEnRi.emplace( 3.12 * ueV, 1.47028 );
  m_RichMatPmtQwEnRi.emplace( 3.44 * ueV, 1.4752 );
  m_RichMatPmtQwEnRi.emplace( 3.59 * ueV, 1.47766 );
  m_RichMatPmtQwEnRi.emplace( 3.65 * ueV, 1.47877 );
  m_RichMatPmtQwEnRi.emplace( 3.79 * ueV, 1.48183 );
  m_RichMatPmtQwEnRi.emplace( 4.17 * ueV, 1.48859 );
  m_RichMatPmtQwEnRi.emplace( 4.52 * ueV, 1.49634 );
  m_RichMatPmtQwEnRi.emplace( 4.83 * ueV, 1.50397 );
  m_RichMatPmtQwEnRi.emplace( 4.90 * ueV, 1.5059 );
  m_RichMatPmtQwEnRi.emplace( 4.97 * ueV, 1.50762 );
  m_RichMatPmtQwEnRi.emplace( 5.34 * ueV, 1.51834 );
  m_RichMatPmtQwEnRi.emplace( 5.49 * ueV, 1.52318 );
  m_RichMatPmtQwEnRi.emplace( 5.80 * ueV, 1.53385 );
  m_RichMatPmtQwEnRi.emplace( 6.03 * ueV, 1.54269 );
  m_RichMatPmtQwEnRi.emplace( 6.14 * ueV, 1.54729 );
  m_RichMatPmtQwEnRi.emplace( 6.42 * ueV, 1.56071 );
  m_RichMatPmtQwEnRi.emplace( 6.5 * ueV, 1.56286 );

  //  m_RichMatPmtQwEnRi.emplace(  6.70 * ueV, 1.57464 );
  // m_RichMatPmtQwEnRi.emplace(  7.5 * ueV, 1.57464 );

  m_RichMatNumPmtQwRiEnDBins = (int)m_RichMatPmtQwEnRi.size();

  int iJ = 0;
  for ( auto itR = m_RichMatPmtQwEnRi.begin(); itR != m_RichMatPmtQwEnRi.end(); ++itR ) {
    double aEnVal = itR->first;
    if ( ( aEnVal >= m_minCkvRndxEn ) && ( aEnVal <= m_maxCkvRndxEn ) ) { iJ++; }
  }
  m_RichMatNumPmtQwCkvRiEnDBins = iJ;

  // Now for PhCath Rindex
  // Modif for DD4HEP structure
  //  m_RichMatPmtPhCathRi.emplace(  1.50 * ueV, ( 2.64 + 0.066 ) );
  // m_RichMatPmtPhCathRi.emplace(  1.55 * ueV, ( 2.65 + 0.11 ) );

  m_RichMatPmtPhCathRi.emplace( 1.6 * ueV, ( 2.65 + 0.11 ) );
  m_RichMatPmtPhCathRi.emplace( 1.66 * ueV, ( 2.67 + 0.16 ) );
  m_RichMatPmtPhCathRi.emplace( 1.78 * ueV, ( 2.68 + 0.188 ) );
  m_RichMatPmtPhCathRi.emplace( 1.94 * ueV, ( 2.72 + 0.22 ) );
  m_RichMatPmtPhCathRi.emplace( 2.07 * ueV, ( 2.76 + 0.264 ) );
  m_RichMatPmtPhCathRi.emplace( 2.22 * ueV, ( 2.84 + 0.337 ) );
  m_RichMatPmtPhCathRi.emplace( 2.39 * ueV, ( 2.91 + 0.411 ) );
  m_RichMatPmtPhCathRi.emplace( 2.59 * ueV, ( 3.1 + 0.608 ) );
  m_RichMatPmtPhCathRi.emplace( 2.83 * ueV, ( 3.58 + 1.18 ) );
  m_RichMatPmtPhCathRi.emplace( 3.11 * ueV, ( 3.59 + 1.09 ) );
  m_RichMatPmtPhCathRi.emplace( 3.45 * ueV, ( 3.33 + 0.86 ) );
  m_RichMatPmtPhCathRi.emplace( 3.88 * ueV, ( 3.33 + 0.86 ) );
  m_RichMatPmtPhCathRi.emplace( 4.14 * ueV, ( 3.33 + 0.86 ) );
  m_RichMatPmtPhCathRi.emplace( 4.60 * ueV, ( 3.33 + 0.86 ) );
  m_RichMatPmtPhCathRi.emplace( 5.18 * ueV, ( 3.33 + 0.86 ) );
  m_RichMatPmtPhCathRi.emplace( 6.22 * ueV, ( 3.33 + 0.86 ) );
  m_RichMatPmtPhCathRi.emplace( 6.5 * ueV, ( 3.33 + 0.86 ) );
  //  m_RichMatPmtPhCathRi.emplace(  7.5 * ueV, ( 3.33 + 0.86 ) );

  m_RichMatNumPmtPhCathRiEnDBins = (int)m_RichMatPmtPhCathRi.size();
}
//==========================================================================//

void RichMatPropData::InitPmtQEValues() {

  m_RichPmtNominal_QE_Percent.clear();
  m_RichPmtNominal_Hamamatsu_QE_Percent.clear();
  m_RichPmtCBAUVGlass_QE_Percent.clear();
  m_RichPmtMeanMeas_QE_Percent.clear();

  double ueV = 1.0 * dd4hep::eV;

  // Nominal QE
  // Nominal QE changed from Hamamatsu sheets to measured values below.

  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.61445 * ueV, 0.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.63569 * ueV, 0.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.6575 * ueV, 0.0007 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.6799 * ueV, 0.0014 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.70291 * ueV, 0.0028 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.72656 * ueV, 0.0056 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.75088 * ueV, 0.01 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.77589 * ueV, 0.0181 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.80163 * ueV, 0.03 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.82812 * ueV, 0.06 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.85541 * ueV, 0.1 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.88352 * ueV, 0.18 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.9125 * ueV, 0.3 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.94238 * ueV, 0.49 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 1.97321 * ueV, 0.77 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.00504 * ueV, 1.21 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.03791 * ueV, 1.71 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.07187 * ueV, 2.29 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.10699 * ueV, 3.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.14332 * ueV, 3.7 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.18092 * ueV, 4.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.21987 * ueV, 5.2 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.26023 * ueV, 6.1 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.30208 * ueV, 7.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.34552 * ueV, 8.3 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.39062 * ueV, 10.8 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.4375 * ueV, 13.6 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.48625 * ueV, 17.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.53699 * ueV, 19.6 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.58984 * ueV, 21.7 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.64495 * ueV, 23.8 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.70245 * ueV, 25.9 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.7625 * ueV, 28.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.82528 * ueV, 30.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.89099 * ueV, 31.2 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 2.95982 * ueV, 32.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.03201 * ueV, 32.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.10781 * ueV, 33.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.1875 * ueV, 33.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.27138 * ueV, 34.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.3598 * ueV, 34.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.45312 * ueV, 35.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.55179 * ueV, 35.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.65625 * ueV, 35.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.76705 * ueV, 34.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 3.88477 * ueV, 33.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.01008 * ueV, 32.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.14375 * ueV, 31.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.28664 * ueV, 27.2 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.43973 * ueV, 23.6 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.60417 * ueV, 20.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.78125 * ueV, 16.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 4.9725 * ueV, 13.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 5.17969 * ueV, 10.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 5.40489 * ueV, 1.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 5.65057 * ueV, 1.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 5.91964 * ueV, 0.5 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 6.21563 * ueV, 0.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 6.54276 * ueV, 0.0 );
  m_RichPmtNominal_Hamamatsu_QE_Percent.emplace( 6.90625 * ueV, 0.0 );

  m_RichPmtNumNominalHamamatsuQEBins = (int)m_RichPmtNominal_Hamamatsu_QE_Percent.size();

  // CBAUV version

  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.61445 * ueV, 0.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.63569 * ueV, 0.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.6575 * ueV, 0.0007 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.6799 * ueV, 0.0014 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.70291 * ueV, 0.0028 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.72656 * ueV, 0.0056 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.75088 * ueV, 0.01 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.77589 * ueV, 0.0181 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.80163 * ueV, 0.03 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.82812 * ueV, 0.06 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.85541 * ueV, 0.1 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.88352 * ueV, 0.18 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.9125 * ueV, 0.3 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.94238 * ueV, 0.49 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 1.97321 * ueV, 0.77 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.00504 * ueV, 1.21 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.03791 * ueV, 1.71 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.07187 * ueV, 2.29 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.10699 * ueV, 3.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.14332 * ueV, 3.7 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.18092 * ueV, 4.5 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.21987 * ueV, 5.2 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.26023 * ueV, 6.1 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.30208 * ueV, 7.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.34552 * ueV, 8.3 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.39062 * ueV, 10.3 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.4375 * ueV, 13.1 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.48625 * ueV, 15.2 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.53699 * ueV, 16.3 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.58984 * ueV, 17.2 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.64495 * ueV, 18.4 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.70245 * ueV, 19.7 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.7625 * ueV, 20.9 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.82528 * ueV, 21.7 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.89099 * ueV, 22.4 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 2.95982 * ueV, 23.3 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.03201 * ueV, 23.6 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.10781 * ueV, 24.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.1875 * ueV, 24.5 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.27138 * ueV, 24.5 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.3598 * ueV, 24.8 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.45312 * ueV, 25.1 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.55179 * ueV, 25.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.65625 * ueV, 25.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.76705 * ueV, 24.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 3.88477 * ueV, 22.9 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.01008 * ueV, 21.8 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.14375 * ueV, 20.2 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.28664 * ueV, 18.5 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.43973 * ueV, 16.7 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.60417 * ueV, 14.8 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.78125 * ueV, 12.7 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 4.9725 * ueV, 10.5 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 5.17969 * ueV, 9.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 5.40489 * ueV, 7.8 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 5.65057 * ueV, 6.1 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 5.91964 * ueV, 5.2 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 6.21563 * ueV, 3.9 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 6.54276 * ueV, 2.7 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 6.90625 * ueV, 0.0 );
  m_RichPmtCBAUVGlass_QE_Percent.emplace( 7.0 * ueV, 0.0 );

  m_RichPmtNumCBAUVQEBins = (int)m_RichPmtCBAUVGlass_QE_Percent.size();

  m_RichPmtMeanMeas_QE_Percent.emplace( 1.61445 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.65750 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.70291 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.72656 * ueV, 0.01 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.77589 * ueV, 0.02 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.82813 * ueV, 0.16 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.88352 * ueV, 0.50 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 1.94238 * ueV, 1.23 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.00504 * ueV, 2.61 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.07188 * ueV, 3.99 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.14332 * ueV, 5.30 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.21987 * ueV, 6.94 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.30208 * ueV, 9.14 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.39063 * ueV, 12.19 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.48625 * ueV, 18.20 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.58984 * ueV, 22.59 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.70245 * ueV, 25.65 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.82528 * ueV, 29.18 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 2.95982 * ueV, 32.93 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 3.10781 * ueV, 34.36 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 3.27138 * ueV, 36.11 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 3.45313 * ueV, 38.29 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 3.65625 * ueV, 38.40 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 3.88477 * ueV, 37.53 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 4.14375 * ueV, 32.94 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 4.43973 * ueV, 26.75 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 4.78125 * ueV, 25.61 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 5.17969 * ueV, 22.37 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 5.65057 * ueV, 16.87 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 6.21563 * ueV, 9.63 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 6.2300 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 6.2500 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 6.5000 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 6.5428 * ueV, 0.00 );
  m_RichPmtMeanMeas_QE_Percent.emplace( 6.9063 * ueV, 0.00 );

  m_RichPmtNumMeanMeasQEBins = (int)m_RichPmtMeanMeas_QE_Percent.size();

  m_RichPmtNominal_QE_Percent = m_RichPmtMeanMeas_QE_Percent;
  m_RichPmtNumNominalQEBins   = (int)m_RichPmtNominal_QE_Percent.size();
}
//==========================================================================//
void RichMatPropData::InitPmtHVValues() {
  m_RichPmtHVNominal.clear();
  m_RichPmtHVClassic.clear();
  typedef std::pair<double, double> hvPair;
  double                            ueV = 1.0 * dd4hep::eV;

  m_RichPmtHVClassic.emplace( hvPair( 1.0, 20000 * ueV ) );
  m_RichPmtNumHVClassicBins = (int)m_RichPmtHVClassic.size();

  m_RichPmtHVNominal.emplace( hvPair( 1.0, 1000 * ueV ) );
  m_RichPmtNumHVNominalBins = (int)m_RichPmtHVNominal.size();
}
//==========================================================================//
void RichMatPropData::InitGasQWARCoatReflValues() {

  m_Rich1GasQWARCoatingRefl.clear();

  const double ueV = 1.0 * dd4hep::eV;

  m_Rich1GasQWARCoatingRefl.emplace( 0.9000 * ueV, 0.03046 );
  m_Rich1GasQWARCoatingRefl.emplace( 1.0000 * ueV, 0.03046 );
  m_Rich1GasQWARCoatingRefl.emplace( 1.2431 * ueV, 0.03046 );
  m_Rich1GasQWARCoatingRefl.emplace( 1.5000 * ueV, 0.02866 );
  m_Rich1GasQWARCoatingRefl.emplace( 1.5539 * ueV, 0.02835 );
  m_Rich1GasQWARCoatingRefl.emplace( 2.0000 * ueV, 0.02544 );
  m_Rich1GasQWARCoatingRefl.emplace( 2.5000 * ueV, 0.02247 );
  m_Rich1GasQWARCoatingRefl.emplace( 3.0000 * ueV, 0.01996 );
  m_Rich1GasQWARCoatingRefl.emplace( 3.5000 * ueV, 0.01830 );
  m_Rich1GasQWARCoatingRefl.emplace( 4.0000 * ueV, 0.01821 );
  m_Rich1GasQWARCoatingRefl.emplace( 4.5000 * ueV, 0.02020 );
  m_Rich1GasQWARCoatingRefl.emplace( 5.0000 * ueV, 0.02403 );
  m_Rich1GasQWARCoatingRefl.emplace( 5.5000 * ueV, 0.03131 );
  m_Rich1GasQWARCoatingRefl.emplace( 5.6506 * ueV, 0.03381 );
  m_Rich1GasQWARCoatingRefl.emplace( 6.2156 * ueV, 0.03712 );
  m_Rich1GasQWARCoatingRefl.emplace( 6.5428 * ueV, 0.03793 );
  m_Rich1GasQWARCoatingRefl.emplace( 6.7196 * ueV, 0.01984 );
  m_Rich1GasQWARCoatingRefl.emplace( 6.9063 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 7.0000 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 7.5000 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 8.0000 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 8.5000 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 9.0000 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 9.5000 * ueV, 0.00000 );
  m_Rich1GasQWARCoatingRefl.emplace( 10.0000 * ueV, 0.00000 );

  m_Rich1GasQWARReflNumBins = (int)m_Rich1GasQWARCoatingRefl.size();
}
//==========================================================================//
void RichMatPropData::InitGasQWAbleVal() {

  const double umm = 1.0 * dd4hep::mm;

  const auto aLowEnAbLeValue = detail::dd4hep_param<double>( "RichGasQwAbsLengthLowEnValue" );
  // double aHighEnAbLeValue = detail::dd4hep_param<double>( "RichGasQwAbsLengthHighEnValue" );

  m_RichMatGasQwEnAble.clear();

  // Modif for DD4HEP structure
  //  std::vector<double>               aSpAble = {1.0e18 * umm,  1.0e18 * umm,  494.983 * umm,    194.957 * umm,
  //                               94.9122 * umm, 44.8142 * umm, aHighEnAbLeValue, aHighEnAbLeValue};

  std::vector<double> aSpAble = {1.0e18 * umm, 1.0e18 * umm, 494.983 * umm, 194.957 * umm};

  //  int                               iLowLim = 10;
  int iLowLim = 9;
  for ( int i = 0; i < iLowLim; i++ ) { m_RichMatGasQwEnAble.emplace( m_RichMatGasQWEnVect[i], aLowEnAbLeValue ); }
  for ( int j = 0; j < (int)aSpAble.size(); j++ ) {
    const auto k = j + iLowLim;
    m_RichMatGasQwEnAble.emplace( m_RichMatGasQWEnVect[k], aSpAble[j] );
  }

  m_RichMatNumGasQWAbleDBins = (int)m_RichMatGasQwEnAble.size();
}

//==========================================================================//
void RichMatPropData::InitCF4ScintValues() {
  m_RichCF4ScintFast.clear();
  m_RichCF4ScFaTimeConst.clear();
  m_RichCF4ScintYield.clear();
  m_RichCF4ScintRes.clear();

  const double ueV = 1.0 * dd4hep::eV;
  // Modif for DD4HEP structure
  m_RichCF4ScintFast.emplace( 1.6 * ueV, 0.0005 );
  m_RichCF4ScintFast.emplace( 1.75 * ueV, 0.001 );
  m_RichCF4ScintFast.emplace( 2.00 * ueV, 0.18 );
  m_RichCF4ScintFast.emplace( 2.25 * ueV, 0.05 );
  m_RichCF4ScintFast.emplace( 2.50 * ueV, 0.01 );
  m_RichCF4ScintFast.emplace( 2.75 * ueV, 0.05 );
  m_RichCF4ScintFast.emplace( 3.00 * ueV, 0.10 );
  m_RichCF4ScintFast.emplace( 3.25 * ueV, 0.12 );
  m_RichCF4ScintFast.emplace( 3.50 * ueV, 0.15 );
  m_RichCF4ScintFast.emplace( 3.75 * ueV, 0.25 );
  m_RichCF4ScintFast.emplace( 4.00 * ueV, 0.48 );
  m_RichCF4ScintFast.emplace( 4.25 * ueV, 0.65 );
  m_RichCF4ScintFast.emplace( 4.50 * ueV, 0.35 );
  m_RichCF4ScintFast.emplace( 4.75 * ueV, 0.28 );
  m_RichCF4ScintFast.emplace( 5.00 * ueV, 0.22 );
  m_RichCF4ScintFast.emplace( 5.25 * ueV, 0.38 );
  m_RichCF4ScintFast.emplace( 5.50 * ueV, 0.37 );
  m_RichCF4ScintFast.emplace( 5.75 * ueV, 0.10 );
  m_RichCF4ScintFast.emplace( 6.00 * ueV, 0.01 );
  m_RichCF4ScintFast.emplace( 6.25 * ueV, 0.0004 );
  m_RichCF4ScintFast.emplace( 6.50 * ueV, 0.0001 );
  //  m_RichCF4ScintFast.emplace(  6.75 * ueV, 0.00001 );
  // m_RichCF4ScintFast.emplace(  7.00 * ueV, 0.0 );

  m_RichCF4NumSCFastBins = (int)m_RichCF4ScintFast.size();

  double uNanoS = 1.0 * dd4hep::ns;

  double aCF4ScintillationFastTimeConstantInNanoSec =
      detail::dd4hep_param<double>( "CF4ScintillationFastTimeConstantInNanoSec" );
  //  m_RichCF4ScFaTimeConst.emplace(  1.75 * ueV, aCF4ScintillationFastTimeConstantInNanoSec * uNanoS );
  // m_RichCF4ScFaTimeConst.emplace(  7.00 * ueV, aCF4ScintillationFastTimeConstantInNanoSec * uNanoS );
  m_RichCF4ScFaTimeConst.emplace( 1.6 * ueV, aCF4ScintillationFastTimeConstantInNanoSec * uNanoS );
  m_RichCF4ScFaTimeConst.emplace( 6.5 * ueV, aCF4ScintillationFastTimeConstantInNanoSec * uNanoS );
  m_RichCF4NumScFaTimeBins = (int)m_RichCF4ScFaTimeConst.size();

  double aCF4ScintillationYieldPerMeV = detail::dd4hep_param<double>( "CF4ScintillationYieldPerMeV" );
  //  m_RichCF4ScintYield.emplace(  1.75 * ueV, aCF4ScintillationYieldPerMeV );
  // m_RichCF4ScintYield.emplace(  7.00 * ueV, aCF4ScintillationYieldPerMeV );
  m_RichCF4ScintYield.emplace( 1.6 * ueV, aCF4ScintillationYieldPerMeV );
  m_RichCF4ScintYield.emplace( 6.5 * ueV, aCF4ScintillationYieldPerMeV );
  m_RichCF4NumScYieldBins = (int)m_RichCF4ScintYield.size();

  double aCF4ScintillationResolutionScale = detail::dd4hep_param<double>( "CF4ScintillationResolutionScale" );
  //  m_RichCF4ScintRes.emplace(  1.75 * ueV, aCF4ScintillationResolutionScale );
  // m_RichCF4ScintRes.emplace(  7.00 * ueV, aCF4ScintillationResolutionScale );
  m_RichCF4ScintRes.emplace( 1.6 * ueV, aCF4ScintillationResolutionScale );
  m_RichCF4ScintRes.emplace( 6.5 * ueV, aCF4ScintillationResolutionScale );
  m_RichCF4NumScResBins = (int)m_RichCF4ScintRes.size();
}
//==========================================================================//

RichMatPropData* RichMatPropData::getRichMatPropDataInstance() {
  static std::once_flag                   alloc_instance_once;
  static std::unique_ptr<RichMatPropData> RichMatPropDataInstance;
  std::call_once( alloc_instance_once, []() { RichMatPropDataInstance = std::make_unique<RichMatPropData>(); } );
  assert( RichMatPropDataInstance.get() );
  return RichMatPropDataInstance.get();
}
