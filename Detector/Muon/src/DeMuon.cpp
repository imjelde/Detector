/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/Muon/DeMuon.h"
#include "Core/yaml_converters.h"

#include "DD4hep/Printout.h"

LHCb::Detector::detail::DeMuonObject::DeMuonObject( dd4hep::DetElement const&             de,
                                                    dd4hep::cond::ConditionUpdateContext& ctxt )
    // 11009 == CLID_DEMuonDetector
    : DeIOVObject( de, ctxt, 11009 ) {}
