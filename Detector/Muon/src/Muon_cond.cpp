//==========================================================================
/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/ConditionHelper.h"
#include "Core/DeConditionCall.h"
#include "Core/DeIOV.h"
#include "Detector/Muon/DeMuon.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Detector.h"

namespace LHCb::Detector {

  /// Condition derivation call to build the top level VP DetElement condition information
  struct DeMuonConditionCall : DeConditionCall {
    using DeConditionCall::DeConditionCall;
    virtual dd4hep::Condition operator()( const dd4hep::ConditionKey& /* key */,
                                          dd4hep::cond::ConditionUpdateContext& ctxt ) override final {
      return DeIOV( new detail::DeMuonObject( dd4hep::Detector::getInstance().detector( "Muon" ), ctxt ) );
    }
  };
} // namespace LHCb::Detector

static long create_conditions_recipes( dd4hep::Detector& description, xml_h e ) {

  // Use the helper to load the XML, setup the callback according
  LHCb::Detector::ConditionConfigHelper<LHCb::Detector::DeMuonConditionCall> config_helper{description, "Muon", e};
  config_helper.configure();

  return 1;
}
DECLARE_XML_DOC_READER( LHCb_Muon_cond, create_conditions_recipes )
