/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <exception>
#include <vector>

namespace LHCb::Detector::Muon {

  struct ReadoutCond {

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    ReadoutCond() { throw NotImplemented(); }

    double              efficiency( const int& ) const { throw NotImplemented(); }
    double              syncDrift( const int& ) const { throw NotImplemented(); }
    double              chamberNoise( const int& ) const { throw NotImplemented(); }
    void                setChamberNoise( const double&, const int& ) { throw NotImplemented(); }
    double              electronicsNoise( const int& ) const { throw NotImplemented(); }
    double              meanDeadTime( const int& ) const { throw NotImplemented(); }
    double              rmsDeadTime( const int& ) const { throw NotImplemented(); }
    int                 singleGapClusterX( const double&, const double&, const int& i ) { throw NotImplemented(); }
    int                 singleGapClusterY( const double&, const double&, const int& i ) { throw NotImplemented(); }
    std::vector<double> timeJitter( double&, double&, const int& ) { throw NotImplemented(); }
  };

} // namespace LHCb::Detector::Muon
