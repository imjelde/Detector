/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace LHCb::Detector::Muon {

  struct FrontEndID {

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    FrontEndID() { throw NotImplemented(); }
    FrontEndID( unsigned int ) { throw NotImplemented(); }

    unsigned int getReadout() const { throw NotImplemented(); }
    unsigned int getLayer() const { throw NotImplemented(); }
    unsigned int getFEIDX() const { throw NotImplemented(); }
    unsigned int getFEIDY() const { throw NotImplemented(); }
  };

} // namespace LHCb::Detector::Muon
