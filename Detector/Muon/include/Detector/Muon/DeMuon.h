/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/DAQHelper.h"
#include "Detector/Muon/DeMuonChamber.h"
#include "Detector/Muon/FrontEndID.h"

#include "Core/DeIOV.h"

#include <vector>

namespace LHCb::Detector {

  namespace detail {

    struct DeMuonObject : DeIOVObject {
      DeMuonObject( dd4hep::DetElement const&, dd4hep::cond::ConditionUpdateContext& );
    };

  } // namespace detail

  template <typename ObjectType>
  struct DeMuonElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    int regNum( const double, const double, int ) const { throw NotImplemented(); }

    // Exception class used in pos2STChamber
    struct ChamberNotFound {};
    DeMuonChamber& pos2StChamber( const double, const double, int ) const { throw NotImplemented(); }

    Muon::TileID Chamber2Tile( int, int, int ) const { throw NotImplemented(); }

    int sensitiveVolumeID( const ROOT::Math::XYZPoint& ) const { throw NotImplemented(); }

    unsigned int gapID( int ) const { throw NotImplemented(); }
    unsigned int chamberID( int ) const { throw NotImplemented(); }
    unsigned int regionID( int ) const { throw NotImplemented(); }
    unsigned int stationID( int ) const { throw NotImplemented(); }
    unsigned int quadrantID( int ) const { throw NotImplemented(); }

    bool isM1defined() const { throw NotImplemented(); }

    // Returns the list of physical channels for a given chamber
    std::vector<std::pair<Muon::FrontEndID, std::array<float, 4>>>
    listOfPhysChannels( ROOT::Math::XYZPoint, ROOT::Math::XYZPoint, int, int ) {
      throw NotImplemented();
    }
    std::optional<ROOT::Math::XYZPoint> getPCCenter( Muon::FrontEndID, int, int, int ) { throw NotImplemented(); }
    // Returns the station index starting from the z position
    int getStation( const double ) const { throw NotImplemented(); }

    DeMuonChamber getChamber( const int, const int, const int ) const { throw NotImplemented(); }

    // Fills various geometry related info
    void fillGeoInfo() { throw NotImplemented(); }
    void fillGeoArray() { throw NotImplemented(); }

    class TilePosition {
    public:
      TilePosition( double x, double y, double z, double dX, double dY, double dZ )
          : m_position( x, y, z ), m_dX( dX ), m_dY( dY ), m_dZ( dZ ) {}
      auto position() const { return m_position; }
      auto x() const { return m_position.x(); }
      auto y() const { return m_position.y(); }
      auto z() const { return m_position.z(); }
      auto dX() const { return m_dX; }
      auto dY() const { return m_dY; }
      auto dZ() const { return m_dZ; }

    private:
      ROOT::Math::XYZPoint m_position;
      double               m_dX, m_dY, m_dZ;
    };
    std::optional<TilePosition> position( Muon::TileID ) const { throw NotImplemented(); }

    void CountDetEls() { throw NotImplemented(); }

    int         stations() const { throw NotImplemented(); }
    int         regions() const { throw NotImplemented(); }
    int         regions( int ) const { throw NotImplemented(); }
    std::string getStationName( int ) { throw NotImplemented(); }
    double      getStationZ( const int ) const { throw NotImplemented(); }

    int          gapsInRegion( const int, const int ) const { throw NotImplemented(); }
    int          gapsPerFE( const int, const int ) const { throw NotImplemented(); }
    int          readoutInRegion( const int, const int ) const { throw NotImplemented(); }
    int          mapInRegion( const int, const int ) const { throw NotImplemented(); }
    double       areaChamber( const int, const int ) const { throw NotImplemented(); }
    unsigned int getPhChannelNX( const int, const int, const int ) const { throw NotImplemented(); }
    unsigned int getPhChannelNY( const int, const int, const int ) const { throw NotImplemented(); }
    float        getPadSizeX( const int, const int ) const { throw NotImplemented(); }

    float        getPadSizeY( const int, const int ) const { throw NotImplemented(); }
    unsigned int getReadoutType( const int, const int, const int ) const { throw NotImplemented(); }
    unsigned int chamberInRegion( const int, const int ) const { throw NotImplemented(); }
    unsigned int getLogMapInRegion( const int, const int ) const { throw NotImplemented(); }
    unsigned int getLogMapRType( const int, const int, const int ) const { throw NotImplemented(); }
    unsigned int getLogMapMergex( const int, const int, const int ) const { throw NotImplemented(); }
    unsigned int getLogMapMergey( const int, const int, const int ) const { throw NotImplemented(); }
    unsigned int getLayoutX( const int, const int, const int ) const { throw NotImplemented(); }
    unsigned int getLayoutY( const int, const int, const int ) const { throw NotImplemented(); }
    float        getSensAreaX( const int, const int ) const { throw NotImplemented(); }
    float        getSensAreaY( const int, const int ) const { throw NotImplemented(); }
    float        getSensAreaZ( const int, const int ) const { throw NotImplemented(); }

    double getInnerX( const int ) const { throw NotImplemented(); }
    double getInnerY( const int ) const { throw NotImplemented(); }
    double getOuterX( const int ) const { throw NotImplemented(); }
    double getOuterY( const int ) const { throw NotImplemented(); }

    unsigned int getCardiacORX( const int, const unsigned int ) const { throw NotImplemented(); }
    unsigned int getCardiacORY( const int, const unsigned int ) const { throw NotImplemented(); }

    // Return the number of the first chamber from the Muon::TileID
    int Tile2FirstChamberNumber( const Muon::TileID ) const { throw NotImplemented(); }

    // Returns the chamber's region number from a Hit
    int Hit2ChamberRegionNumber( const ROOT::Math::XYZPoint ) { throw NotImplemented(); }

    Muon::DAQHelper*       getDAQInfo() { throw NotImplemented(); }
    const Muon::DAQHelper* getDAQInfo() const { throw NotImplemented(); }
  };
  using DeMuon = DeMuonElement<detail::DeMuonObject>;

} // End namespace LHCb::Detector
