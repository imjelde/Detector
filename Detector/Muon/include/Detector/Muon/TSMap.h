/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"

#include <vector>

namespace LHCb::Detector::Muon {

  namespace detail {

    struct TSMapObject : LHCb::Detector::detail::DeIOVObject {
      TSMapObject( dd4hep::DetElement const& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // namespace detail

  /**
   *  @author Alessia Satta
   *  @date   2004-01-05
   */
  template <typename ObjectType>
  struct TSMapElement : DeIOVElement<ObjectType> {

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    using DeIOVElement<ObjectType>::DeIOVElement;

    long numberOfLayout() { throw NotImplemented(); }
    long gridXLayout( int ) { throw NotImplemented(); }
    long gridYLayout( int ) { throw NotImplemented(); }
    long numberOfOutputSignal() { throw NotImplemented(); }
    long layoutOutputChannel( int ) { throw NotImplemented(); }
    long gridXOutputChannel( int ) { throw NotImplemented(); }
    long gridYOutputChannel( int ) { throw NotImplemented(); }
    long synchChSize() { throw NotImplemented(); }
    bool synchChUsed( int ) { throw NotImplemented(); }
    long numberOfPad() { throw NotImplemented(); }
  };
  using TSMap = TSMapElement<detail::TSMapObject>;

} // End namespace LHCb::Detector::Muon
