
/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"

#include "Math/Point3D.h"

namespace LHCb::Detector {

  namespace detail {

    struct DeMuonChamberObject : DeIOVObject {
      DeMuonChamberObject( dd4hep::DetElement const& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // namespace detail

  template <typename ObjectType>
  struct DeMuonChamberElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    int chamberNumber() const { throw NotImplemented(); }
    int regionNumber() const { throw NotImplemented(); }
    int stationNumber() const { throw NotImplemented(); }

    bool checkHitAndGapInChamber( float, float ) const { throw NotImplemented(); }
    bool calculateHitPosInGap( int, float, float, float, float, float, ROOT::Math::XYZPoint&,
                               ROOT::Math::XYZPoint& ) const {
      throw NotImplemented();
    }
    float calculateAverageGap( int, int, float, float ) const { throw NotImplemented(); }
    std::tuple<ROOT::Math::XYZPoint, ROOT::Math::XYZPoint> getGapPoints( int, double, double ) const {
      throw NotImplemented();
    }
  };
  using DeMuonChamber = DeMuonChamberElement<detail::DeMuonChamberObject>;

} // End namespace LHCb::Detector
