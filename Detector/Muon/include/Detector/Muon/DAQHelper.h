/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/L1Board.h"
#include "Detector/Muon/ODEBoard.h"
#include "Detector/Muon/TSMap.h"
#include "Detector/Muon/TileID.h"

#include <optional>
#include <string>
#include <vector>

namespace LHCb::Detector::Muon {

  struct DAQHelper {

    static const unsigned int maxTell1Number = 14;
    static const unsigned int maxODENumber   = 180;
    static const unsigned int linkNumber     = 24;

    // To be dropped once everything is implemented
    struct NotImplemented : std::exception {
      const char* what() const noexcept override { return "not implemented"; };
    };

    TileID getADDInODE( long, long ) const { throw NotImplemented(); }
    TileID getADDInODENoHole( long, long ) const { throw NotImplemented(); }
    TileID getADDInTell1( unsigned int, long ) const { throw NotImplemented(); }

    const std::vector<TileID>& getADDInTell1( unsigned int ) const { throw NotImplemented(); }
    TileID                     getPadInTell1DC06( unsigned int, long ) const { throw NotImplemented(); }
    TileID                     getPadInTell1V1( unsigned int, long ) const { throw NotImplemented(); }
    unsigned int               getPPNumber( unsigned int, unsigned int ) { throw NotImplemented(); }
    inline unsigned int        TotTellNumber() { throw NotImplemented(); }
    inline unsigned int        M1TellNumber() { throw NotImplemented(); }
    inline std::string         Tell1Name( unsigned int ) { throw NotImplemented(); }
    inline unsigned int        ODEInTell1( unsigned int ) { throw NotImplemented(); }
    inline unsigned int        getODENumberInTell1( unsigned int, unsigned int ) { throw NotImplemented(); }

    unsigned int getODENumberInLink( unsigned int, unsigned int ) { throw NotImplemented(); }

    struct DAQAddress {
      unsigned int position;
      long         ODENumber;
    };
    std::optional<DAQAddress>                findHWNumber( TileID ) { throw NotImplemented(); }
    std::optional<std::pair<TileID, TileID>> findStrips( TileID ) { throw NotImplemented(); }

    std::string findL1( TileID ) { throw NotImplemented(); }

    DAQAddress DAQaddressInODE( TileID, bool = true ) { throw NotImplemented(); }
    DAQAddress DAQaddressInL1( TileID, bool = true ) { throw NotImplemented(); }

    std::string               getBasePath( std::string ) { throw NotImplemented(); }
    std::vector<unsigned int> padsinTS( std::vector<unsigned int>&, std::string ) { throw NotImplemented(); }

    Muon::L1Board    getL1Board( unsigned int ) { throw NotImplemented(); }
    Muon::ODEBoard   getODEBoard( Muon::L1Board, unsigned int ) { throw NotImplemented(); }
    Muon::TSMap      getTSMap( Muon::L1Board, Muon::ODEBoard, unsigned int ) { throw NotImplemented(); }
    std::vector<int> getTell1InStation( int ) { throw NotImplemented(); }

    // GP
    // new method to retrieve the progressive number of Tell1 named L1Name
    // this is neded at the moment because TELL1s are indexed sometimes
    // by serial number and sometimes by an progressive counter.
    // This is irrelevant when M1 is present by matters if M1 is missing !
    //
    int findL1Index( std::string_view ) { throw NotImplemented(); }
  };

} // namespace LHCb::Detector::Muon
