/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\******************************************************************************/

#pragma once

#include "Detector/Rich/DeRichMapmt.h"

namespace LHCb::Detector {

  namespace detail {
    /**
     *  Rich2 Mapmt  data
     *  \author  Sajan Easo
     *  \date    2022-02-10
     *  \version  1.0
     */
    struct DeRich2StdMapmtObject : DeRichMapmtObject {
      DeRich2StdMapmtObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };
    struct DeRich2GrandMapmtObject : DeRichMapmtObject {
      DeRich2GrandMapmtObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };
  } // End namespace detail

  template <typename ObjectType>
  struct DeRich2StdMapmtElement : detail::DeRichMapmtElement<ObjectType> {
    using detail::DeRichMapmtElement<ObjectType>::DeRichMapmtElement;
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich2StdMapmt = DeRich2StdMapmtElement<detail::DeRich2StdMapmtObject>;

  template <typename ObjectType>
  struct DeRich2GrandMapmtElement : detail::DeRichMapmtElement<ObjectType> {
    using detail::DeRichMapmtElement<ObjectType>::DeRichMapmtElement;
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich2GrandMapmt = DeRich2GrandMapmtElement<detail::DeRich2GrandMapmtObject>;

} // namespace LHCb::Detector
