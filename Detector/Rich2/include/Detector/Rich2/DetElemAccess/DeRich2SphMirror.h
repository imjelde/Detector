/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*******************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/Rich/DeRichMirror.h"
#include "Detector/Rich/Rich_Geom_Constants_De.h"
#include <array>
#include <cstdint>
#include <optional>

namespace LHCb::Detector {

  namespace detail {
    /**
     *  Rich2 Sph Mirror detector element data
     *  \author  Sajan Easo
     *  \date    2022-01-11
     *  \version  1.0
     */
    struct DeRich2SphMirrorSegObject : DeRichMirrorSegObject {
      void PrintR2M1ReflectivityFromDBInObj();
      DeRich2SphMirrorSegObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

    struct DeRich2SphMirrorMasterObject : DeIOVObject {
      std::int32_t m_Rich2SphMMCopyIdRef       = -1;
      std::int32_t m_Rich2SphMMSN              = -1;
      std::int32_t m_Rich2NumSphMirrorSegTotal = -1;
      std::int32_t m_Rich2NumSphMirrorSegSide  = -1;

      using Segments = std::array<std::optional<DeRich2SphMirrorSegObject>, Rich2::NumRich2SphMirrorSegSide>;
      Segments m_Rich2SphMirrorSegDetElem;

      DeRich2SphMirrorMasterObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // End namespace detail

  template <typename ObjectType>
  struct DeRich2SphMirrorSegElement : detail::DeRichMirrorSegElement<ObjectType> {
    using detail::DeRichMirrorSegElement<ObjectType>::DeRichMirrorSegElement;
    void PrintReflectivityFromDB() const noexcept { this->access()->PrintR2M1ReflectivityFromDBInObj(); }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  /// The following line must be just after the corresponding template struct, as placed here.
  using DeRich2SphMirrorSeg = DeRich2SphMirrorSegElement<detail::DeRich2SphMirrorSegObject>;

  template <typename ObjectType>
  struct DeRich2SphMirrorMasterElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;

    auto SphMMSN() const noexcept { return this->access()->m_Rich2SphMMSN; }
    auto SphMMCopyIdRef() const noexcept { return this->access()->m_Rich2SphMMCopyIdRef; }

    auto NumSphMirrorSegTotal() const noexcept { return this->access()->m_Rich2NumSphMirrorSegTotal; }
    auto NumSphMirrorSegSide() const noexcept { return this->access()->m_Rich2NumSphMirrorSegSide; }
    auto SphMirrorSeg( int aS ) const noexcept {
      assert( this->access()->m_Rich2SphMirrorSegDetElem[aS].has_value() );
      return DeRich2SphMirrorSeg( &( this->access()->m_Rich2SphMirrorSegDetElem[aS].value() ) );
    }
    auto SphMirrorSegVect() const noexcept {
      std::vector<DeRich2SphMirrorSeg> aRich2SphMSegVect( NumSphMirrorSegSide() );
      for ( int i = 0; i < ( NumSphMirrorSegSide() ); i++ ) { aRich2SphMSegVect[i] = SphMirrorSeg( i ); }
      return aRich2SphMSegVect;
    }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich2SphMirrorMaster = DeRich2SphMirrorMasterElement<detail::DeRich2SphMirrorMasterObject>;

} // End namespace LHCb::Detector
