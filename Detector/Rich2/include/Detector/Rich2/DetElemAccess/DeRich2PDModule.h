/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*******************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/Rich/DeRichPDModule.h"
#include "Detector/Rich/Rich_Geom_Constants_De.h"
#include "Detector/Rich2/DetElemAccess/DeRich2Mapmt.h"
#include <array>
#include <optional>

namespace LHCb::Detector {

  //====================================================================================//
  namespace detail {
    /**
     *  Rich2 Mapmt Module  data
     *  \author  Sajan Easo
     *  \date    2022-02-10
     *  \version  1.0
     */

    struct DeRich2StdPDModuleObject : DeRichPDModuleObject {
      using PMTs = std::array<std::optional<DeRich2StdMapmtObject>, Rich1::NumRich1MapmtsInStdModule>;
      PMTs m_Rich2StdMapmtDetElem;
      DeRich2StdPDModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

    struct DeRich2GrandPDModuleObject : DeRichPDModuleObject {
      using PMTs = std::array<std::optional<DeRich2GrandMapmtObject>, Rich2::NumRich2MapmtsInGrandModule>;
      PMTs m_Rich2GrandMapmtDetElem;
      DeRich2GrandPDModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // End namespace detail

  template <typename ObjectType>
  struct DeRich2StdPDModuleElement : detail::DeRichPDModuleElement<ObjectType> {
    using detail::DeRichPDModuleElement<ObjectType>::DeRichPDModuleElement;
    auto Mapmt( const unsigned int aP ) const noexcept {
      assert( aP < this->access()->m_Rich2StdMapmtDetElem.size() );
      assert( this->access()->m_Rich2StdMapmtDetElem[aP].has_value() );
      return DeRich2StdMapmt( &( this->access()->m_Rich2StdMapmtDetElem[aP].value() ) );
    }
    auto Mapmts() const noexcept {
      std::vector<DeRich2StdMapmt> pmts;
      const auto                   nPmts = this->access()->m_Rich2StdMapmtDetElem.size();
      pmts.reserve( nPmts );
      for ( std::size_t i = 0; i < nPmts; ++i ) { pmts.emplace_back( this->Mapmt( i ) ); }
      return pmts;
    }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich2StdMapmt    = DeRich2StdMapmtElement<detail::DeRich2StdMapmtObject>;
  using DeRich2StdPDModule = DeRich2StdPDModuleElement<detail::DeRich2StdPDModuleObject>;

  template <typename ObjectType>
  struct DeRich2GrandPDModuleElement : detail::DeRichPDModuleElement<ObjectType> {
    using detail::DeRichPDModuleElement<ObjectType>::DeRichPDModuleElement;
    auto Mapmt( const unsigned int aP ) const noexcept {
      assert( aP < this->access()->m_Rich2GrandMapmtDetElem.size() );
      assert( this->access()->m_Rich2GrandMapmtDetElem[aP].has_value() );
      return DeRich2GrandMapmt( &( this->access()->m_Rich2GrandMapmtDetElem[aP].value() ) );
    }
    auto Mapmts() const noexcept {
      std::vector<DeRich2GrandMapmt> pmts;
      const auto                     nPmts = this->access()->m_Rich2GrandMapmtDetElem.size();
      pmts.reserve( nPmts );
      for ( std::size_t i = 0; i < nPmts; ++i ) { pmts.emplace_back( this->Mapmt( i ) ); }
      return pmts;
    }
  };

  /// For the full object, we have to combine them with the geometry stuff:
  using DeRich2GrandMapmt    = DeRich2GrandMapmtElement<detail::DeRich2GrandMapmtObject>;
  using DeRich2GrandPDModule = DeRich2GrandPDModuleElement<detail::DeRich2GrandPDModuleObject>;

} // End namespace LHCb::Detector
