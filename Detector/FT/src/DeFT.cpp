/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Framework include files
#include "Detector/FT/DeFT.h"
#include "Detector/FT/FTChannelID.h"

#include "DD4hep/Printout.h"

// FIXME: ROOT vs LHCB lengths

LHCb::Detector::detail::DeFTObject::DeFTObject( const dd4hep::DetElement&             de,
                                                dd4hep::cond::ConditionUpdateContext& ctxt )
    : DeIOVObject( de, ctxt )
    , m_stations{{{de.child( "T1" ), ctxt, 1}, {de.child( "T2" ), ctxt, 2}, {de.child( "T3" ), ctxt, 3}}} {}

LHCb::Detector::detail::DeFTStationObject::DeFTStationObject( const dd4hep::DetElement&             de,
                                                              dd4hep::cond::ConditionUpdateContext& ctxt,
                                                              unsigned int                          iStation )
    : DeIOVObject( de, ctxt )
    , m_id{iStation}
    , m_layers{{{de.child( "X1" ), ctxt, 0, m_id},
                {de.child( "U" ), ctxt, 1, m_id},
                {de.child( "V" ), ctxt, 2, m_id},
                {de.child( "X2" ), ctxt, 3, m_id}}} {}

LHCb::Detector::detail::DeFTLayerObject::DeFTLayerObject( const dd4hep::DetElement&             de,
                                                          dd4hep::cond::ConditionUpdateContext& ctxt,
                                                          unsigned int iLayer, unsigned int stationID )
    : DeIOVObject( de, ctxt )
    , m_layerID{( stationID * FT::nLayers + iLayer )}
    , m_quarters{{{de.child( "Q0" ), ctxt, 0, m_layerID},
                  {de.child( "Q1" ), ctxt, 1, m_layerID},
                  {de.child( "Q2" ), ctxt, 2, m_layerID},
                  {de.child( "Q3" ), ctxt, 3, m_layerID}}} {
  // Get the global z position of the layer
  ROOT::Math::XYZPoint globalPoint = this->toGlobal( ROOT::Math::XYZPoint( 0., 0., 0. ) );
  m_globalZ                        = globalPoint.z();

  // Get the boundaries of the layer
  // FIXME
  //      const dd4hep::Box* box = dynamic_cast<const dd4hep::Box*>( geometry()->lvolume()->solid()->coverTop() );
  //      m_sizeX             = box->xsize();
  //      m_sizeY             = box->ysize();

  // Make the plane for the layer
  const ROOT::Math::XYZPoint g1 = this->toGlobal( ROOT::Math::XYZPoint( 0., 0., 0. ) );
  const ROOT::Math::XYZPoint g2 = this->toGlobal( ROOT::Math::XYZPoint( 1., 0., 0. ) );
  const ROOT::Math::XYZPoint g3 = this->toGlobal( ROOT::Math::XYZPoint( 0., 1., 0. ) );
  m_plane                       = ROOT::Math::Plane3D( g1, g2, g3 );
  m_dzdy                        = ( g3.z() - g1.z() ) / ( g3.y() - g1.y() );
  //      m_dxdy                   = -tan( m_stereoAngle );
  m_dxdy        = ( g3.x() - g1.x() ) / ( g3.y() - g1.y() ); // FIXME: to check
  m_stereoAngle = -atan( m_dxdy );
}

LHCb::Detector::detail::DeFTQuarterObject::DeFTQuarterObject( const dd4hep::DetElement&             de,
                                                              dd4hep::cond::ConditionUpdateContext& ctxt,
                                                              unsigned int iQuarter, unsigned int layerID )
    : DeIOVObject( de, ctxt )
    , m_id{layerID * FT::nQuarters + iQuarter} // FIXME no always 6 modules ?
    , m_modules{} {

  m_modules.emplace_back( de.child( "M0" ), ctxt, 0, m_id );
  m_modules.emplace_back( de.child( "M1" ), ctxt, 1, m_id );
  m_modules.emplace_back( de.child( "M2" ), ctxt, 2, m_id );
  m_modules.emplace_back( de.child( "M3" ), ctxt, 3, m_id );
  m_modules.emplace_back( de.child( "M4" ), ctxt, 4, m_id );
  // Only T3 has this module, we check whether we have it defined
  const auto& c = de.children();
  if ( c.find( "M5" ) != c.end() ) { m_modules.emplace_back( de.child( "M5" ), ctxt, 5, m_id ); }
}

LHCb::Detector::detail::DeFTModuleObject::DeFTModuleObject( const dd4hep::DetElement&             de,
                                                            dd4hep::cond::ConditionUpdateContext& ctxt,
                                                            unsigned int iModule, unsigned int quarterID )
    : DeIOVObject( de, ctxt )
    , m_id{quarterID * FT::nModules + iModule}
    , m_mats{{{de.child( "Mat0" ), ctxt, 0, m_id},
              {de.child( "Mat1" ), ctxt, 1, m_id},
              {de.child( "Mat2" ), ctxt, 2, m_id},
              {de.child( "Mat3" ), ctxt, 3, m_id}}} {
  // Is reversed?
  const auto firstPoint = this->toGlobal( {-1, 0, 0} );
  const auto lastPoint  = this->toGlobal( {1, 0, 0} );
  m_reversed            = std::abs( firstPoint.x() ) > std::abs( lastPoint.x() );
  // Make the plane for the module
  const ROOT::Math::XYZPoint g1 = this->toGlobal( ROOT::Math::XYZPoint( 0., 0., 0. ) );
  const ROOT::Math::XYZPoint g2 = this->toGlobal( ROOT::Math::XYZPoint( 1., 0., 0. ) );
  const ROOT::Math::XYZPoint g3 = this->toGlobal( ROOT::Math::XYZPoint( 0., 1., 0. ) );
  m_plane                       = ROOT::Math::Plane3D( g1, g2, g3 );

  // Derive the module id/quarter id/layer id/ station id
  auto tmp = m_id;
  auto m   = tmp % FT::nModules;
  tmp /= FT::nModules;
  auto q = tmp % FT::nQuarters;
  tmp /= FT::nQuarters;
  auto l = tmp % FT::nLayers;
  tmp /= FT::nLayers;
  auto s = tmp % FT::nStations;
  // Validation debug
  // std::cout << "============== FT m_id:" << m_id << " m:" << m << " q:" << q << " l:" << l << " s:" << s << " "
  //           << de.path() << '\n';

  auto localModuleID  = FTChannelID::ModuleID{static_cast<unsigned int>( m )};
  auto localQuarterID = FTChannelID::QuarterID{static_cast<unsigned int>( q )};
  auto localLayerID   = FTChannelID::LayerID{static_cast<unsigned int>( l )};
  auto localStationID = FTChannelID::StationID{static_cast<unsigned int>( s )};
  m_elementID         = FTChannelID( localStationID, localLayerID, localQuarterID, localModuleID, 0 );
}

LHCb::Detector::detail::DeFTMatObject::DeFTMatObject( const dd4hep::DetElement&             de,
                                                      dd4hep::cond::ConditionUpdateContext& ctxt, unsigned int iMat,
                                                      unsigned int moduleID )
    : DeIOVObject( de, ctxt ), m_elementID{iMat}, m_matID{moduleID * FT::nMats + iMat} {
  // FIXME: see VP
  // Get some useful geometric parameters from the database
  m_halfChannelPitch = 0.5f * m_channelPitch;

  m_sipmPitch      = m_nChannelsInSiPM * m_channelPitch + m_dieGap + 2 * m_airGap + 2 * m_deadRegion;
  m_nChannelsInDie = m_nChannelsInSiPM / m_nDiesInSiPM;
  m_diePitch       = m_nChannelsInDie * m_channelPitch + m_dieGap;
  m_uBegin         = m_airGap + m_deadRegion - 2.f * m_sipmPitch;

  // Update cache
  // Get the central points of the fibres at the mirror and at the SiPM locations
  m_mirrorPoint = this->toGlobal( ROOT::Math::XYZPoint( 0, -0.5f * m_sizeY, 0 ) );
  m_sipmPoint   = this->toGlobal( ROOT::Math::XYZPoint( 0, +0.5f * m_sizeY, 0 ) );

  // Define the global z position to be at the point closest to the mirror
  m_globalZ = m_mirrorPoint.z();

  // Define the global length in y of the mat
  m_globaldy = m_sipmPoint.y() - m_mirrorPoint.y();

  // Make the plane for the mat
  const ROOT::Math::XYZPoint g1 = this->toGlobal( ROOT::Math::XYZPoint( 0., 0., 0. ) );
  const ROOT::Math::XYZPoint g2 = this->toGlobal( ROOT::Math::XYZPoint( 1., 0., 0. ) );
  const ROOT::Math::XYZPoint g3 = this->toGlobal( ROOT::Math::XYZPoint( 0., 1., 0. ) );
  m_plane                       = ROOT::Math::Plane3D( g1, g2, g3 );

  // Get the slopes in units of local delta x
  m_ddx = ROOT::Math::XYZVectorF( g2 - g1 );

  // Get the slopes in units of delta y (needed by PrFTHit, mind the sign)
  ROOT::Math::XYZVectorF deltaY( g1 - g3 );
  m_dxdy = deltaY.x() / deltaY.y();
  m_dzdy = deltaY.z() / deltaY.y();
}
