/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace LHCb::Detector {
  namespace FT {
    enum DetElementTypes {
      TOP     = 1 << 0,
      STATION = 1 << 1,
      LAYER   = 1 << 2,
      QUARTER = 1 << 3,
      MODULE  = 1 << 4,
      MAT     = 1 << 5
    };
    constexpr unsigned int nStations = 3;
    constexpr unsigned int nLayers   = 4;
    constexpr unsigned int nQuarters = 4;
    constexpr unsigned int nModules  = 6;
    constexpr unsigned int nMats     = 4;
    constexpr unsigned int nSiPM     = 4;
    constexpr unsigned int nChannels = 128;
  } // namespace FT

} // End namespace LHCb::Detector
