/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/FT/DeFTMat.h"

#include <array>

namespace LHCb::Detector {

  namespace detail {

    /**
     *  Generic FT iov dependent detector element of type FT module
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    struct DeFTModuleObject : DeIOVObject {
      /// Reference to the static information of mats
      unsigned int                         m_id;
      int                                  m_nChannelsInModule{dd4hep::_toInt( "FT:nChannelsInModule" )};
      std::array<DeFTMatObject, FT::nMats> m_mats;
      ROOT::Math::Plane3D                  m_plane; ///< xy-plane in the z-middle of the module
      bool                                 m_reversed;
      FTChannelID                          m_elementID;
      DeFTModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt, unsigned int iModule,
                        unsigned int quarterID );
      /// Convert local position to global position
      ROOT::Math::XYZPoint toGlobal( const ROOT::Math::XYZPoint& point ) const {
        return ROOT::Math::XYZPoint( this->detectorAlignment.localToWorld( ROOT::Math::XYZVector( point ) ) );
      }
      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& mat : m_mats ) { func( LHCb::Detector::DeIOV{&mat} ); };
      };
      FTChannelID::StationID stationID() const { return m_elementID.station(); }
      FTChannelID::ModuleID  moduleID() const { return FTChannelID::ModuleID{m_id}; }

      /// Get the pseudo-channel for a FTChannelID (useful in the monitoring)
      int pseudoChannel( const FTChannelID channelID ) const {
        int channelInModule = channelID.channelID() & ( m_nChannelsInModule - 1u );
        if ( m_reversed ) { channelInModule = m_nChannelsInModule - 1 - channelInModule; }
        return channelInModule + to_unsigned( moduleID() ) * m_nChannelsInModule;
      }

      FTChannelID channelFromPseudo( const int pseudoChannel ) const {
        int channelInModule = pseudoChannel & ( m_nChannelsInModule - 1u );
        if ( m_reversed ) { channelInModule = m_nChannelsInModule - 1 - channelInModule; }
        return FTChannelID( m_elementID + channelInModule );
      }
    };
  } // End namespace detail
  template <typename ObjectType>
  struct DeFTModuleElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;
    FTChannelID::ModuleID  moduleID() const { return this->access()->moduleID(); }
    FTChannelID::StationID stationID() const { return this->access()->stationID(); }

    /// Find the layer for a given XYZ point
    const std::optional<DeFTMat> findMat( const ROOT::Math::XYZPoint& aPoint ) const {
      /// Find the layer and return a pointer to the layer from XYZ point
      const auto iter =
          std::find_if( this->access()->m_mats.begin(), this->access()->m_mats.end(),
                        [&aPoint]( const detail::DeFTMatObject& m ) { return DeFTMat{&m}.isInside( aPoint ); } );
      return iter != this->access()->m_mats.end() ? iter : std::optional<DeFTMat>{}; // DeFTMat{};
    }

    const std::optional<DeFTMat> findMat( const FTChannelID& id ) const {
      return ( to_unsigned( id.mat() ) < this->access()->m_mats.size() )
                 ? &( this->access()->m_mats[to_unsigned( id.mat() )] )
                 : std::optional<DeFTMat>{};
    }

    [[nodiscard]] FTChannelID elementID() const { return this->access()->m_elementID; }

    [[nodiscard]] int pseudoChannel( const FTChannelID channelID ) const {
      return this->access()->pseudoChannel( channelID );
    }

    [[nodiscard]] FTChannelID channelFromPseudo( const int pseudoChannel ) const {
      const auto& obj             = this->access();
      int         channelInModule = pseudoChannel & ( obj->m_nChannelsInModule - 1u );
      if ( obj->m_reversed ) { channelInModule = obj->m_nChannelsInModule - 1 - channelInModule; }
      return FTChannelID( this->elementID() + channelInModule );
    }
    ROOT::Math::Plane3D plane() const { return this->access()->m_plane; }
  };

  using DeFTModule = DeFTModuleElement<detail::DeFTModuleObject>;

} // End namespace LHCb::Detector
