/*****************************************************************************\
 * (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/FT/DeFTStation.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
namespace DeFTLocation {
  // FT location defined in the XmlDDDB
  inline const std::string Default = "/dd/Structure/LHCb/AfterMagnetRegion/T/FT";
} // namespace DeFTLocation

namespace LHCb::Detector {
  namespace detail {
    /**
     *  FT detector element data
     *  \author  Markus Frank
     *  \date    2018-11-08
     *  \version  1.0
     */
    struct DeFTObject : DeIOVObject {
      int m_version{dd4hep::_toInt( "FT:version" )};                     // FT Geometry Version
      int m_nModulesT1{dd4hep::_toInt( "FT:nModulesT1" )};               // Number of modules in T1
      int m_nModulesT2{dd4hep::_toInt( "FT:nModulesT2" )};               // Number of modules in T2
      int m_nModulesT3{dd4hep::_toInt( "FT:nModulesT3" )};               // Number of modules in T3
      int m_nStations{dd4hep::_toInt( "FT:nStations" )};                 // Number of stations
      int m_nLayers{dd4hep::_toInt( "FT:nLayers" )};                     // Number of layers per station
      int m_nQuarters{dd4hep::_toInt( "FT:nQuarters" )};                 // Number of quarters per layer
      int m_nChannelsInModule{dd4hep::_toInt( "FT:nChannelsInModule" )}; // Number of channels per SiPM
      int m_nTotQuarters = m_nStations * m_nLayers * m_nQuarters;
      int m_nTotModules  = m_nModulesT1 + m_nModulesT2 + m_nModulesT3;
      int m_nTotChannels = m_nTotModules * m_nChannelsInModule;

      std::array<DeFTStationObject, 3> m_stations;
      DeFTObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& station : m_stations ) { func( LHCb::Detector::DeIOV{&station} ); };
      };
      void applyToAllLayers( const std::function<void( LHCb::Detector::DeIOV )>& func ) const {
        for ( auto& station : m_stations ) { station.applyToAllChildren( func ); };
      };
      void applyToAllMats( const std::function<void( DeIOVElement<detail::DeIOVObject> )>& func ) const {
        for ( auto& station : m_stations )
          for ( auto& layer : station.m_layers )
            for ( auto& quarter : layer.m_quarters )
              for ( auto& module : quarter.m_modules ) module.applyToAllChildren( func );
      }
      const DeFTMatObject& firstMat() const { return m_stations[0].m_layers[0].m_quarters[0].m_modules[0].m_mats[0]; };
    }; // End namespace detail
  }    // namespace detail
  /**
   *  FT interface
   *
   *  \author  Louis Henry
   *  \date    2021-09-21
   *  \version  1.0
   */
  template <typename ObjectType>
  struct DeFTElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;
    int                              version() const { return this->access()->m_version; };
    const std::array<DeFTStation, 3> stations() const {
      const auto obj = this->access();
      return {obj->m_stations[0], obj->m_stations[1], obj->m_stations[2]};
    };
    void applyToAllLayers( const std::function<void( DeIOVElement<detail::DeIOVObject> )>& func ) const {
      this->access()->applyToAllLayers( func );
    };
    void applyToAllMats( const std::function<void( DeIOVElement<detail::DeIOVObject> )>& func ) const {
      this->access()->applyToAllMats( func );
    }
    const DeFTMat& firstMat() const { return &( this->access()->firstMat() ); }

    int nStations() const { return this->access()->m_nStations; }
    int nChannels() const { return this->access()->m_nTotChannels; }

    /** Find the FT Station corresponding to the point
     *  @return Pointer to the relevant station
     */
    [[nodiscard]] const std::optional<DeFTStation> findStation( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto iS = std::find_if(
          std::begin( this->access()->m_stations ), std::end( this->access()->m_stations ),
          [&aPoint]( detail::DeFTStationObject const& s ) { return DeFTStation{&s}.isInside( aPoint ); } );
      return iS != this->access()->m_stations.end() ? std::optional<DeFTStation>{iS} : std::optional<DeFTStation>{};
    }

    /// Find the layer for a given XYZ point
    [[nodiscard]] const std::optional<DeFTLayer> findLayer( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto s = findStation( aPoint );
      return s ? s->findLayer( aPoint ) : std::optional<DeFTLayer>{};
    }

    /// Find the quarter for a given XYZ point
    [[nodiscard]] const std::optional<DeFTQuarter> findQuarter( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto l = findLayer( aPoint );
      return l ? l->findQuarter( aPoint ) : std::optional<DeFTQuarter>{};
    }

    /// Find the module for a given XYZ point
    [[nodiscard]] const std::optional<DeFTModule> findModule( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto l = findLayer( aPoint ); // is faster than via DeFTQuarter
      return l ? l->findModule( aPoint ) : std::optional<DeFTModule>{};
    }

    /// Find the mat for a given XYZ point
    [[nodiscard]] const std::optional<DeFTMat> findMat( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto m = findModule( aPoint );
      return m ? m->findMat( aPoint ) : std::optional<DeFTMat>{};
    }

    /** Find the FT Station corresponding to the channel id
     *  @return Pointer to the relevant station
     */
    [[nodiscard]] const std::optional<DeFTStation> findStation( const FTChannelID& aChannel ) const {
      return ( to_unsigned( aChannel.station() ) < this->access()->m_stations.size() )
                 ? &( this->access()->m_stations[to_unsigned( aChannel.station() )] )
                 : std::optional<DeFTStation>{};
    }

    /** Find the FT Layer corresponding to the channel id
     *  @return Pointer to the relevant layer
     */
    [[nodiscard]] const std::optional<DeFTLayer> findLayer( const FTChannelID& aChannel ) const {
      const auto s = findStation( aChannel );
      return s ? s->findLayer( aChannel ) : std::optional<DeFTLayer>{};
    }

    /** Find the FT Quarter corresponding to the channel id
     *  @return Pointer to the relevant quarter
     */
    [[nodiscard]] const std::optional<DeFTQuarter> findQuarter( const FTChannelID& aChannel ) const {
      const auto l = findLayer( aChannel );
      return l ? l->findQuarter( aChannel ) : std::optional<DeFTQuarter>{};
    }

    /** Find the FT Module corresponding to the channel id
     *  @return Pointer to the relevant module
     */
    [[nodiscard]] const std::optional<DeFTModule> findModule( const FTChannelID& aChannel ) const {
      const auto q = findQuarter( aChannel );
      return q ? q->findModule( aChannel ) : std::optional<DeFTModule>{};
    }

    /** Find the FT Mat corresponding to the channel id
     *  @return Pointer to the relevant module
     */
    [[nodiscard]] const std::optional<DeFTMat> findMat( const FTChannelID& aChannel ) const {
      const auto m = findModule( aChannel );
      return m ? m->findMat( aChannel ) : std::optional<DeFTMat>{};
    }

    int sensitiveVolumeID( const ROOT::Math::XYZPoint& point ) const {
      const auto& mat = findMat( point );
      return mat ? mat->sensitiveVolumeID( point ) : -1;
    }

    /// Get a random FTChannelID (useful for the thermal noise, which is ~flat)
    FTChannelID getRandomChannelFromSeed( const float seed ) const {
      if ( seed < 0.f || seed > 1.f ) return FTChannelID::kInvalidChannel();
      const auto&  obj             = this->access();
      unsigned int flatChannel     = int( seed * obj->m_nTotChannels );
      unsigned int channelInModule = flatChannel & ( obj->m_nChannelsInModule - 1u );
      flatChannel /= obj->m_nChannelsInModule;
      unsigned int quarter = flatChannel & ( obj->m_nQuarters - 1u );
      flatChannel /= obj->m_nQuarters;
      unsigned int layer = flatChannel & ( obj->m_nLayers - 1u );
      flatChannel /= obj->m_nLayers;
      unsigned int station = 1;
      unsigned int module  = flatChannel;
      if ( flatChannel >= obj->m_nModulesT1 + obj->m_nModulesT2 ) {
        station = 3;
        module  = flatChannel - obj->m_nModulesT1 - obj->m_nModulesT2;
      } else if ( flatChannel >= obj->m_nModulesT1 ) {
        station = 2;
        module  = flatChannel - obj->m_nModulesT1;
      }
      return FTChannelID( FTChannelID::StationID{station}, FTChannelID::LayerID{layer}, FTChannelID::QuarterID{quarter},
                          FTChannelID::ModuleID{module}, channelInModule );
    }

    /// Get a random FTChannelID from a pseudoChannel (useful for the AP noise)
    FTChannelID getRandomChannelFromPseudo( const int pseudoChannel, const float seed ) const {
      if ( seed < 0.f || seed > 1.f ) return FTChannelID::kInvalidChannel();
      const auto&  obj         = this->access();
      unsigned int flatQuarter = int( seed * obj->m_nTotQuarters );
      auto         quarter     = FTChannelID::QuarterID{flatQuarter & ( obj->m_nQuarters - 1u )};
      flatQuarter /= obj->m_nQuarters;
      auto layer = FTChannelID::LayerID{flatQuarter & ( obj->m_nLayers - 1u )};
      flatQuarter /= obj->m_nLayers;
      auto station = FTChannelID::StationID{( flatQuarter & obj->m_stations.size() ) + 1u};

      auto        module    = FTChannelID::ModuleID{pseudoChannel / obj->m_nChannelsInModule};
      const auto& moduleDet = findModule( FTChannelID( station, layer, quarter, module, 0u ) );
      return moduleDet->channelFromPseudo( pseudoChannel & ( obj->m_nChannelsInModule - 1u ) );
    }
  };

  using DeFT = DeFTElement<detail::DeFTObject>;

} // End namespace LHCb::Detector
