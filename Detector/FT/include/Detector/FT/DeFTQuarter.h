/*****************************************************************************\
 * (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <deque>

#include "Core/DeIOV.h"
#include "Detector/FT/DeFTModule.h"

namespace LHCb::Detector {

  namespace detail {

    /**
     *  Generic FT iov dependent detector element of a FT quarter
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    struct DeFTQuarterObject : DeIOVObject {
      /// Reference to the static information of modules
      unsigned int                 m_id;
      std::deque<DeFTModuleObject> m_modules;
      DeFTQuarterObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt,
                         unsigned int iQuarter, unsigned int layerID );
      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& module : m_modules ) { func( LHCb::Detector::DeIOV{&module} ); };
      };
    };
  } // End namespace detail

  template <typename ObjectType>
  struct DeFTQuarterElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;
    unsigned int quarterID() const { return this->access()->m_id; }

    /** Find the FT Module corresponding to the point
     *  @return Pointer to the relevant module
     */
    [[nodiscard]] const std::optional<DeFTModule> findModule( const ROOT::Math::XYZPoint& aPoint ) const {
      auto iM =
          std::find_if( this->access()->m_modules.begin(), this->access()->m_modules.end(),
                        [&aPoint]( const detail::DeFTModuleObject& m ) { return DeFTModule{&m}.isInside( aPoint ); } );
      return iM != this->access()->m_modules.end() ? DeFTModule{&*iM} : std::optional<DeFTModule>{}; // DeFTModule{};
    }
    /** Const method to return the module for a given channel id
     * @param  aChannel an FT channel id
     * @return pointer to detector element
     */
    [[nodiscard]] const std::optional<DeFTModule> findModule( const FTChannelID& id ) const {
      return ( to_unsigned( id.module() ) < this->access()->m_modules.size() )
                 ? &( this->access()->m_modules[to_unsigned( id.mat() )] )
                 : std::optional<DeFTModule>{};
    }

    /** Flat vector of all FT modules
     * @return vector of modules
     */
    [[nodiscard]] const auto& modules() const { return this->access()->m_modules; }
  };

  using DeFTQuarter = DeFTQuarterElement<detail::DeFTQuarterObject>;

} // End namespace LHCb::Detector
