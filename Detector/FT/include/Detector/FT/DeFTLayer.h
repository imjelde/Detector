/*****************************************************************************\
 * (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/FT/DeFTQuarter.h"

#include <array>

namespace LHCb::Detector {

  namespace detail {

    /**
     *  Generic FT iov dependent detector element of a FT layer
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    struct DeFTLayerObject : DeIOVObject {
      /// Reference to the static information of the quarters
      /// FIXME: LayerID does not mean the same thing as in DetDesc. Here it is a global layer ID.
      unsigned int        m_layerID; ///< layer ID number
      float               m_globalZ; ///< Global z position of layer closest to y-axis
      ROOT::Math::Plane3D m_plane;   ///< xy-plane in the z-middle of the layer
      float               m_dzdy;    ///< dz/dy of the layer (tan of the beam angle)
      float m_stereoAngle{0};        // = {dd4hep::_toFloat("FT::stereoAngle")}; ///< stereo angle of the layer // FIXME
      float m_sizeX = {dd4hep::_toFloat( "FT:StationSizeX" )}; ///< Size of the layer in x // FIXME
      float m_sizeY = {dd4hep::_toFloat( "FT:StationSizeY" )}; ///< Size of the layer in y // FIXME
      float m_dxdy  = tan( m_stereoAngle );                    ///< dx/dy of the layer (ie. tan(m_stereoAngle))
      std::array<DeFTQuarterObject, FT::nQuarters> m_quarters;
      DeFTLayerObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt, unsigned int iLayer,
                       unsigned int stationID );
      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& quarter : m_quarters ) { func( LHCb::Detector::DeIOV{&quarter} ); };
      };
      void applyToAllMats( const std::function<void( LHCb::Detector::DeIOV )>& func ) const {
        for ( auto& quarter : m_quarters ) {
          for ( auto& module : quarter.m_modules ) module.applyToAllChildren( func );
        }
      };
      ROOT::Math::XYZPoint toLocal( const ROOT::Math::XYZPoint& p ) const {
        return ROOT::Math::XYZPoint(
            toLHCbLengthUnits( this->detectorAlignment.worldToLocal( toDD4hepUnits( ROOT::Math::XYZVector( p ) ) ) ) );
      }
      ROOT::Math::XYZVector toLocal( const ROOT::Math::XYZVector& v ) const {
        return toLHCbLengthUnits( this->detectorAlignment.worldToLocal( toDD4hepUnits( v ) ) );
      }
      ROOT::Math::XYZPoint toGlobal( const ROOT::Math::XYZPoint& p ) const {
        return ROOT::Math::XYZPoint( this->detectorAlignment.localToWorld( ROOT::Math::XYZVector( p ) ) );
      }
      ROOT::Math::XYZVector toGlobal( const ROOT::Math::XYZVector& v ) const {
        return this->detectorAlignment.localToWorld( v );
      }
    };
  } // End namespace detail

  template <typename ObjectType>
  struct DeFTLayerElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;
    unsigned int layerID() const { return this->access()->m_layerID; }

    /** Find the FT Quarter corresponding to the point
     *  @return Pointer to the relevant quarter
     */
    [[nodiscard]] const std::optional<DeFTQuarter> findQuarter( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto iQ = std::find_if(
          this->access()->m_quarters.begin(), this->access()->m_quarters.end(),
          [&aPoint]( const detail::DeFTQuarterObject& q ) { return DeFTQuarter{&q}.isInside( aPoint ); } );
      return iQ != this->access()->m_quarters.end() ? iQ : std::optional<DeFTQuarter>{}; // DeFTQuarter{};
    }

    /// Find the module for a given XYZ point
    [[nodiscard]] const std::optional<DeFTModule> findModule( const ROOT::Math::XYZPoint& aPoint ) const {
      const auto iQ = findQuarter( aPoint );
      return iQ ? iQ->findModule( aPoint ) : std::optional<DeFTModule>{}; // DeFTModule{};
    }

    /** Const method to return the layer for a given channel id
     * @param  aChannel an FT channel id
     * @return pointer to detector element
     */
    [[nodiscard]] const std::optional<DeFTQuarter> findQuarter( const FTChannelID& id ) const {
      return ( to_unsigned( id.quarter() ) < this->access()->m_quarters.size() )
                 ? &( this->access()->m_quarters[to_unsigned( id.quarter() )] )
                 : std::optional<DeFTQuarter>{};
    }

    float               globalZ() const { return this->access()->m_globalZ; }         // FIXME
    ROOT::Math::Plane3D plane() const { return this->access()->m_plane; }             // FIXME
    float               dxdy() const { return this->access()->m_dxdy; }               // FIXME
    float               stereoAngle() const { return this->access()->m_stereoAngle; } // FIXME
    float               dzdy() const { return this->access()->m_dzdy; }               // FIXME
    float               sizeX() const { return this->access()->m_sizeX; }             // FIXME
    float               sizeY() const { return this->access()->m_sizeY; }             // FIXME
  };

  using DeFTLayer = DeFTLayerElement<detail::DeFTLayerObject>;

} // End namespace LHCb::Detector
