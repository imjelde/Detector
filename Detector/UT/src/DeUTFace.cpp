/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/UT/DeUTFace.h"
#include "Core/PrintHelpers.h"

#include "DD4hep/Printout.h"

LHCb::Detector::UT::detail::DeUTFaceObject::DeUTFaceObject( const dd4hep::DetElement&             de,
                                                            dd4hep::cond::ConditionUpdateContext& ctxt )
    : DeIOVObject( de, ctxt, 9311 ) {
  for ( auto iter : de.children() ) {
    // m_modules.emplace_back( iter.second, ctxt );
  }
  /*ChanID = template UTChannelID(
      template UTChannelID::detType::typeUT,
      parentObject.ChanID.template <UTChannelID::side>() parentObject.ChanID.template <UTChannelID::halflayer>(),
      parentObject.ChanID.template <UTChannelID::stave>(), id, 0, 0, 0 );

  {
    m_fractionActive = 0;
    unsigned int nSectors;
    for ( auto& module : m_modules ) {
      for ( auto& sector : module.m_sectors ) {
        m_fractionActive += sector.fractionActive();
      }
      nSectors += module.m_sectors.size();
    }
    m_fractionActive /= nSectors;
  }
  */
}
