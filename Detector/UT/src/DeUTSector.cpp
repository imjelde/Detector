/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/UT/DeUTSector.h"
#include "Core/PrintHelpers.h"

#include "DD4hep/Printout.h"

namespace LHCb::Detector::UT {

  std::string toString( Status status ) {
    auto i = s_map.find( status );
    return i == s_map.end() ? "UnknownStatus" : i->second;
  }

  Status toStatus( std::string_view str ) {
    auto i = std::find_if( s_map.begin(), s_map.end(), [&]( const auto& i ) { return i.second == str; } );
    return i == s_map.end() ? Status::UnknownStatus : i->first;
  }

  detail::DeUTSectorObject::DeUTSectorObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt )
      : DeIOVObject( de, ctxt, 9320 ) {
    /*
      m_pitch = ;
      m_stripflip = ;
      m_nStrip = ;
      m_deadWidth = ;
      m_capacitance = ;
      m_xInverted = ;
      m_yInverted = ;

      {
      auto    firstTraj = this->createTraj( m_firstStrip, 0 );
      if ( m_stripflip && m_xInverted ) firstTraj = this->createTraj( m_nStrip - m_firstStrip, 0 );
      const ROOT::Math::XYZPoint g1          = firstTraj.beginPoint();
      const ROOT::Math::XYZPoint g2          = firstTraj.endPoint();
      const double               activeWidth = m_sensors.front().activeWidth();
      ROOT::Math::XYZVector      direction   = g2 - g1;
      m_stripLength                        = std::sqrt( direction.Mag2() );
      direction                              = direction.Unit();
      ROOT::Math::XYZVector      zVec( 0, 0, 1 );
      ROOT::Math::XYZVector      norm = direction.Cross( zVec );
      const ROOT::Math::XYZPoint g3   = g1 + 0.5 * ( g2 - g1 );
      const ROOT::Math::XYZPoint g4   = g3 + activeWidth * norm;

      const ROOT::Math::XYZVector vectorlayer = ( g4 - g3 ).unit() * this->access()->m_pitch;
      const ROOT::Math::XYZPoint  p0          = g3 - 0.5 * m_stripLength * direction;
      m_dxdy                                = direction.x() / direction.y();
      m_dzdy                                = direction.z() / direction.y();
      m_dy                                  = m_stripLength * direction.y();
      m_dp0di.SetX( vectorlayer.x() - vectorlayer.y() * m_dxdy );
      m_dp0di.SetY( vectorlayer.y() );
      m_dp0di.SetZ( vectorlayer.z() - vectorlayer.y() * m_dzdy );
      m_p0.SetX( p0.x() - p0.y() * m_dxdy );
      m_p0.SetY( p0.y() );
      m_p0.SetZ( p0.z() - p0.y() * m_dzdy );
      ROOT::Math::XYZVector dir = direction;
      if ( dir.y() < 0 ) dir *= -1;
      m_angle    = atan2( -dir.x(), dir.y() );
      m_cosAngle = cos( m_angle );
      m_sinAngle = sin( m_angle );
      }
    */
  }

} // namespace LHCb::Detector::UT
