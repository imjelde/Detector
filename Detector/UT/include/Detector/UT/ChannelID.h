/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cassert>
#include <ostream>
#include <type_traits>

namespace LHCb::Detector::UT {

  /**
   * Channel ID for class for UT
   * @author A Beiter (based on code by M Needham, J. Wang)
   */
  class ChannelID final {

    /// Bitmasks for UT bitfield that are different from TT/IT
    enum struct Mask : unsigned {
      strip = 0x3ff, // FIXME: maxstrip seems to be 512, so why not 0x1ff, but then what is the meaning of the bit
                     // corresponding to 0x200 ?
      sector          = 0x1fc00,
      detRegion       = 0x60000,
      layer           = 0x180000,
      station         = 0x600000,
      type            = 0x1800000,
      uniqueLayer     = station | layer,
      uniqueDetRegion = uniqueLayer | detRegion,
      uniqueSector    = uniqueDetRegion | sector
    };

    template <Mask m>
    [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      return ( i & static_cast<unsigned int>( m ) ) >> b;
    }

    template <Mask m>
    [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      auto v = ( i << static_cast<unsigned int>( b ) );
      assert( extract<m>( v ) == i );
      return v;
    }

  public:
    /// types of sub-detector channel ID
    enum detType { typeUT = 2 };

    /// Default Constructor
    constexpr ChannelID() = default;

    /// constructor with int
    constexpr explicit ChannelID( unsigned int id ) : m_channelID( id ) {}

    /// constructor with station, layer, detRegion, sector , strip,
    ChannelID( const unsigned int iType, const unsigned int iStation, const unsigned int iLayer,
               const unsigned int iDetRegion, const unsigned int iSector, const unsigned int iStrip )
        : ChannelID{shift<Mask::type>( iType ) | shift<Mask::station>( iStation ) | shift<Mask::layer>( iLayer ) |
                    shift<Mask::detRegion>( iDetRegion ) | shift<Mask::sector>( iSector ) |
                    shift<Mask::strip>( iStrip )} {}

    /// cast
    constexpr operator unsigned int() const { return m_channelID; }

    /// Retrieve type
    [[nodiscard]] constexpr unsigned int type() const { return extract<Mask::type>( m_channelID ); }

    /// test whether UT or not
    [[nodiscard]] constexpr bool isUT() const { return type() == ChannelID::detType::typeUT; }

    /// Retrieve sector
    [[nodiscard]] constexpr unsigned int sector() const { return extract<Mask::sector>( m_channelID ); }

    /// Retrieve detRegion
    [[nodiscard]] constexpr unsigned int detRegion() const { return extract<Mask::detRegion>( m_channelID ); }

    /// Retrieve layer
    [[nodiscard]] constexpr unsigned int layer() const { return extract<Mask::layer>( m_channelID ); }

    /// Retrieve unique layer
    [[nodiscard]] constexpr unsigned int uniqueLayer() const { return extract<Mask::uniqueLayer>( m_channelID ); }

    /// Retrieve unique detRegion
    [[nodiscard]] constexpr unsigned int uniqueDetRegion() const {
      return extract<Mask::uniqueDetRegion>( m_channelID );
    }

    /// Print method for python NOT NEEDED + SLOW IN C++ use operator<<
    [[nodiscard]] std::string toString() const;

    /// Retrieve const  UT Channel ID
    [[nodiscard]] constexpr unsigned int channelID() const { return m_channelID; }

    /// Update  UT Channel ID
    [[deprecated]] constexpr ChannelID& setChannelID( unsigned int value ) {
      m_channelID = value;
      return *this;
    }

    /// Retrieve strip
    [[nodiscard]] constexpr unsigned int strip() const { return extract<Mask::strip>( m_channelID ); }

    /// Retrieve station
    [[nodiscard]] constexpr unsigned int station() const { return extract<Mask::station>( m_channelID ); }

    /// Retrieve unique sector
    [[nodiscard]] constexpr unsigned int uniqueSector() const { return extract<Mask::uniqueSector>( m_channelID ); }

    friend std::ostream& operator<<( std::ostream& str, const ChannelID& obj );

  private:
    unsigned int m_channelID{0}; ///< UT Channel ID

  }; // class ChannelID

} // namespace LHCb::Detector::UT
