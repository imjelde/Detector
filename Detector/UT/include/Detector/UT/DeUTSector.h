/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Core/LineTraj.h"
#include "Detector/UT/ChannelID.h"
#include "Detector/UT/DeUTSensor.h"

#include <array>
#include <vector>

namespace LHCb::Detector::UT {

  namespace detail {

    struct DeUTSectorObject : LHCb::Detector::detail::DeIOVObject {
      DeUTSectorObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
      std::vector<DeUTSensorObject> m_sensors;

      double       m_pitch;
      unsigned int m_nStrip;
      double       m_deadWidth;
      unsigned int m_firstStrip{0};
      bool         m_stripflip;
      double       m_version{1};
      double       m_capacitance;
      bool         m_xInverted;
      bool         m_yInverted;
      ChannelID    m_channelID;

      double                m_stripLength;
      double                m_dxdy;
      double                m_dzdy;
      double                m_dy;
      ROOT::Math::XYZVector m_dp0di;
      ROOT::Math::XYZPoint  m_p0;
      double                m_angle;
      double                m_cosAngle;
      double                m_sinAngle;
    };
  } // End namespace detail

  enum class Status {
    OK              = 0,
    Open            = 1,
    Short           = 2,
    Pinhole         = 3,
    ReadoutProblems = 4,
    NotBonded       = 5,
    LowGain         = 6,
    Noisy           = 7,
    OtherFault      = 9,
    Dead            = 10,
    UnknownStatus   = 100
  };

  static const std::map<Status, std::string> s_map = {{Status::OK, "OK"},
                                                      {Status::Open, "Open"},
                                                      {Status::Short, "Short"},
                                                      {Status::Pinhole, "Pinhole"},
                                                      {Status::NotBonded, "NotBonded"},
                                                      {Status::LowGain, "LowGain"},
                                                      {Status::Noisy, "Noisy"},
                                                      {Status::ReadoutProblems, "ReadoutProblems"},
                                                      {Status::OtherFault, "OtherFault"},
                                                      {Status::Dead, "Dead"},
                                                      {Status::UnknownStatus, "Unknown"}};

  std::string          toString( Status status );
  Status               toStatus( std::string_view str );
  inline std::ostream& operator<<( std::ostream& os, Status const& s ) { return os << s_map.at( s ); }
  inline std::ostream& toStream( Status status, std::ostream& os ) {
    return os << std::quoted( toString( status ), '\'' );
  }

  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  template <typename ObjectType>
  class DeUTSectorElement : public DeIOVElement<ObjectType> {
  private:
    auto const& sensors() const { return this->access()->m_sensors; }

  public:
    using DeIOVElement<ObjectType>::DeIOVElement;

    unsigned int firstStrip() const { return this->access()->m_firstStrip; }
    unsigned int nStrip() const { return this->access()->m_nStrip; }
    bool         stripflip() { return this->access()->m_stripflip; }
    double       thickness() const { throw "Not implemented"; }

    DeUTSensor   sensor( unsigned int index ) const { return &this->access()->m_sensors[index]; }
    unsigned int nSensors() const { return sensors().size(); }
    bool         xInverted() const { throw "Not implemented"; }
    bool         yInverted() const { throw "Not implemented"; }
    ChannelID    channelID() const { return this->access()->m_channelID; }

    LineTraj<double> createTraj( unsigned int, double ) const {
      /*auto const& theSensors = sensors();
      if ( theSensors.size() != 1 ) {
        // throw out error;
      }
      return theSensors.front()->trajectory( strip, offset );*/
      throw "Not implemented";
    }
    LineTraj<double> trajectoryFirstStrip() const { return createTraj( firstStrip(), 0. ); }
    LineTraj<double> trajectoryLastStrip() const { return createTraj( nStrip(), 0. ); }

    ChannelID                 elementID() const { throw "Not implemented"; }
    unsigned int              column() const { throw "Not implemented"; }
    unsigned int              row() const { throw "Not implemented"; }
    unsigned int              id() const { throw "Not implemented"; }
    std::string const&        type() const { throw "Not implemented"; }
    ROOT::Math::XYZPoint      globalCentre() const { throw "Not implemented"; }
    std::pair<double, double> halfLengths() const { throw "Not implemented"; }
    const std::string&        nickname() const { throw "Not implemented"; }

    /// beetle corresponding to channel  1-3 (IT) 1-4 (TT)
    unsigned int beetle( const ChannelID& ) const { throw "Not implemented"; }

    ROOT::Math::XYZVector normalY() { throw "Not implemented"; }
    double                pitch() const { throw "Not implemented"; }

    /*
    // might no need anymore
    std::string staveNumber( unsigned int reg, unsigned int station ) const {
      int           col                      = 0;
      constexpr int num_stave_in_UTaReg1and3 = 6;
      constexpr int num_stave_in_UTaReg2     = 4; // number of staves in Region 1,2,3 of UTa
      constexpr int num_stave_in_UTbReg1and3 = 7;
      constexpr int num_stave_in_UTbReg2     = 4; // number of staves in Region 1,2,3 of UTb

      // UTaX or UTaU
      if ( station == 1 ) {
        switch ( reg ) {
        case 1:
          col = column();
          break;
        case 2:
          col = column() - num_stave_in_UTaReg1and3;
          break;
        case 3:
          col = column() - num_stave_in_UTaReg1and3 - num_stave_in_UTaReg2;
          break;
        }
      } else if ( station == 2 ) { // UTbV or UTbX
        switch ( reg ) {
        case 1:
          col = column();
          break;
        case 2:
          col = column() - num_stave_in_UTbReg1and3;
          break;
        case 3:
          col = column() - num_stave_in_UTbReg1and3 - num_stave_in_UTbReg2;
          break;
        }
      }
      return std::to_string( col );
    }
    */
    std::string m_conditionPathName = "..to be filled";
    std::string conditionsPathName() const { return m_conditionPathName; }

    void setStatusCondition( const std::string&, unsigned int, const Status& ) { throw "Not implemented"; }
    /// Status of channel
    Status stripStatus( ChannelID ) const { throw "Not implemented"; }
    void   setStripStatus( unsigned int, const Status& ) { throw "Not implemented"; }
    void   setBeetleStatus( unsigned int, const Status& ) { throw "Not implemented"; }
    void   setSectorStatus( const Status& ) { throw "Not implemented"; }
    Status sectorStatus() const { throw "Not implemented"; }
    /// measured efficiency
    double measEff() const { throw "Not implemented"; }
    void   setMeasEff( const double ) { throw "Not implemented"; }
    double fractionActive() const { throw "Not implemented"; }
    bool   globalInBondGap( const ROOT::Math::XYZPoint&, double ) const { throw "Not implemented"; }
    bool   globalInActive( const ROOT::Math::XYZPoint&, ROOT::Math::XYZPoint = {0., 0., 0.} ) const {
      throw "Not implemented";
    }
    const std::optional<DeUTSensor> findSensor( const ROOT::Math::XYZPoint& ) const {
      // return pointer to the layer from point
      /*auto iter = std::find_if( sensors().begin(), sensors().end(),
                                [&point]( const DeUTSensorObject& s ) { return s.isInside( point ); } );
      if ( iter != sensors().end() ) return DeUTSensor(&*iter);
      return {};*/
      throw "Not implemented";
    }

    /// apply given callable to all sensors
    void applyToAllSensors( const std::function<void( DeUTSensor const& )>& ) const { throw "Not implemented"; }

    bool        updateNoiseCondition() { throw "Not implemented"; }
    bool        updateStatusCondition() { throw "Not implemented"; }
    bool        registerConditionsCallbacks() { throw "Not implemented"; }
    inline bool isStrip( const unsigned int strip ) const {
      return strip >= firstStrip() && strip < firstStrip() + nStrip();
    }
    inline bool contains( const ChannelID aChannel ) const {
      return aChannel.uniqueSector() == channelID().uniqueSector();
    }
    ChannelID nextLeft( const ChannelID testChan ) const {
      if ( ( contains( testChan ) ) && ( testChan.strip() > 0 ) && ( isStrip( testChan.strip() - 1u ) ) ) {
        return ChannelID( testChan.channelID() - 1u );
      } else {
        return ChannelID( 0u );
      }
    }
    ChannelID nextRight( const ChannelID testChan ) const {
      if ( ( contains( testChan ) ) && ( isStrip( testChan.strip() + 1u ) ) ) {
        return ChannelID( testChan.channelID() + 1u );
      } else {
        return ChannelID( 0u );
      }
    }
    LineTraj<double> trajectory( ChannelID aChan, double offset ) const {
      if ( !contains( aChan ) ) {
        // MsgStream msg( msgSvc(), name() );
        // msg << MSG::ERROR << "Failed to link to sector " << nickname() << " test strip  number " << aChan.strip()
        // << " strip " << endmsg;
        // throw GaudiException( "Failed to make trajectory", "DeUTSector.cpp", StatusCode::FAILURE );
        // throw error;
      }
      return createTraj( aChan.strip(), offset );
    }
    double toElectron( double, unsigned int ) const { throw "Not implemented"; }
    /*double toElectron( double val, ChannelID aChannel ) const {
      return toElectron( val, aChannel.template <ChannelID::strip>() );
      }*/
    double toADC( double, unsigned int ) const { throw "Not implemented"; }
    /*double toADC( double e, ChannelID aChannel ) const {
      return toADC( e, aChannel.template <ChannelID::strip>() );
      }*/
    float cmPortNoise( unsigned int, unsigned int ) const { throw "Not implemented"; }
    float cmBeetleNoise( unsigned int ) const { throw "Not implemented"; }
    float cmSectorNoise() const { throw "Not implemented"; }
    float cmNoise( ChannelID ) const { throw "Not implemented"; }
    void  setADCConversion( std::vector<double> ) { throw "Not implemented"; }
    void  setCMNoise( std::vector<double> ) { throw "Not implemented"; }
    void  setCMNoise( unsigned int, double ) { throw "Not implemented"; }
    void  setNoise( std::vector<double> ) { throw "Not implemented"; }
    void  setNoise( unsigned int, double ) { throw "Not implemented"; }
    float portNoise( unsigned int, unsigned int ) const { throw "Not implemented"; }
    float beetleNoise( unsigned int ) const { throw "Not implemented"; }
    float sectorNoise() const { throw "Not implemented"; }
    float rawSectorNoise() const {
      return std::sqrt( sectorNoise() * sectorNoise() + cmSectorNoise() * cmSectorNoise() );
    }
    float rawBeetleNoise( unsigned int beetle ) const {
      return std::sqrt( beetleNoise( beetle ) * beetleNoise( beetle ) +
                        cmBeetleNoise( beetle ) * cmBeetleNoise( beetle ) );
    }
    float rawPortNoise( unsigned int beetle, unsigned int port ) const {
      return std::sqrt( portNoise( beetle, port ) * portNoise( beetle, port ) +
                        cmPortNoise( beetle, port ) * cmPortNoise( beetle, port ) );
    }
    float rawNoise( ChannelID aChannel ) const {
      return std::sqrt( noise( aChannel ) * noise( aChannel ) + cmNoise( aChannel ) * cmNoise( aChannel ) );
    }
    float       noise( ChannelID ) const { throw "Not implemented"; }
    std::string m_hybridType;
    std::string hybridType() const { return m_hybridType; }

    ChannelID stripToChan( unsigned int ) const { throw "Not implemented"; }
    bool      isOKStrip( ChannelID ) const { throw "Not implemented"; }
    void      trajectory( unsigned int, double, double&, double&, double&, double&, double&, double& ) const {
      throw "Not implemented";
    }
    double                cosAngle() const { throw "Not implemented"; }
    double                capacitance() const { throw "Not implemented"; }
    ROOT::Math::XYZPoint  get_p0() const { throw( "Not implemented" ); }
    ROOT::Math::XYZVector get_dp0di() const { throw( "Not implemented" ); }
    double                get_dy() const { throw( "Not implemented" ); }
    bool                  getStripflip() const { throw( "Not implemented" ); }

    friend std::ostream& operator<<( std::ostream&, const DeUTSectorElement& ) { throw "Not implemented"; }
  };

  using DeUTSector = DeUTSectorElement<detail::DeUTSectorObject>;

} // namespace LHCb::Detector::UT
