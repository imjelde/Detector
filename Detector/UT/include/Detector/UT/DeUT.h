/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/LineTraj.h"
#include "Detector/UT/ChannelID.h"
#include "Detector/UT/DeUTSide.h"

#include "Core/DeIOV.h"

#include "fmt/format.h"

#include <array>
#include <vector>

namespace LHCb::Detector::UT {

  namespace detail {

    struct DeUTObject : LHCb::Detector::detail::DeIOVObject {
      DeUTObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
      std::array<DeUTSideObject, 2> sides; // A side at index 0, C side at index 1;
    };
  } // End namespace detail

  inline std::string const  Location = "/world/BeforeMagnetRegion/UT";
  inline std::string const& location() { return Location; }

  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  template <typename ObjectType>
  struct DeUTElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;

    /** Implementation of sensitive volume identifier for a given point in the
        global reference frame. This is the sensor number defined in the xml.
    */
    int sensitiveVolumeID( const ROOT::Math::XYZPoint& ) const { throw( "Not implemented" ); }

    /// locate layer based on a point
    const std::optional<DeUTLayer> findLayer( const ROOT::Math::XYZPoint& ) const { throw( "Not implemented" ); }

    /// check contains channel
    bool contains( ChannelID ) const { throw( "Not implemented" ); }

    /// Workaround to prevent hidden base class function
    bool isValid() const { throw( "Not implemented" ); }

    /// check channel number is valid
    bool isValid( ChannelID ) const { throw( "Not implemented" ); }

    /// flat vector of sectors
    const DeUTSector& sector( unsigned int ) const { throw( "Not implemented" ); }

    /// @return number of sectors
    unsigned int nSectors() const { throw( "Not implemented" ); }

    /// apply given callable to all sectors
    void applyToAllSectors( const std::function<void( DeUTSector const& )>& ) const { throw( "Not implemented" ); }

    /// check no sector returns true for the given callable
    bool none_of_sectors( const std::function<bool( DeUTSector const& )>& ) const { throw( "Not implemented" ); }

    /**
     * access to a given layer
     * Note that there is no checks on the validity of the index.
     * use at your own risks
     */
    const DeUTLayer& layer( unsigned int ) const { throw( "Not implemented" ); }

    /// apply given callable to all layers
    void applyToAllLayers( const std::function<void( DeUTLayer const& )>& ) const { throw( "Not implemented" ); }

    /// short cut to pick up the wafer corresponding to a channel
    const std::optional<DeUTSector> findSector( ChannelID ) const { throw( "Not implemented" ); }

    const std::optional<DeUTSector> findSector( const ROOT::Math::XYZPoint& ) const { throw( "Not implemented" ); }

    /// get the sector corresponding to the input channel
    const DeUTSector& getSector( ChannelID ) const {
      throw( "Not implemented" );
      /*return getSector( chan.template <ChannelID::side>(), chan.template <ChannelID::halflayer>(),
                        chan.template <ChannelID::stave>(), chan.template <ChannelID::face>(),
                        chan.template <ChannelID::module>(), chan.template <ChannelID::subsector>() );
      */
    }

    /// get the next channel left
    ChannelID nextLeft( ChannelID ) const { throw( "Not implemented" ); }

    /// get the next channel right
    ChannelID nextRight( ChannelID ) const { throw( "Not implemented" ); }

    /// get the trajectory
    // LHCb::LineTraj<double> trajectory( LHCb::LHCbID id, double offset ) const { return {}; }

    /// get the number of strips in detector
    unsigned int nStrip() const { throw( "Not implemented" ); }

    /// get the number of layers
    unsigned int nLayer() const { throw( "Not implemented" ); }

    /// get the number of readout sectors
    unsigned int nReadoutSector() const { throw( "Not implemented" ); }

    /// number of layers per station
    unsigned int nLayersPerStation() const { throw( "Not implemented" ); }

    /// fraction active channels
    double fractionActive() const { throw( "Not implemented" ); };

    void setOffset(){};

    /// get list of all disabled sectors
    std::vector<DeUTSector> disabledSectors() const { throw( "Not implemented" ); }

    /// get list of disabled beetles
    std::vector<ChannelID> disabledBeetles() const { throw( "Not implemented" ); }

    /// beetle as a string
    std::string uniqueBeetle( const ChannelID& chan ) const {
      auto const& sector = findSector( chan );
      return fmt::format( "{}Beetle{}", sector->nickname(), sector->beetle( chan ) );
    };

    /// port
    std::string uniquePort( const ChannelID& ) const {
      // const unsigned int port = ( ( chan.strip() - 1u ) / LHCbConstants::nStripsInPort ) + 1u;
      // return fmt::format( "{}Port{}", uniqueBeetle( chan ), port );
      throw( "Not implemented" );
    };

    inline unsigned int firstStation() const { throw( "Not implemented" ); }
    void applyToSectors( const std::vector<ChannelID>&, const std::function<void( DeUTSector const& )>& ) const {
      throw( "Not implemented" );
    }

    std::optional<DeUTLayer> findLayer( ChannelID ) const {
      throw( "Not implemented" );
      /*auto iter = std::find_if( m_layers.begin(), m_layers.end(),
                                [&]( const DeUTLayer* l ) { return l->contains( aChannel ); } );
                                return iter != m_layers.end() ? *iter : nullptr;*/
    }
    std::optional<DeUTSide> findStation( ChannelID ) const { throw( "Not implemented" ); }

    const DeUTSide& station( unsigned int ) const { throw( "Not implemented" ); }
    unsigned int    nStation() const { throw( "Not implemented" ); }

#if 0
    using Side   = DeUTSide<ChannelID, LineTraj>;
    using Layer  = DeUTLayer<ChannelID, LineTraj>;
    using Stave  = DeUTStave<ChannelID, LineTraj>;
    using Face   = DeUTFace<ChannelID, LineTraj>;
    using Module = DeUTModule<ChannelID, LineTraj>;
    using Sector = DeUTSector<ChannelID, LineTraj>;
    using Sensor = DeUTSensor<ChannelID, LineTraj>;

    using Sectors = std::vector<const DeUTSector*>;
    using Layers  = std::vector<const DeUTLayer*>;
    using Modules = std::vector<const DeUTModule*>;
    struct LayerGeom {
      float z;
      int   nColsPerSide;
      int   nRowsPerSide;
      float invHalfSectorYSize;
      float invHalfSectorXSize;
      float dxDy;
    };
    Layers m_layers = (&)[] {
      for ( auto ptrside : m_sides ) {
        for ( auto ptrlayer : dynamic_cast<const DeUTSide*>( ptrside )->layers() ) { m_layers.push_back( ptrlayer ); }
      }
    }
    ();
    Sectors m_sectors = (&)[] {
      for ( auto ptrlayer : m_layers ) {
        const auto& vecptrsectors = ptrlayer->sectors();
        m_sectors.insert( m_sectors.end(), vecptrsectors.begin(), vecptrsectors.end() );
      }
    }
    ();
    // For new Geo no need for DeUTDetector::setOffset
    unsigned int              m_nStrip = m_sectors.front()->nStrip() * m_sectors.size();
    unsigned int              nStrip() const { return m_nStrip; }
    inline const std::string& location() { return ( ""; /*DeUTDetLocation::UT*/ ); }
    // inline unsigned int DeUTDetLocation::detType() { return ( ChannelID::detType::typeUT ); }
    inline unsigned int detType() { return ( template UTChannelID::detType::typeUT ); }
    inline unsigned int firstStation() const { return m_firstSide; }
    inline unsigned int lastStation() const { return m_firstSide + m_sides.size() - 1u; }
    inline unsigned int firstSide() const { return m_firstSide; }
    inline unsigned int lastSide() const { return m_firstSide + m_sides.size() - 1u; }
    inline unsigned int nSide() const { return m_sides.size(); }
    inline bool         contains( const ChannelID aChannel ) const {
      return aChannel.template <ChannelID::side>
             () < lastSide();
    }
    unsigned int                              m_version = 1u; // Suppose always be 1 in new Geo
    std::map<unsigned int, const DeUTSector*> m_sMap    = (&)[] {
      std::map<unsigned int, const DeUTSector*> t_sMap;
      for ( auto i : m_sectors ) { m_sMap.insert( i->m_elementID.template <ChannelID::uniqueSector>(), i ); }
      return t_sMap;
    }
    ();
    unsigned int m_firstSide = 0u;
    void         setFirstSide( const unsigned int iSide ) { m_firstSide = iSide; } // Actually no need!
    double       fractionActive() const {
      return std::accumulate( m_sectors.begin(), m_sectors.end(), 0.0,
                              [&]( double fA, const DeUTSector* s ) { return fA + s->fractionActive(); } ) /
             double( m_sectors.size() );
    }
    const DeUTSide* findSide( const ChannelID aChannel ) const {
      auto iter =
          std::find_if( m_sides.begin(), m_sides.end(), [&]( const DeUTSide* s ) { return s->contains( aChannel ); } );
      return iter != m_sides.end() ? *iter : nullptr;
    }
    const DeUTSide* findSide( const ROOT::Math::XYZPoint& point ) const {
      auto iter =
          std::find_if( m_sides.begin(), m_sides.end(), [&]( const DeUTSide* s ) { return s->isInside( point ); } );
      return iter != m_sides.end() ? *iter : nullptr;
    }
    const DeUTLayer* findLayer( const ROOT::Math::XYZPoint& point ) const {
      auto iter =
          std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->isInside( point ); } );
      return iter != m_layers.end() ? *iter : nullptr;
    }
#  if 0 // not sure we still need this or not
    const DeUTSector* findSector( std::string_view nickname ) const {
	   // return pointer to the sector from the nickname
	   auto iter = std::find_if( m_sectors.begin(), m_sectors.end(), UTDetFun::equal_by_name( nickname ) );
	     return iter != m_sectors.end() ? *iter : nullptr;
    }
#  endif
    const DeUTSector* findSector( const ROOT::Math::XYZPoint& aPoint ) const {
      const DeUTSector* aSector = nullptr;
      const DeUTSide*   tSide   = findSide( aPoint );
      if ( tSide ) {
        const DeUTSide*  aSide  = dynamic_cast<const DeUTSide*>( tSide );
        const DeUTLayer* aLayer = aSide->findLayer( aPoint );
        if ( aLayer ) {
          const DeUTStave* aStave = aLayer->findStave( aPoint );
          if ( aStave ) { aSector = aStave->findSector( aPoint ); }
        }
      }
      return aSector;
    }
    const DeUTSector* findSector( const ChannelID aChannel ) const {
      auto iter = std::find_if( m_sectors.begin(), m_sectors.end(),
                                [&]( const DeUTSector* l ) { return l->contains( aChannel ); } );
      return iter != m_sectors.end() ? *iter : nullptr;
    }
    int sensitiveVolumeID( const ROOT::Math::XYZPoint& point ) const {
      const DeUTSector* sector = findSector( point );
      if ( !sector ) {
        // MsgStream msg( msgSvc(), name() );
        // msg << MSG::ERROR << "sensitiveVolumeID: no sensitive volume at " << point << endmsg;
        // throw out error report
        return -1;
      }
      return sector->m_elementID;
    }
    const Sectors& sectors() const { return m_sectors; }
    Sectors        disabledSectors() const {
      Sectors     disabled;
      const auto& vec = sectors();
      std::copy_if( vec.begin(), vec.end(), std::back_inserter( disabled ),
                    []( const DeUTSector* s ) { return s->sectorStatus() == DeUTSector::ReadoutProblems; } );
      return disabled;
    }
    std::vector<ChannelID> disabledBeetles() const {
      std::vector<ChannelID> disabledBeetles;
      for ( auto s : sectors() ) {
        auto bStatus = s->beetleStatus();
        for ( unsigned int i = 0; i < bStatus.size(); ++i ) {
          if ( bStatus[i] == DeUTSector::ReadoutProblems ) {
            const unsigned int firstStripOnBeetle = ( i * LHCbConstants::nStripsInBeetle ) + 1;
            disabledBeetles.push_back( s->stripToChan( firstStripOnBeetle ) );
          }
        } // i
      }
      return disabledBeetles;
    }
    LineTraj trajectory( const LHCbID& id, const double offset ) const {
      if ( !id.isUT() ) {
        // throw GaudiException( "The LHCbID is not of UT type!", "DeUTDetector.cpp", StatusCode::FAILURE );
        // should throw but not in Gaudi...
      }
      const DeUTSector* aSector = findSector( id.utID() );
      if ( !aSector ) { /*throw GaudiException( "Failed to find sector", "DeUTDetector.cpp", StatusCode::FAILURE );*/
      }
      return aSector->trajectory( id.template <LHCbID::utID>(), offset );
    }
    LineTraj trajectoryFirstStrip( const LHCbID& id ) const {
      if ( !id.isUT() ) {
        // throw GaudiException( "The LHCbID is not of UT type!", "DeUTDetector.cpp", StatusCode::FAILURE );
        // should throw but not in Gaudi...
      }
      const DeUTSector* aSector = findSector( id.template <LHCbID::utID>() );
      if ( !aSector ) { /*throw GaudiException( "Failed to find sector", "DeUTDetector.cpp", StatusCode::FAILURE );*/
      }
      return aSector->trajectoryFirstStrip();
    }

    LineTraj trajectoryLastStrip( const LHCbID& id ) const {
      if ( !id.template <LHCbID::isUT>() ) {
        // throw GaudiException( "The LHCbID is not of UT type!", "DeUTDetector.cpp", StatusCode::FAILURE );
        // should throw but not in Gaudi...
      }
      const DeUTSector* aSector = findSector( id.template <LHCbID::utID>() );
      if ( !aSector ) { /*throw GaudiException( "Failed to find sector", "DeUTDetector.cpp", StatusCode::FAILURE ); */
      }
      return aSector->trajectoryLastStrip();
    }

    double fractionActive() const {
      return std::accumulate( m_sectors.begin(), m_sectors.end(), 0.0,
                              [&]( double fA, const DeUTSector* s ) { return fA + s->fractionActive(); } ) /
             double( m_sectors.size() );
    }
    std::map<unsigned int, unsigned int> m_STCBmap  = {{0, 0}, {2, 1}, {4, 2},  {5, 3},  {6, 4},  {7, 5},
                                                      {8, 6}, {9, 7}, {10, 8}, {12, 9}, {14, 10}};
    std::map<unsigned int, unsigned int> m_STCFmap  = {{0, 0}, {2, 1},  {4, 2},  {6, 3},  {7, 4},  {8, 5},
                                                      {9, 6}, {10, 7}, {11, 8}, {12, 9}, {14, 10}};
    std::map<unsigned int, unsigned int> m_STBBmap  = {{0, 0}, {2, 1},  {4, 2},  {5, 3}, {8, 4},
                                                      {9, 5}, {10, 6}, {12, 7}, {14, 8}};
    std::map<unsigned int, unsigned int> m_STBFmap  = {{0, 0},  {2, 1},  {4, 2},  {6, 3}, {7, 4},
                                                      {10, 5}, {11, 6}, {12, 7}, {14, 8}};
    std::map<unsigned int, unsigned int> m_STABmap  = {{0, 0}, {2, 1}, {4, 2}, {8, 3}, {10, 4}, {12, 5}, {14, 6}};
    std::map<unsigned int, unsigned int> m_STAFmap  = {{0, 0}, {2, 1}, {4, 2}, {6, 3}, {10, 4}, {12, 5}, {14, 6}};
    inline static const unsigned int     NTOTSECTOR = 1048;
    inline static const unsigned int     NSECTSTA   = 124;
    inline static const unsigned int     NSECTSTB   = 138;
    inline static const unsigned int     NSECTSTVC  = 22;
    inline static const unsigned int     NSECTSTVB  = 18;
    inline static const unsigned int     NSECTSTVA  = 14;
    const DeUTSector*                    getSector( unsigned int side,   //
                                                    unsigned int layer,  //
                                                    unsigned int stave,  //
                                                    unsigned int face,   //
                                                    unsigned int module, //
                                                    unsigned int sector ) const {

      const DeUTSector* res = nullptr;

      if ( m_version != ChannelID::IDType::v1 ) return res;
      if ( side < NBSIDE && layer < NBHALFLAYER && stave < NBSTAVE && face < NBFACE && module < NBMODULE &&
           sector < NBSUBSECTOR ) {
        const unsigned int id_Layer =
            side * NTOTSECTOR / 2 + ( ( layer < 2 ) ? layer * NSECTSTA : 2 * NSECTSTA + ( layer - 2 ) * NSECTSTB );
        const unsigned int id_Stave = ( ( stave > 0 ) ? NSECTSTVC : 0 ) + ( ( stave > 1 ) ? NSECTSTVB : 0 ) +
                                      ( ( stave > 2 ) ? ( stave - 2 ) * NSECTSTVA : 0 );
        const unsigned int id_Face = ( ( stave == 0 ) ? ( ( face + side ) % 2 ) * ( NSECTSTVC / 2 ) : 0 ) +
                                     ( ( stave == 1 ) ? ( ( face + side ) % 2 ) * ( NSECTSTVB / 2 ) : 0 ) +
                                     ( ( stave > 1 ) ? ( ( face + side ) % 2 ) * ( NSECTSTVA / 2 ) : 0 );
        const unsigned int id_Module = 2 * module + sector;
        unsigned int       id_Sector;
        if ( side == 0 ) {
          if ( face == 0 ) {
            if ( stave == 0 )
              id_Sector = m_STCBmap.find( id_Module )->second;
            else if ( stave == 1 )
              id_Sector = m_STBBmap.find( id_Module )->second;
            else
              id_Sector = m_STABmap.find( id_Module )->second;
          } else {
            if ( stave == 0 )
              id_Sector = m_STCFmap.find( id_Module )->second;
            else if ( stave == 1 )
              id_Sector = m_STBFmap.find( id_Module )->second;
            else
              id_Sector = m_STAFmap.find( id_Module )->second;
          }
        } else {
          if ( face == 1 ) {
            if ( stave == 0 )
              id_Sector = m_STCBmap.find( id_Module )->second;
            else if ( stave == 1 )
              id_Sector = m_STBBmap.find( id_Module )->second;
            else
              id_Sector = m_STABmap.find( id_Module )->second;
          } else {
            if ( stave == 0 )
              id_Sector = m_STCFmap.find( id_Module )->second;
            else if ( stave == 1 )
              id_Sector = m_STBFmap.find( id_Module )->second;
            else
              id_Sector = m_STAFmap.find( id_Module )->second;
          }
        }
        const auto i = id_Sector + id_Face + id_Stave + id_Layer;
        res          = ( i < m_sectors.size() ? m_sectors[i] : nullptr );
      }

      return res;
    }
    std::size_t          getOffset( const std::size_t index ) const noexcept; // no need for new Geo
    inline const Layers& layers() const { return m_layers; }
    inline unsigned int  nLayer() const { return layers().size(); }
    inline unsigned int  nReadoutSector() const { return sectors().size(); }
    inline unsigned int  nLayersPerStation() const { return 0; }
    inline unsigned int  nLayersPerSide() const { return nLayer() / nSide(); }
    inline void          setNstrip( const unsigned int nStrip ) { m_nStrip = nStrip; }
    inline const Sides&  sides() const { return m_sides; }
    inline const Sectors sectorsinLayer( unsigned int layerID ) const {
      const DeUTLayer* layerC   = m_layers[layerID];
      Sectors          tsectors = layerC->sectors();
      const DeUTLayer* layerA   = m_layers[layerID + 4];
      tsectors.insert( tsectors.end(), layerA->sectors().begin(), layerA->sectors().end() );
      return tsectors;
    }
    // still need? always true for new Geo
    inline bool SectorsSwapped() const {
      float xPos24( 0.0f ), xPos25( 0.0f );
      auto  SectorsInLayer    = sectorsinLayer( 0 );
      xPos24                  = SectorsInLayer[107]->toGlobal( {0, 0, 0} ).X();
         xPos25               = SectorsInLayer[108]->toGlobal( {0, 0, 0}().X();
	 if ( m_version == ChannelID::IDType::v1 ) {
        return true;
	 } else {
        if ( xPos24 < xPos25 )
          return true;
        else
          return false;
	 }
    }
    inline double zStation( unsigned int stationid ) const {
      if ( stationid > 2 ) throw std::runtime_error( "wrong stationID in DeUTDetector::zStation" );
      return ( m_sides[0]->layers()[stationid * 2]->toGlobal( {0, 0, 0} ).z() +
               m_sides[0]->layers()[stationid * 2 + 1]->toGlobal( {0, 0, 0} ).z() ) /
             2;
    }
    inline bool            layerSizeOK() const { return ( m_layers.size() == 8 ); }
    inline const LayerGeom getLayerGeom( unsigned int layerid ) const {
      LayerGeom    layergoem;
      auto         SectorsInLayer = sectorsinLayer( layerid );
      auto         tStation       = layerid / 2;
      unsigned int keysectorID    = 0;
      if ( m_version == ChannelID::IDType::v1 ) { keysectorID = ( tStation == 0 ) ? 31 : 11; }
      layergoem.z = SectorsInLayer[keysectorID]->globalCentre().z();

      float        YFirstRow         = std::numeric_limits<float>::max();
      float        YLastRow          = std::numeric_limits<float>::lowest();
      float        smallestXLastCol  = std::numeric_limits<float>::max();
      float        smallestXFirstcol = std::numeric_limits<float>::max();
      float        biggestXFirstCol  = std::numeric_limits<float>::lowest();
      unsigned int biggestColumn     = ( tStation == 0 ) ? 16u : 18u;
      unsigned int smallestColumn    = 1u;
      unsigned int topMostRow        = 14u;
      unsigned int bottomMostRow     = 1u;
      // Second pass
      // find x and y values in the corners to deduce the geometry of the layer
      for ( const auto& sector : SectorsInLayer ) {
        // deal with x,y coordinates. Remember the corner coordinates
        const DeUTSector& utSector = dynamic_cast<const DeUTSector&>( *sector );
        auto              column   = utSector.column(); // should be defined in DeUTSector
        auto              row      = utSector.row();    // should be defined in DeUTSector
        auto              center   = sector->toGlobal( ROOT::Math::XYZPoint{0, 0, 0} ); // This one should be better?
        // auto              center   = sector->geometry()->toGlobal( Gaudi::XYZPoint{0, 0, 0} );
        if ( column == smallestColumn ) {
          if ( row == bottomMostRow ) {
            smallestXFirstcol = center.x();
            YFirstRow         = center.y();
          } else if ( row == topMostRow ) {
            biggestXFirstCol = center.x();
            YLastRow         = center.y();
          }
        }
        if ( column == biggestColumn && row == bottomMostRow ) { smallestXLastCol = center.x(); }
      }
      // gather all information into the corresponding LayerInfo object
      auto ncols                   = biggestColumn - smallestColumn + 1;
      auto nrows                   = topMostRow - bottomMostRow + 1;
      layergoem.nColsPerSide       = ncols / 2;
      layergoem.nRowsPerSide       = nrows / 2;
      layergoem.invHalfSectorYSize = 2 * ( nrows - 1 ) / ( YLastRow - YFirstRow );
      layergoem.invHalfSectorXSize = 2 * ( ncols - 1 ) / ( smallestXLastCol - smallestXFirstcol );
      layergoem.dxDy               = ( biggestXFirstCol - smallestXFirstcol ) / ( YLastRow - YFirstRow );

      return layergoem;
    }
    bool isValid() const override { return true; /*ValidDataObject::isValid();*/ }
    // bool isValid( const Gaudi::Time& t ) const override { return ValidDataObject::isValid( t ); } //what is
    // Gaudi::Time  in dd4hep
#endif
  };

  using DeUT = DeUTElement<detail::DeUTObject>;

} // namespace LHCb::Detector::UT
