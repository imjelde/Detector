/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/UT/ChannelID.h"
#include "Detector/UT/DeUTSector.h"
#include "Detector/UT/DeUTStave.h"

#include <array>
#include <vector>

namespace LHCb::Detector::UT {

  namespace detail {

    struct DeUTLayerObject : LHCb::Detector::detail::DeIOVObject {
      DeUTLayerObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );
      std::vector<DeUTStaveObject> m_staves;
      ChannelID                    ChanID;
      ROOT::Math::Plane3D          m_plane;
      double                       m_angle;
      double                       m_sinAngle;
      double                       m_cosAngle;
    };
  } // End namespace detail

  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  template <typename ObjectType>
  struct DeUTLayerElement : DeIOVElement<ObjectType> {

    using DeIOVElement<ObjectType>::DeIOVElement;

    auto const&         staves() const { return this->access()->m_staves; }
    ROOT::Math::Plane3D plane() const { return this->access()->m_plane; }

    /// detector element id - for experts only !
    ChannelID elementID() const { throw "Not implemednted"; }

    DeUTSector sector( unsigned int ) const { throw "Not implemednted"; }
    void       applyToAllSectors( const std::function<void( DeUTSector const& )>& ) const { throw "Not implemednted"; }

    auto   chanID() { return this->access()->ChanID; }
    double fractionActive() const {
      return std::accumulate( staves().begin(), staves().end(), 0.0,
                              []( double f, const DeUTStave m ) { return f + m.fractionActive(); } ) /
             double( staves().size() );
    }
    const DeUTStave findStave( const ChannelID aChannel ) const {
      auto iter =
          std::find_if( staves().begin(), staves().end(), [&]( const DeUTStave m ) { return m.contains( aChannel ); } );
      return iter != staves().end() ? *iter : nullptr;
    }
    const DeUTStave findStave( const ROOT::Math::XYZPoint& point ) const {
      auto iter =
          std::find_if( staves().begin(), staves().end(), [&]( const DeUTStave m ) { return m.isInside( point ); } );
      return iter != staves().end() ? *iter : nullptr;
    }

    bool contains( const ChannelID ) const { throw "Not implemednted"; }
    /*( chanID().side() == aChannel.side() &&
      chanID().layer() == aChannel.layer() );*/
    double sinAngle() const { return this->access()->m_sinAngle; }
    double cosAngle() const { return this->access()->m_cosAngle; }
    double angle() const { return this->access()->m_angle; }
  };

  using DeUTLayer = DeUTLayerElement<detail::DeUTLayerObject>;

  inline std::ostream& operator<<( std::ostream&, const DeUTLayer& ) { throw "Not implemednted"; }

} // namespace LHCb::Detector::UT
