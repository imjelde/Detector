/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"

#include "Detector/UT/ChannelID.h"
#include "Detector/UT/DeUTLayer.h"

#include <vector>

namespace LHCb::Detector::UT {

  namespace detail {

    struct DeUTStationObject : LHCb::Detector::detail::DeIOVObject {
      DeUTStationObject( dd4hep::DetElement const& de, dd4hep::cond::ConditionUpdateContext& ctxt );
    };

  } // namespace detail

  template <typename ObjectType>
  class DeUTStationElement : public DeIOVElement<ObjectType> {
  public:
    using DeIOVElement<ObjectType>::DeIOVElement;

    /// check contains channel
    bool contains( const ChannelID aChannel ) const { return false; }

    /// locate the layer based on a channel id
    const DeUTLayer& findLayer( const ChannelID aChannel ) const { return m_noLayer; }

    /// locate layer based on a point
    const std::optional<DeUTLayer> findLayer( const ROOT::Math::XYZPoint& point ) const { return {}; }

    /// vector of children
    const std::vector<DeUTLayer>& layers() const { return m_emptyLayers; }

    /// fraction active channels
    double fractionActive() const { return 0; }

  private:
    std::vector<DeUTLayer> m_emptyLayers;
    DeUTLayer              m_noLayer;
  };
  using DeUTStation = DeUTStationElement<detail::DeUTStationObject>;

} // End namespace LHCb::Detector::UT
