/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/UT/ChannelID.h"
#include "Detector/UT/DeUTSector.h"

#include <array>
#include <numeric>
#include <vector>

namespace LHCb::Detector::UT {

  namespace detail {

    struct DeUTModuleObject : LHCb::Detector::detail::DeIOVObject {
      DeUTModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );

      unsigned int                  id;
      std::vector<DeUTSectorObject> m_sectors;
      ChannelID                     m_channelID;

      unsigned int m_firstSector{0};
      std::string  m_versionString{"DC11"};
    };
  } // End namespace detail

  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  template <typename ObjectType>
  struct DeUTModuleElement : DeIOVElement<ObjectType> {

    using DeIOVElement<ObjectType>::DeIOVElement;

    ChannelID          channelID() const { return this->access()->m_channelID; }
    auto const&        module() { return this->access()->id; }
    auto               sectors() { return this->access()->m_sectors; }
    unsigned int       lastSector() const { return this->access()->m_firstSector + 1u; }
    unsigned int       firstSector() const { return this->access()->m_firstSector; }
    const std::string& versionString() const { return this->access()->m_versionString; }

    inline bool contains( const ChannelID ) const {
      throw "Not implemented";
      /* return ( aChannel.module() == module() &&
               aChannel.subsector() < 2 ) &&
               m_parent->contains( aChannel );*/
    }
    double fractionActive() const {
      return std::accumulate( sectors().begin(), sectors().end(), 0.0,
                              []( double f, const DeUTSector s ) { return f + s.fractionActive(); } ) /
             double( sectors().size() );
    }
    const DeUTSector findSector( const ROOT::Math::XYZPoint& point ) const {
      auto iter =
          std::find_if( sectors().begin(), sectors().end(), [&]( const DeUTSector s ) { return s.isInside( point ); } );
      return iter != sectors().end() ? *iter : nullptr;
    }
    const DeUTSector findSector( const ChannelID aChannel ) const {
      auto iter = std::find_if( sectors().begin(), sectors().end(),
                                [&]( const DeUTSector s ) { return s.contains( aChannel ); } );
      return iter != sectors().end() ? *iter : nullptr;
    }
  };

  using DeUTModule = DeUTModuleElement<detail::DeUTModuleObject>;

} // namespace LHCb::Detector::UT
