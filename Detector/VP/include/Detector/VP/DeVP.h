/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/DeIOV.h"
#include "Detector/VP/DeVPSensor.h"

#include <array>
#include <vector>

namespace LHCb::Detector {

  namespace detail {

    struct DeVPModuleObject : DeIOVObject {
      std::array<DeVPSensorObject, 4> sensors;
      DeVPModuleObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt, bool _isLeft );
      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& sensor : sensors ) { func( LHCb::Detector::DeIOV{&sensor} ); };
      };
      std::size_t size() const override { return sensors.size(); }
    };

    struct DeVPSideObject : DeIOVObject {
      std::array<DeVPModuleObject, VP::NModules / 2> modules;
      DeVPSideObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt, bool _isLeft );
      // FIXME This should not return false but rather the proper transformation
      std::optional<ROOT::Math::Transform3D> motionSystemTransform() const { return {}; }
      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& module : modules ) { func( LHCb::Detector::DeIOV{&module} ); };
      };
      std::size_t size() const override { return modules.size(); }
    };

    /**
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    struct DeVPObject : DeIOVObject {
      /// Left/right side
      std::array<DeVPSideObject, 2> sides; // left at index 0, right at index 1;
      DeVPObject( const dd4hep::DetElement& de, dd4hep::cond::ConditionUpdateContext& ctxt );

      // Caching of sensor values, for faster access

      /// local to global matrix cache
      std::array<std::array<float, 12>, VP::NSensors> m_ltg{{}};

      /// Cosine squared of rotation angle for each sensor.
      std::array<double, VP::NSensors> m_c2;

      /// Sine squared of rotation angle for each sensor.
      std::array<double, VP::NSensors> m_s2; /// Return the pixel size.

      void applyToAllChildren( const std::function<void( LHCb::Detector::DeIOV )>& func ) const override {
        for ( auto& side : sides ) { func( LHCb::Detector::DeIOV{&side} ); };
      };
      std::size_t size() const override { return sides.size(); }

      /// Printout method to stdout
      void print( int indent, int flags ) const override;
    };
  } // End namespace detail

  using DeVPModule = DeIOVElement<detail::DeVPModuleObject>;
  using DeVPSide   = DeIOVElement<detail::DeVPSideObject>;

  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  template <typename ObjectType>
  struct DeVPElement : DeIOVElement<ObjectType> {
    using DeIOVElement<ObjectType>::DeIOVElement;
    /// Return the number of sensors.
    size_t numberSensors() const { return VP::NSensors; }
    /// Return local x for a given sensor.
    float local_x( unsigned int n ) const {
      assert( n < VP::NSensorColumns );
      return this->access()->sides[0].modules[0].sensors[0].local_x[n] * 10.0f;
    }
    /// Return pitch for a given sensor.
    float x_pitch( unsigned int n ) const {
      assert( n < VP::NSensorColumns );
      return this->access()->sides[0].modules[0].sensors[0].x_pitch[n] * 10.0f;
    }
    float pixel_size() const { return this->access()->sides[0].modules[0].sensors[0].pixelSize * 10.0f; }
    /// Return local to global matirx for a sensor
    const std::array<float, 12>& ltg( VPChannelID::SensorID n ) const {
      return this->access()->m_ltg[to_unsigned( n )];
    }
    /// Return Cosine squared of rotation angle for a sensor
    double cos2OfSensor( VPChannelID::SensorID n ) const {
      assert( to_unsigned( n ) < VP::NSensors );
      return this->access()->m_c2[to_unsigned( n )];
    }
    /// Return Sine squared of rotation angle for a sensor
    double sin2OfSensor( VPChannelID::SensorID n ) const {
      assert( to_unsigned( n ) < VP::NSensors );
      return this->access()->m_s2[to_unsigned( n )];
    }
    /// Return pointer to sensor for a given sensor number.
    const DeVPSensor sensor( VPChannelID id ) const { return sensor( id.sensor() ); }

    /// Return pointer to sensor for a given sensor number.
    const DeVPSensor sensor( VPChannelID::SensorID sensorNumber ) const {
      assert( to_unsigned( sensorNumber ) < VP::NSensors );
      auto moduleNumber = to_unsigned( sensorNumber ) / VP::NSensorsPerModule;
      return moduleNumber % 2 == 0
                 ? DeVPSensor(
                       &( this->access()->sides[1].modules[moduleNumber / 2].sensors[to_unsigned( sensorNumber ) %
                                                                                     VP::NSensorsPerModule] ) )
                 : DeVPSensor( &(
                       this->access()->sides[0].modules[( moduleNumber - 1 ) / 2].sensors[to_unsigned( sensorNumber ) %
                                                                                          VP::NSensorsPerModule] ) );
    }
    /// Return pointer to sensor for a given sensor number.
    const DeVPModule module( VPChannelID::SensorID sensorNumber ) const {
      assert( to_unsigned( sensorNumber ) < VP::NSensors );
      auto moduleNumber = to_unsigned( sensorNumber ) / VP::NSensorsPerModule;
      return moduleNumber % 2 == 0 ? DeVPModule( &( this->access()->sides[1].modules[moduleNumber / 2] ) )
                                   : DeVPModule( &( this->access()->sides[0].modules[( moduleNumber - 1 ) / 2] ) );
    }
    template <class Operation>
    /// runs the given callable on every sensor
    void runOnAllSensors( Operation op ) const {
      for ( auto& side : this->access()->sides )
        for ( auto& module : side.modules )
          for ( auto& sensor : module.sensors ) op( DeVPSensor( &sensor ) );
    }
    const DeVPSide right() const { return DeVPSide( &( this->access()->sides[1] ) ); }
    const DeVPSide left() const { return DeVPSide( &( this->access()->sides[0] ) ); }
  };
  using DeVP = DeVPElement<detail::DeVPObject>;
} // End namespace LHCb::Detector
