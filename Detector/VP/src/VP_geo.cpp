//==========================================================================
//  AIDA Detector description implementation
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
//
//==========================================================================
#include "Core/UpgradeTags.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "XML/Utilities.h"

namespace {

  /// Helper class to build the VP detector of LHCb
  struct VPBuild : public dd4hep::xml::tools::VolumeBuilder {
    /// Debug flags
    dd4hep::Material silicon;
    /// Reference to the module detector element
    dd4hep::DetElement de_module_with_support;
    /// Flag to disable building supports
    bool build_mod_support = true;
    /// Flag to disable building the VP half stations
    bool build_sides = true;
    /// Flag to disable building the wakefield cones
    bool build_wake_cones = true;
    /// Flag to disable building the detector support in the vacuum tank
    bool build_det_support = true;
    /// Flag to disable building the vakuum tank
    bool build_vacuum_tank = true;
    /// Flag to disable building the RF box
    bool build_rf_box = true;
    /// Flag to disable building the RF foil
    bool        build_rf_foil = true;
    std::string select_volume{"lvVP"};
    /// Initializing constructor
    VPBuild( dd4hep::Detector& description, xml_elt_t e, dd4hep::SensitiveDetector sens );
    void build_groups();
    void build_module();
    void build_detector();
  };

  /// Initializing constructor
  VPBuild::VPBuild( dd4hep::Detector& dsc, xml_elt_t e, dd4hep::SensitiveDetector sens )
      : dd4hep::xml::tools::VolumeBuilder( dsc, e, sens ) {
    // Process debug flags
    xml_comp_t x_dbg = x_det.child( _U( debug ), false );
    if ( x_dbg ) {
      for ( xml_coll_t i( x_dbg, _U( item ) ); i; ++i ) {
        xml_comp_t  c( i );
        std::string n = c.nameStr();
        if ( n == "debug" )
          debug = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_mod_support" )
          build_mod_support = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_sides" )
          build_sides = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_det_support" )
          build_det_support = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_vacuum_tank" )
          build_vacuum_tank = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_wake_cones" )
          build_wake_cones = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_rf_box" )
          build_rf_box = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "build_rf_foil" )
          build_rf_foil = c.attr<int>( _U( value ) ) != 0;
        else if ( n == "select_volume" )
          select_volume = c.attr<std::string>( _U( value ) );
      }
    }
    silicon = description.material( "Silicon" );
  }

  void VPBuild::build_module() {
    // double       eps    = dd4hep::_toDouble("VP:Epsilon");
    double               vp_eps   = dd4hep::_toDouble( "VP:VPEpsilon" );
    double               vp_rot_z = dd4hep::_toDouble( "VP:Rotation" );
    dd4hep::Position     pos;
    dd4hep::PlacedVolume pv;

    // Distance between two readout chips
    double Chip2ChipDist = dd4hep::_toDouble( "VP:Chip2ChipDist" );
    // Chip dimensions (total = active area plus inactive edges)
    double ChipThick      = dd4hep::_toDouble( "VP:ChipThick" );
    double ChipTotalSizeX = dd4hep::_toDouble( "VP:ChipTotalSizeX" );
    double ChipTotalSizeY = dd4hep::_toDouble( "VP:ChipTotalSizeY" );
    double ChipActiveSize = dd4hep::_toDouble( "VP:ChipActiveSize" );
    double ChipTopEdgeY   = dd4hep::_toDouble( "VP:ChipTopEdgeY" );
    // Thickness of glue layer
    double GlueThick = dd4hep::_toDouble( "VP:GlueThick" );
    // Thickness of bump bond layer
    double BumpBondsThick = dd4hep::_toDouble( "VP:BumpBondsThick" );
    // Sensor dimensions (active area)
    double SiThick = dd4hep::_toDouble( "VP:SiThick" );
    double SiSizeX = dd4hep::_toDouble( "VP:SiSizeX" );
    double SiSizeY = dd4hep::_toDouble( "VP:SiSizeY" );
    // Width of inactive edge
    double SiEdge = dd4hep::_toDouble( "VP:SiEdge" );

    // Description of single Chip
    dd4hep::Box    boxChip( ChipTotalSizeX / 2.0, ChipTotalSizeY / 2.0, ChipThick / 2.0 );
    dd4hep::Volume lvChip( "lvChip", boxChip, silicon );
    lvChip.setVisAttributes( description, "VP:Chip" );

    // Glue layer between substrate and chip
    dd4hep::Box    boxGlue( ChipTotalSizeX / 2.0, ChipTotalSizeY / 2.0, GlueThick / 2.0 );
    dd4hep::Volume lvGlue( "lvGlue", boxGlue, description.material( "VP:Stycast" ) );
    lvGlue.setVisAttributes( description, "VP:Glue" );

    // Bump bond layer between chip and sensor
    dd4hep::Box    boxBumpBonds( ChipActiveSize / 2.0, ChipActiveSize / 2.0, BumpBondsThick / 2.0 );
    dd4hep::Volume lvBumpBonds( "lvBumpBonds", boxBumpBonds, description.material( "VP:BumpBonds" ) );
    lvBumpBonds.setVisAttributes( description, "VP:BumpBonds" );

    // Chip array
    dd4hep::Assembly lvChips( "lvChips" );
    double           pos_X = SiSizeX / 2.0 - ChipActiveSize - Chip2ChipDist;
    double           pos_Y = SiSizeY + ChipTopEdgeY - ChipTotalSizeY / 2.0;
    lvChips.placeVolume( lvChip, dd4hep::Position( pos_X, pos_Y, ChipThick / 2.0 ) );
    lvChips.placeVolume( lvGlue, dd4hep::Position( pos_X, pos_Y, -GlueThick / 2.0 ) );
    lvChips.placeVolume( lvBumpBonds, dd4hep::Position( pos_X, SiSizeY / 2.0, ChipThick + BumpBondsThick / 2.0 ) );
    pos_X = SiSizeX / 2.0;
    lvChips.placeVolume( lvChip, dd4hep::Position( pos_X, pos_Y, ChipThick / 2.0 ) );
    lvChips.placeVolume( lvGlue, dd4hep::Position( pos_X, pos_Y, -GlueThick / 2.0 ) );
    lvChips.placeVolume( lvBumpBonds, dd4hep::Position( pos_X, SiSizeY / 2.0, ChipThick + BumpBondsThick / 2.0 ) );
    pos_X = SiSizeX / 2.0 + ChipActiveSize + Chip2ChipDist;
    lvChips.placeVolume( lvChip, dd4hep::Position( pos_X, pos_Y, ChipThick / 2.0 ) );
    lvChips.placeVolume( lvGlue, dd4hep::Position( pos_X, pos_Y, -GlueThick / 2.0 ) );
    lvChips.placeVolume( lvBumpBonds, dd4hep::Position( pos_X, SiSizeY / 2.0, ChipThick + BumpBondsThick / 2.0 ) );

    // Silicon sensor
    dd4hep::Box              detBox( ( SiSizeX + 2 * SiEdge ) / 2.0, ( SiSizeY + 2 * SiEdge ) / 2.0, SiThick / 2.0 );
    dd4hep::Tube             detCutTopRight( SiEdge + vp_eps, 2.0 * SiEdge, 2.0 * SiThick, 0.0 * dd4hep::degree,
                                 90.0 * dd4hep::degree );
    dd4hep::Tube             detCutTopLeft( SiEdge + vp_eps, 2.0 * SiEdge, 2.0 * SiThick, 90.0 * dd4hep::degree,
                                180.0 * dd4hep::degree );
    dd4hep::Tube             detCutBottomLeft( SiEdge + vp_eps, 2.0 * SiEdge, 2.0 * SiThick, 180.0 * dd4hep::degree,
                                   270.0 * dd4hep::degree );
    dd4hep::Tube             detCutBottomRight( SiEdge + vp_eps, 2.0 * SiEdge, 2.0 * SiThick, 270.0 * dd4hep::degree,
                                    360.0 * dd4hep::degree );
    dd4hep::SubtractionSolid detSolid( detBox, detCutTopRight, dd4hep::Position( SiSizeX / 2.0, SiSizeY / 2.0, 0 ) );
    detSolid =
        dd4hep::SubtractionSolid( detSolid, detCutTopLeft, dd4hep::Position( -SiSizeX / 2.0, SiSizeY / 2.0, 0 ) );
    detSolid =
        dd4hep::SubtractionSolid( detSolid, detCutBottomLeft, dd4hep::Position( -SiSizeX / 2.0, -SiSizeY / 2.0, 0 ) );
    detSolid =
        dd4hep::SubtractionSolid( detSolid, detCutBottomRight, dd4hep::Position( SiSizeX / 2.0, -SiSizeY / 2.0, 0 ) );
    dd4hep::Volume lvDet( "lvDet", detSolid, silicon );
    lvDet.setVisAttributes( description, "VP:Sensor" );
    lvDet.setSensitiveDetector( sensitive );

    dd4hep::Assembly     lvSensor( "lvSensor" );
    dd4hep::PlacedVolume pvSensor = lvSensor.placeVolume( lvDet, dd4hep::Position( SiSizeX / 2.0, SiSizeY / 2.0, 0 ) );
    registerVolume( lvSensor.name(), lvSensor );

    dd4hep::Assembly lvLadder( "lvLadder" );
    lvLadder.placeVolume( lvSensor, dd4hep::Position( 0, 0, ChipThick + BumpBondsThick + SiThick / 2.0 + vp_eps ) );
    lvLadder.placeVolume( lvChips );
    registerVolume( lvLadder.name(), lvLadder );

    dd4hep::DetElement deLadder, deSensor;
    dd4hep::Assembly   lvModule( "lvModule" );
    registerVolume( lvModule.name(), lvModule );

    /// Module structural hierarchy
    de_module_with_support = DetElement( "module", id );

    // Thickness of silicon substrate
    double SubstrateThick = dd4hep::_toDouble( "VP:SubstrateThick" );

    // Size of a single pixel
    double PixelSize = dd4hep::_toDouble( "VP:PixelSize" );

    // Positions of first pixel rows and columns
    double ClosestInnerPixelRow    = dd4hep::_toDouble( "VP:ClosestInnerPixelRow" );
    double ClosestOuterPixelRow    = dd4hep::_toDouble( "VP:ClosestOuterPixelRow" );
    double ClosestShortPixelColumn = dd4hep::_toDouble( "VP:ClosestShortPixelColumn" );
    double ClosestNLOPixelColumn   = dd4hep::_toDouble( "VP:ClosestNLOPixelColumn" );
    double ClosestCLIPixelColumn   = dd4hep::_toDouble( "VP:ClosestCLIPixelColumn" );

    // Connector side Long Inner (CLI)
    pos = dd4hep::Position( SiSizeX - ClosestCLIPixelColumn - PixelSize / 2.0,
                            -( ClosestInnerPixelRow + SiSizeY - PixelSize / 2.0 ),
                            -( SubstrateThick / 2.0 + GlueThick ) );
    pv  = lvModule.placeVolume( lvLadder, dd4hep::Transform3D( dd4hep::RotationZ( -vp_rot_z ) ) *
                                             dd4hep::Transform3D( dd4hep::RotationY( M_PI ), pos ) );
    pv->SetName( "pvLadder_0" );
    pv.addPhysVolID( "ladder", 0 );
    deLadder = DetElement( de_module_with_support, "ladder_0", 0 );
    deLadder.setPlacement( pv );
    deSensor = DetElement( deLadder, "sensor", 0 );
    deSensor.setPlacement( pvSensor );

    // Non-connector side Long Outer (NLO)
    pos = dd4hep::Position( -( ClosestNLOPixelColumn + PixelSize / 2.0 ),
                            -( ClosestOuterPixelRow + SiSizeY - PixelSize / 2.0 ), SubstrateThick / 2.0 + GlueThick );
    pv  = lvModule.placeVolume( lvLadder,
                               dd4hep::Transform3D( dd4hep::RotationZ( -vp_rot_z ) ) * dd4hep::Transform3D( pos ) );
    pv->SetName( "pvLadder_1" );
    pv.addPhysVolID( "ladder", 1 );
    deLadder = DetElement( de_module_with_support, "ladder_1", 0 );
    deLadder.setPlacement( pv );
    deSensor = DetElement( deLadder, "sensor", 1 );
    deSensor.setPlacement( pvSensor );

    // Non-connector side Short Inner (NSI)
    pos = dd4hep::Position( -( ClosestInnerPixelRow + SiSizeY - PixelSize / 2.0 ),
                            ClosestShortPixelColumn + PixelSize / 2.0, SubstrateThick / 2.0 + GlueThick );
    pv  = lvModule.placeVolume( lvLadder, dd4hep::Transform3D( dd4hep::RotationZ( -vp_rot_z ) ) *
                                             dd4hep::Transform3D( dd4hep::RotationZ( -M_PI / 2.0 ), pos ) );
    pv->SetName( "pvLadder_2" );
    pv.addPhysVolID( "ladder", 2 );
    deLadder = DetElement( de_module_with_support, "ladder_2", 0 );
    deLadder.setPlacement( pv );
    deSensor = DetElement( deLadder, "sensor", 2 );
    deSensor.setPlacement( pvSensor );

    // Connector side Short Outer (CSO)
    pos = dd4hep::Position( -( ClosestOuterPixelRow + SiSizeY - PixelSize / 2.0 ),
                            -( SiSizeX - ClosestShortPixelColumn - PixelSize / 2.0 ),
                            -( SubstrateThick / 2.0 + GlueThick ) );
    pv  = lvModule.placeVolume( lvLadder, dd4hep::Transform3D( dd4hep::RotationZ( -vp_rot_z ) ) *
                                             dd4hep::Transform3D( dd4hep::RotationZYX( M_PI / 2.0, M_PI, 0.0 ), pos ) );
    pv->SetName( "pvLadder_3" );
    pv.addPhysVolID( "ladder", 3 );
    deLadder = DetElement( de_module_with_support, "ladder_3", 0 );
    deLadder.setPlacement( pv );
    deSensor = DetElement( deLadder, "sensor", 3 );
    deSensor.setPlacement( pvSensor );

    // Hybrid
    if ( build_mod_support ) lvModule.placeVolume( volume( "lvHybrid" ) );

    dd4hep::Assembly lvModuleWithSupport( "lvModuleWithSupport" );
    if ( build_mod_support ) lvModuleWithSupport.placeVolume( volume( "lvSupport" ) );
    lvModuleWithSupport.placeVolume( lvModule );
    registerVolume( lvModuleWithSupport.name(), lvModuleWithSupport );
  }

  void VPBuild::build_detector() {
    dd4hep::PlacedVolume pv;
    double               Right2LeftDist = dd4hep::_toDouble( "VP:Right2LeftDist" );
    dd4hep::Assembly     lvLeft, lvRight;
    dd4hep::DetElement   deOpt, deLeft, deRight;
    if ( build_sides ) {
      dd4hep::RotationZYX rot_left( M_PI, 0, 0 );
      dd4hep::Volume      lvModule = volume( "lvModuleWithSupport" );
      lvLeft                       = dd4hep::Assembly( "lvVPLeft" );
      lvRight                      = dd4hep::Assembly( "lvVPRight" );
      deLeft                       = dd4hep::DetElement( detector, "VPLeft", 0 );
      deRight                      = dd4hep::DetElement( detector, "VPRight", 1 );

      for ( int i = 0; i <= 25; ++i ) { // One or two loops -- what would be more efficient in tracking?
        double station_z = dd4hep::_toDouble( dd4hep::_toString( i, "VP:Station%02dZ" ) );
        pv               = lvRight.placeVolume( lvModule, dd4hep::Position( 0, 0, station_z - Right2LeftDist / 2.0 ) );
        pv->SetName( dd4hep::_toString( i * 2, "pvModule%02dWithSupport" ).c_str() ); // Optional
        pv.addPhysVolID( "module", i * 2 );
        deOpt = de_module_with_support.clone( dd4hep::_toString( i * 2, "Module%02d" ), i * 2 );
        deOpt.setPlacement( pv );
        deRight.add( deOpt );
        pv = lvLeft.placeVolume(
            lvModule, dd4hep::Transform3D( rot_left, dd4hep::Position( 0, 0, station_z + Right2LeftDist / 2.0 ) ) );
        pv->SetName( dd4hep::_toString( 1 + i * 2, "pvModule%02dWithSupport" ).c_str() ); // Optional
        pv.addPhysVolID( "module", i * 2 + 1 );
        deOpt = de_module_with_support.clone( dd4hep::_toString( i * 2 + 1, "Module%02d" ), i * 2 + 1 );
        deOpt.setPlacement( pv );
        deLeft.add( deOpt );
      }
      if ( build_rf_box ) {
        // dd4hep::printout( dd4hep::WARNING, "VP", "+++ Placing RF box at an ASSUMED position! [not found in XML]" );
        // This looks correct:
        // lvLeft.placeVolume( volume("lvRFBoxLeft"),  dd4hep::Position(0,0,dd4hep::_toDouble("-243*mm")));
        // lvRight.placeVolume(volume("lvRFBoxRight"), dd4hep::Position(0,0,dd4hep::_toDouble("-243*mm")));
        // However, this is what is in the XML:
        pv    = lvLeft.placeVolume( volume( "lvRFBoxLeft" ) );
        deOpt = DetElement( deLeft, "RFBox", deLeft.id() );
        deOpt.setPlacement( pv );
        pv    = lvRight.placeVolume( volume( "lvRFBoxRight" ) );
        deOpt = DetElement( deRight, "RFBox", deRight.id() );
        deOpt.setPlacement( pv );
      }
      if ( build_rf_foil ) {
        dd4hep::Volume   vol = volume( "lvRFFoil" );
        dd4hep::Position pos_left( 0, 0, dd4hep::_toDouble( "VP:RFFoilLeftPosZ" ) );
        dd4hep::Position pos_right( 0, 0, dd4hep::_toDouble( "VP:RFFoilRightPosZ" ) );
        pv    = lvLeft.placeVolume( vol, pos_left );
        deOpt = DetElement( deLeft, "RFFoilLeft", deLeft.id() );
        deOpt.setPlacement( pv );
        pv    = lvRight.placeVolume( vol, dd4hep::Transform3D( dd4hep::RotationZYX( M_PI, 0, 0 ), pos_right ) );
        deOpt = DetElement( deRight, "RFFoilRight", deRight.id() );
        deOpt.setPlacement( pv );
      }
    }
    if ( build_vacuum_tank && build_det_support ) {
      dd4hep::Volume   vol = volume( "lvVeloDetSup" );
      dd4hep::Position pos_left( dd4hep::_toDouble( "VP:DetSupLeftPosX" ), 0,
                                 dd4hep::_toDouble( "VP:DetSupLeftPosZ" ) );
      dd4hep::Position pos_right( dd4hep::_toDouble( "VP:DetSupRightPosX" ), 0,
                                  dd4hep::_toDouble( "VP:DetSupRightPosZ" ) );
      pv    = lvLeft.placeVolume( vol, dd4hep::Transform3D( dd4hep::RotationZYX( M_PI, 0, 0 ), pos_left ) );
      deOpt = DetElement( deLeft, "Support", deLeft.id() );
      deOpt.setPlacement( pv );
      pv    = lvRight.placeVolume( vol, pos_right );
      deOpt = DetElement( deRight, "Support", deRight.id() );
      deOpt.setPlacement( pv );
    }

    // Now we build the main velo shape
    double             zpos            = dd4hep::_toDouble( "VP:VeloZ" );
    double             radius          = dd4hep::_toDouble( "VP:VeloRad" );
    double             DSEndStartZ     = dd4hep::_toDouble( "VP:VeloDSEndStartZ" );
    double             DSEndDeltaZ     = dd4hep::_toDouble( "VP:VeloDSEndDeltaZ" );
    double             EWFlangeZExcess = dd4hep::_toDouble( "VP:VeloEWFlangeZExcess" );
    dd4hep::UnionSolid vp_shape( dd4hep::Tube( 0, radius, zpos / 2.0 ), dd4hep::Tube( 0, radius, DSEndDeltaZ / 2.0 ),
                                 dd4hep::Position( 0, 0, DSEndStartZ + DSEndDeltaZ / 2.0 ) );
    vp_shape = dd4hep::UnionSolid(
        vp_shape,
        dd4hep::Tube( "VP:vTankDownEWFlangeIR-2*mm", "VP:vTankDownEWFlangeOR+2*mm",
                      EWFlangeZExcess / 2.0 + 1. * dd4hep::mm ),
        dd4hep::Position( 0, 0, DSEndStartZ + DSEndDeltaZ + ( EWFlangeZExcess + 2.0 * dd4hep::mm ) / 2.0 ) );

    dd4hep::Volume lvVP( "lvVP", vp_shape, description.vacuum() );
    lvVP.setAttributes( description, x_det.regionStr(), x_det.limitsStr(), x_det.visStr() );
    if ( build_sides ) {
      pv = lvVP.placeVolume( lvLeft );
      pv.addPhysVolID( "side", 0 );
      deLeft.setPlacement( pv );
      pv = lvVP.placeVolume( lvRight );
      pv.addPhysVolID( "side", 1 );
      deRight.setPlacement( pv );
    }
    if ( build_vacuum_tank ) {
      DetElement       de( detector, "VacuumTank", id );
      dd4hep::Position pos( 0, 0, dd4hep::_toDouble( "VP:vTankPosZ" ) );
      pv = lvVP.placeVolume( volume( "lvVacTank" ), pos );
      de.setPlacement( pv );
    }
    if ( build_wake_cones ) {
      // We have them as volumes, but we dop not enter them into the hierarchy.
      dd4hep::Position pos_up( 0, 0, dd4hep::_toDouble( "VP:UpStrWakeFieldConePosZ" ) );
      lvVP.placeVolume( volume( "lvUpstreamWakeFieldCone" ), pos_up );
      dd4hep::Position pos_down( 0, 0, dd4hep::_toDouble( "VP:DownStrWakeFieldConePosZ" ) );
      lvVP.placeVolume( volume( "lvDownstreamWakeFieldCone" ), pos_down );
    }
    registerVolume( lvRight.name(), lvRight );
    registerVolume( lvLeft.name(), lvRight );
    registerVolume( lvVP.name(), lvVP );
  }

  void VPBuild::build_groups() {
    sensitive.setType( "tracker" );
    load( x_det, "include" );
    buildVolumes( x_det );
    placeDaughters( detector, dd4hep::Volume(), x_det );
    if ( build_sides ) { build_module(); }
    build_detector();
    dd4hep::Volume       vol = volume( select_volume );
    dd4hep::PlacedVolume pv  = placeDetector( vol );
    pv.addPhysVolID( "system", id );
    dd4hep::detail::destroyHandle( de_module_with_support );
  }

} // namespace

static dd4hep::Ref_t create_element( dd4hep::Detector& description, xml_h e, dd4hep::SensitiveDetector sens_det ) {
  VPBuild builder( description, e, sens_det );
  builder.build_groups();
  return builder.detector;
}
DECLARE_DETELEMENT( LHCb_VP_v1_0, create_element )
