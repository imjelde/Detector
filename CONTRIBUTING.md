# General information

This project contains the description of the LHCb Detector using the DD4hep
CompactXML format. It can be compiled independently of the LHCb stack and only
relies on ROOT and DD4hep (and their dependencies) to be available.

Each change to subdetector descriptions should be validated by members of the
subdetector teams before being merged into the main codebase.

[//]: # Include versioning info
