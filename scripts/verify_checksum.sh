#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -e

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
SCRIPT_LOC=$(cd  $dir;pwd)

tmpfile=$(mktemp /tmp/verifchksum.XXXXXX)
checksumGeo -o $tmpfile $1
diff $(dirname $1)/checksums $tmpfile
ret=$?
if [ $? -eq 0 ]; then
    echo "Checksums in $(dirname $1)/checksums: OK"
else
    echo "Checksums in $(dirname $1)/checksums: ERROR"
fi

rm "$tmpfile"
exit $ret
