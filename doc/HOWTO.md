# Detector development howto


## Setting up the project

To download the code and setup the project (only the first time):

``` 
git clone ssh://git@gitlab.cern.ch:7999/lhcb/Detector.git
cd Detector
lb-project-init
```

## Choosing a platform and building

To be repeated in every new session:

```
lb-set-platform x86_64-centos7-gcc11-dbg
export LCG_VERSION=101
make install
```


## Viewing the geometry


At this stage, Detector is built and the the "run" script in the InstallArea:
```
./InstallArea/x86_64-centos7-gcc11-dbg/bin/run geoDisplay compact/trunk/LHCb.xml

```

N.B. Due to interferences between ROOT and the llvmpipe graphics driver on *centos7*, segfaults can occur when using the ROOT visualization tools, in libLLVM-6.0-rhel.so e.g.

```
#6  0x00007f4723b94e20 in __memcpy_ssse3 () from /lib64/libc.so.6
#7  0x00007f4706057bdb in llvm::WritableMemoryBuffer::getNewUninitMemBuffer(unsigned long, llvm::Twine const&) () from /usr/lib64/libLLVM-6.0-rhel.so
#8  0x00007f4706057caf in getMemBufferCopyImpl(llvm::StringRef, llvm::Twine const&) () from /usr/lib64/libLLVM-6.0-rhel.so

```
In that case, try using the drivers provided by EP-SFT on CVMFS:
```
export LIBGL_DRIVERS_PATH=/cvmfs/sft.cern.ch/lcg/contrib/mesa/18.0.5/x86_64-centos7/lib64/dri
```

It is also possible to visualize using the ROOT experimental web interface by doing:

```
./InstallArea/x86_64-centos7-gcc11-dbg/bin/run geoDisplayWeb compact/trunk/LHCb.xml

```




## Developing a subdetector


See [DETECTOR.md](doc/DETECTOR.md) for a description of the project, and  <http://dd4hep.web.cern.ch/dd4hep/> for DD4hep itself. 

### Running  DD4hep plugins

To run a specific plugin, the DD4hep commands are available in the environment setup by the "run" script. 
e.g. to view the VP subdetector as described in compact representation:

```
./InstallArea/x86_64-centos7-gcc11-dbg/bin/run geoDisplay compact/trunk/debug/VP_debug.xml 
```

The *geoDisplay* command keeps a ROOT prompt, with the geometry loaded. From there it is possible to:
  * export the geometry to a ROOT file for further use
  * run any ROOT tool or script, e.g. the overlap checker

### Running simulations

Simlations with the geometry should be run using the Gaussino tool. PLease contact the simulation group for more information.

